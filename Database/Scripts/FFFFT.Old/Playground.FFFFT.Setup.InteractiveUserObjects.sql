USE FFFFT
GO

--creating the interactive tables
IF EXISTS (SELECT TOP 1 8 FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'DraftLeague')
BEGIN
	DROP TABLE dbo.[DraftLeague]
END
GO
CREATE TABLE [dbo].[DraftLeague] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[LeagueName] nvarchar(50) NOT NULL,
	[LeagueMotto] nvarchar(400) NULL,
	[LeagueSportCode] nvarchar(5) NOT NULL,
	[LeagueBuyIn] money NULL,
	[LeagueTeamCount] int NULL,
	[LeagueMaxPlayersPerTeam] int NULL,
	[LeagueHash] [varbinary](128) NOT NULL,
	[LeagueImage] [varbinary](max) NULL,
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(50) NULL
	CONSTRAINT [PK_DraftLeague] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT TOP 1 8 FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'SiteUser')
BEGIN
	DROP TABLE dbo.[SiteUser]
END
GO
CREATE TABLE [dbo].[SiteUser] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[UserName] nvarchar(50) NOT NULL,
	[Password] [varbinary](128) NULL,
	[Hash] [varbinary](128) NULL,
	[Salt] [varchar](32) NULL,
	[FirstName] nvarchar(50) NOT NULL,
	[LastName] nvarchar(50) NOT NULL,
	[MiddleInitial] nvarchar(1) NULL,
	[UserEmail] nvarchar(200) NOT NULL,
	[UserImage] varbinary(max) NULL,
	[IsAdmin] bit NOT NULL DEFAULT(0),
	[InActive] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(50) NULL
	CONSTRAINT [PK_SiteUser] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT TOP 1 8 FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'DraftLeaguePayout')
BEGIN
	DROP TABLE dbo.[DraftLeaguePayout]
END
GO
CREATE TABLE [dbo].[DraftLeaguePayout] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[DraftLeagueKey] int NOT NULL,
	[Ranking] int NOT NULL,
	[Payout] money NULL,
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(50) NULL
	CONSTRAINT [PK_DraftLeaguePayout] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT TOP 1 8 FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'DraftUser')
BEGIN
	DROP TABLE dbo.[DraftUser]
END
GO
CREATE TABLE [dbo].[DraftUser] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[SiteUserKey] int NOT NULL,
	[DraftLeagueKey] int NOT NULL,
	[DraftImage] [varbinary](max) NOT NULL,
	[IsCommissioner] bit NOT NULL DEFAULT(0),
	[IsCoCommissioner] bit NOT NULL DEFAULT(0),
	[IsPieceOfShit] bit NOT NULL DEFAULT(1),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(50) NULL
	CONSTRAINT [PK_DraftUser] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT TOP 1 8 FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'DraftTeam')
BEGIN
	DROP TABLE dbo.[DraftTeam]
END
GO
CREATE TABLE [dbo].[DraftTeam] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[DraftUserKey] int NOT NULL,
	[TeamName] nvarchar(50) NOT NULL,
	[TeamImage] [varbinary](max) NULL,
	[TeamMotto] nvarchar(100) NULL,
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(50) NULL
	CONSTRAINT [PK_DraftTeam] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT TOP 1 8 FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'PlayersDrafted')
BEGIN
	DROP TABLE dbo.[PlayersDrafted]
END
GO
CREATE TABLE [dbo].[PlayersDrafted] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[PlayerKey] int NOT NULL,
	[DraftTeamKey] int NOT NULL,
	[AmountPaid] money NOT NULL,
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(50) NULL
	CONSTRAINT [PK_PlayersDrafted] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT TOP 1 8 FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'TeamsDrafted')
BEGIN
	DROP TABLE dbo.[TeamsDrafted]
END
GO
CREATE TABLE [dbo].[TeamsDrafted] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[TeamKey] int NOT NULL,
	[DraftTeamKey] int NOT NULL,
	[AmountPaid] money NOT NULL,
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(50) NULL
	CONSTRAINT [PK_TeamsDrafted] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO

IF EXISTS (SELECT TOP 1 8 FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'DraftLog')
BEGIN
	DROP TABLE dbo.[DraftLog]
END
GO
CREATE TABLE [dbo].[DraftLog] (
	[PK] [bigint] NOT NULL IDENTITY(1,1),
	[SiteUserKey] int NOT NULL,
	[Procedure] nvarchar(200) NOT NULL,
	[Action] nvarchar(200) NOT NULL,
	[Parameters] varchar(max) NULL,
	[Status] nvarchar(3) NOT NULL,
	[ErrorCode] nvarchar(10) NULL,
	[ErrorMessage] varchar(max) NULL,
	[DateTimeStamp] datetime2 NOT NULL
	CONSTRAINT [PK_DraftLog] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO


--uniques
CREATE UNIQUE NONCLUSTERED INDEX UIX_SiteUser_UserName
ON [dbo].[SiteUser] ([UserName])
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_SiteUser_UserEmail
ON [dbo].[SiteUser] ([UserEmail])
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_DraftLeague_LeagueSportCodeLeagueHash
ON [dbo].[DraftLeague] ([LeagueSportCode], [LeagueHash])
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_DraftUser_SiteUserKeyDraftLeagueKey
ON [dbo].[DraftUser] ([SiteUserKey], [DraftLeagueKey])
WHERE [SiteUserKey] IS NOT NULL
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_PlayersDrafted_PlayerKeyDraftTeamKey
ON [dbo].[PlayersDrafted] ([PlayerKey], [DraftTeamKey])
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_TeamsDrafted_PlayerKeyTeamKey
ON [dbo].[TeamsDrafted] ([TeamKey], [DraftTeamKey])
GO


IF EXISTS (SELECT TOP 1 100 FROM sys.objects WHERE [type] IN (N'FN', N'IF', N'TF', N'FS', N'FT') AND SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'sp_user_exists')
BEGIN
	DROP FUNCTION dbo.sp_user_exists;
END
GO
CREATE FUNCTION dbo.sp_user_exists (@SiteUserKey int)
RETURNS bit
WITH EXECUTE AS CALLER
AS
	BEGIN
		IF EXISTS (SELECT TOP 1 100 FROM dbo.SiteUser WHERE PK = @SiteUserKey)
		BEGIN
			RETURN 1;
		END
		RETURN 0;		
	END
GO

IF EXISTS (SELECT TOP 1 100 FROM sys.objects WHERE [type] IN (N'FN', N'IF', N'TF', N'FS', N'FT') AND SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'sp_get_username')
BEGIN
	DROP FUNCTION dbo.sp_get_username;
END
GO
CREATE FUNCTION dbo.sp_get_username (@SiteUserKey int)
RETURNS nvarchar(50)
WITH EXECUTE AS CALLER
AS
	BEGIN		
		RETURN (SELECT TOP 1 UserName FROM dbo.SiteUser WHERE PK = @SiteUserKey);		
	END
GO

IF EXISTS (SELECT TOP 1 100 FROM sys.objects WHERE [type] IN (N'FN', N'IF', N'TF', N'FS', N'FT') AND SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'sp_user_authorized')
BEGIN
	DROP FUNCTION dbo.sp_user_authorized;
END
GO
CREATE FUNCTION dbo.sp_user_authorized (@SiteUserKey int, @AdminOnly bit = 0)
RETURNS bit
WITH EXECUTE AS CALLER
AS
	BEGIN
		IF EXISTS (SELECT TOP 1 100 FROM dbo.SiteUser WHERE PK = @SiteUserKey AND InActive = 0 AND (@AdminOnly = 0 OR IsAdmin = 1))
		BEGIN
			RETURN 1;
		END
		RETURN 0;		 
	END
GO

IF EXISTS (SELECT TOP 1 100 FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'usp_insert_DraftLog')
BEGIN
	DROP PROC dbo.usp_insert_DraftLog;
END
GO
CREATE PROC dbo.usp_insert_DraftLog
@ErrorType nchar(2) OUTPUT
, @ErrorMessage nvarchar(max) = NULL OUTPUT
, @ErrorCode nvarchar(10) = NULL OUTPUT 
, @SiteUserKey int 
, @Parameters varchar(max) 
, @Action nvarchar(200) 
, @Procedure nvarchar(200) 
AS
	BEGIN TRY

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRAN
	
	DECLARE @DateTimeStamp datetime2 = GETDATE();

	INSERT INTO [dbo].[DraftLog] (
		[SiteUserKey]
		, [Parameters]
		, [Action]
		, [Procedure]
		, [Status]
		, [ErrorCode]
		, [ErrorMessage]
		, [DateTimeStamp]
	)
	VALUES
	(@SiteUserKey
	, @Parameters
	, @Action
	, @Procedure
	, @ErrorType
	, @ErrorCode
	, @ErrorMessage
	, @DateTimeStamp)

	COMMIT TRAN

	END TRY
	BEGIN CATCH
		IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRAN
		END
		SET @ErrorCode = (SELECT ERROR_NUMBER());
		SET @ErrorType = 'ET'
		SET @ErrorMessage = (SELECT ERROR_MESSAGE());
				
		RAISERROR(@ErrorMessage, 16, 1);
		RETURN;
	END CATCH
GO

IF EXISTS (SELECT TOP 1 100 FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'usp_insert_SiteUser')
BEGIN
	DROP PROC dbo.usp_insert_SiteUser;
END
GO
CREATE PROC dbo.usp_insert_SiteUser
@ErrorType nchar(2) = 'ST' OUTPUT
, @ErrorMessage nvarchar(max) = NULL OUTPUT
, @ErrorCode nvarchar(10) = NULL OUTPUT 
, @Action nvarchar(200)
, @SiteUserKey int NULL OUTPUT

, @UserName [nvarchar](50) 
, @Password [varbinary](128) 
, @Hash [varbinary](128) 
, @Salt [varchar](32) 
, @FirstName [nvarchar](50) 
, @LastName [nvarchar](50) 
, @MiddleInitial [nvarchar](1)
, @UserEmail [nvarchar](200) 
, @UserImage [varbinary](max) 
, @IsAdmin [bit] = 0
--, @SiteUserKey int OUTPUT
AS
DECLARE @Procedure nvarchar(200) = 'dbo.usp_insert_SiteUser'
, @Parameters varchar(max) = '@UserName: ' + ISNULL(@UserName, 'NULL')
							+ '| @Password: ' + ISNULL(CONVERT(varchar, @Password), 'NULL')
							+ '| @Hash: ' + ISNULL(CONVERT(varchar, @Hash), 'NULL')
							+ '| @Salt: ' + ISNULL(@Salt, 'NULL')
							+ '| @FirstName: ' + ISNULL(@FirstName, 'NULL')
							+ '| @LastName: ' + ISNULL(@LastName, 'NULL')
							+ '| @MiddleInitial: ' + ISNULL(@MiddleInitial, 'NULL')
							+ '| @UserEmail: ' + ISNULL(@UserEmail, 'NULL')
							+ '| @UserImage: ' + ISNULL(CONVERT(varchar, @UserImage), 'NULL')
							+ '| @IsAdmin: ' + ISNULL(CONVERT(varchar, @IsAdmin), 'NULL');

BEGIN TRY

	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;

	IF (@ErrorType = 'ET')
	BEGIN
		RETURN;
	END

	IF EXISTS (SELECT TOP 1 100 FROM dbo.SiteUser WHERE UserName = @UserName)
	BEGIN
		SET @ErrorCode = 'USER';
		SET @ErrorMessage = 'Username already exists';
		GOTO LogError;
	END
	
	IF EXISTS (SELECT TOP 1 100 FROM dbo.SiteUser WHERE UserEmail = @UserEmail)
	BEGIN
		SET @ErrorCode = 'USEREMAIL';
		SET @ErrorMessage = 'Email address already exists for another username';
		GOTO LogError;
	END
	
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRAN

	DECLARE @CreatedDate datetime2 = GETDATE();

	INSERT INTO [dbo].[SiteUser] (
		[UserName]
		, [Password]
		, [Hash]
		, [Salt]
		, [FirstName]
		, [LastName]
		, [MiddleInitial]
		, [UserEmail]
		, [UserImage]
		, [IsAdmin]
		, [CreatedDate]
		, [CreatedBy]
	)
	VALUES
	(
		@UserName
		, @Password
		, @Hash
		, @Salt
		, @FirstName
		, @LastName
		, @MiddleInitial
		, @UserEmail
		, @UserImage
		, @IsAdmin
		, @CreatedDate
		, @UserName
	)

	COMMIT TRAN
	
	SET @ErrorType = 'CT';
	SET @ErrorMessage = NULL;
	SET @ErrorCode = NULL;

	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;

RETURN
LogError: 
	SET @ErrorType = (CASE WHEN @ErrorType != 'ST' THEN @ErrorType ELSE 'ET' END);
	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;
RETURN
END TRY
BEGIN CATCH
	IF (@@TRANCOUNT > 0)
	BEGIN
		ROLLBACK TRAN
	END
	SET @ErrorCode = (SELECT ERROR_NUMBER());
	SET @ErrorType = 'ET'
	SET @ErrorMessage = (SELECT ERROR_MESSAGE());

	EXEC dbo.usp_insert_DraftLog 
		 @ErrorType OUTPUT
		 , @ErrorMessage OUTPUT
		 , @ErrorCode OUTPUT
		 , @SiteUserKey
		 , @Parameters
		 , @Action
		 , @Procedure;
				
	--RAISERROR(@ErrorMessage, 16, 1);
	RETURN;
END CATCH
GO

IF EXISTS (SELECT TOP 1 100 FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'usp_update_SiteUser')
BEGIN
	DROP PROC dbo.usp_update_SiteUser;
END
GO
CREATE PROC dbo.usp_update_SiteUser
@ErrorType nchar(2) = 'ST' OUTPUT
, @ErrorMessage nvarchar(max) = NULL OUTPUT
, @ErrorCode nvarchar(10) = NULL OUTPUT 
, @Action nvarchar(200)
, @SiteUserKey int

, @FirstName [nvarchar](50) 
, @LastName [nvarchar](50) 
, @MiddleInitial [nvarchar](1)
, @UserEmail [nvarchar](200) 
, @UserImage [varbinary](max) 
, @PK int
AS
DECLARE @Procedure nvarchar(200) = 'dbo.usp_update_SiteUser'
, @Parameters varchar(max) = '@PK: ' + ISNULL(CONVERT(varchar, @PK), 'NULL')
							+ '| @FirstName: ' + ISNULL(@FirstName, 'NULL')
							+ '| @LastName: ' + ISNULL(@LastName, 'NULL')
							+ '| @MiddleInitial: ' + ISNULL(@MiddleInitial, 'NULL')
							+ '| @UserEmail: ' + ISNULL(@UserEmail, 'NULL')
							+ '| @UserImage: ' + ISNULL(CONVERT(varchar, @UserImage), 'NULL');

BEGIN TRY

	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;

	IF (@ErrorType = 'ET')
	BEGIN
		RETURN;
	END

	IF (dbo.sp_user_exists(@SiteUserKey) = 0)
	BEGIN
		SET @ErrorCode = 'UNAUTH';
		SET @ErrorMessage = 'Unauthorized';
		GOTO LogError;
	END
	
	IF (dbo.sp_user_exists(@PK) = 0)
	BEGIN
		SET @ErrorCode = 'USEREXIST';
		SET @ErrorMessage = 'User account cannot be found';
		GOTO LogError;
	END

	IF EXISTS (SELECT TOP 1 100 FROM dbo.SiteUser WHERE UserEmail = @UserEmail)
	BEGIN
		SET @ErrorCode = 'USEREMAIL';
		SET @ErrorMessage = 'Email address already exists for another username';
		GOTO LogError;
	END
	
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRAN

	DECLARE @UpdatedDate datetime2 = GETDATE();

	UPDATE T
	SET
	[FirstName] = @FirstName
	, [LastName] = @LastName
	, [MiddleInitial] = @MiddleInitial
	, [UserEmail] = @UserEmail
	, [UserImage] = @UserImage
	, [UpdatedDate] = @UpdatedDate
	, [UpdatedBy] = dbo.sp_get_username(@SiteUserKey)
	FROM [dbo].[SiteUser] T 
	WHERE [PK] = @PK

	COMMIT TRAN
	
	SET @ErrorType = 'CT';

LogError: 
	SET @ErrorType = (CASE WHEN @ErrorType != 'ST' THEN @ErrorType ELSE 'ET' END);
	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;
RETURN
END TRY
BEGIN CATCH
	IF (@@TRANCOUNT > 0)
	BEGIN
		ROLLBACK TRAN
	END
	SET @ErrorCode = (SELECT ERROR_NUMBER());
	SET @ErrorType = 'ET'
	SET @ErrorMessage = (SELECT ERROR_MESSAGE());

	EXEC dbo.usp_insert_DraftLog 
		 @ErrorType OUTPUT
		 , @ErrorMessage OUTPUT
		 , @ErrorCode OUTPUT
		 , @SiteUserKey
		 , @Parameters
		 , @Action
		 , @Procedure;
				
	--RAISERROR(@ErrorMessage, 16, 1);
	RETURN;
END CATCH
GO

IF EXISTS (SELECT TOP 1 100 FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'usp_updatepassword_SiteUser')
BEGIN
	DROP PROC dbo.usp_updatepassword_SiteUser;
END
GO
CREATE PROC dbo.usp_updatepassword_SiteUser
@ErrorType nchar(2) = 'ST' OUTPUT
, @ErrorMessage nvarchar(max) = NULL OUTPUT
, @ErrorCode nvarchar(10) = NULL OUTPUT 
, @Action nvarchar(200)
, @SiteUserKey int NULL OUTPUT

, @Password [varbinary](128) 
, @Hash [varbinary](128) 
, @Salt [varchar](32) 
, @PK int
AS
DECLARE @Procedure nvarchar(200) = 'dbo.usp_updatepassword_SiteUser'
, @Parameters varchar(max) = '@PK: ' + ISNULL(CONVERT(varchar, @PK), 'NULL');

BEGIN TRY

	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;

	IF (@ErrorType = 'ET')
	BEGIN
		RETURN;
	END
	
	IF (dbo.sp_user_exists(@SiteUserKey) = 0)
	BEGIN
		SET @ErrorCode = 'UNAUTH';
		SET @ErrorMessage = 'Unauthorized';
		GOTO LogError;
	END
	
	IF (dbo.sp_user_exists(@PK) = 0)
	BEGIN
		SET @ErrorCode = 'USEREXIST';
		SET @ErrorMessage = 'User account cannot be found';
		GOTO LogError;
	END
	
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRAN

	DECLARE @UpdatedDate datetime2 = GETDATE();

	UPDATE T
	SET
	[Password] = @Password
	, [Hash] = @Hash
	, [Salt] = @Salt
	, [UpdatedDate] = @UpdatedDate
	, [UpdatedBy] = dbo.sp_get_username(@SiteUserKey)
	FROM [dbo].[SiteUser] T 
	WHERE [PK] = @PK

	COMMIT TRAN
	
	SET @ErrorType = 'CT';

	LogError: 
		SET @ErrorType = (CASE WHEN @ErrorType != 'ST' THEN @ErrorType ELSE 'ET' END);
		EXEC dbo.usp_insert_DraftLog 
		@ErrorType OUTPUT
		, @ErrorMessage OUTPUT
		, @ErrorCode OUTPUT
		, @SiteUserKey
		, @Parameters
		, @Action
		, @Procedure;
	RETURN
END TRY
BEGIN CATCH
	IF (@@TRANCOUNT > 0)
	BEGIN
		ROLLBACK TRAN
	END
	SET @ErrorCode = (SELECT ERROR_NUMBER());
	SET @ErrorType = 'ET'
	SET @ErrorMessage = (SELECT ERROR_MESSAGE());

	EXEC dbo.usp_insert_DraftLog 
		 @ErrorType OUTPUT
		 , @ErrorMessage OUTPUT
		 , @ErrorCode OUTPUT
		 , @SiteUserKey
		 , @Parameters
		 , @Action
		 , @Procedure;
				
	--RAISERROR(@ErrorMessage, 16, 1);
	RETURN;
END CATCH
GO

IF EXISTS (SELECT TOP 1 100 FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'usp_activate_SiteUser')
BEGIN
	DROP PROC dbo.usp_activate_SiteUser;
END
GO
CREATE PROC dbo.usp_activate_SiteUser
@ErrorType nchar(2) = 'ST' OUTPUT
, @ErrorMessage nvarchar(max) = NULL OUTPUT
, @ErrorCode nvarchar(10) = NULL OUTPUT 
, @Action nvarchar(200)
, @SiteUserKey int NULL OUTPUT

, @PK int
AS
DECLARE @Procedure nvarchar(200) = 'dbo.usp_activate_SiteUser'
, @Parameters varchar(max) = '@PK: ' + ISNULL(CONVERT(varchar, @PK), 'NULL');

BEGIN TRY

	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;

	IF (@ErrorType = 'ET')
	BEGIN
		RETURN;
	END
	
	IF (dbo.sp_user_exists(@SiteUserKey) = 0)
	BEGIN
		SET @ErrorCode = 'UNAUTH';
		SET @ErrorMessage = 'Unauthorized';
		GOTO LogError;
	END
	
	IF (dbo.sp_user_exists(@PK) = 0)
	BEGIN
		SET @ErrorCode = 'USEREXIST';
		SET @ErrorMessage = 'User account cannot be found';
		GOTO LogError;
	END
	
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRAN

	DECLARE @UpdatedDate datetime2 = GETDATE();

	UPDATE T
	SET
	[InActive] = 0
	, [UpdatedDate] = @UpdatedDate
	, [UpdatedBy] = dbo.sp_get_username(@SiteUserKey)
	FROM [dbo].[SiteUser] T 
	WHERE [PK] = @PK

	COMMIT TRAN
	
	SET @ErrorType = 'CT';
	SET @ErrorMessage = 'Account Reactivated.';

LogError: 
	SET @ErrorType = (CASE WHEN @ErrorType != 'ST' THEN @ErrorType ELSE 'ET' END);
	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;
RETURN
END TRY
BEGIN CATCH
	IF (@@TRANCOUNT > 0)
	BEGIN
		ROLLBACK TRAN
	END
	SET @ErrorCode = (SELECT ERROR_NUMBER());
	SET @ErrorType = 'ET'
	SET @ErrorMessage = (SELECT ERROR_MESSAGE());

	EXEC dbo.usp_insert_DraftLog 
		 @ErrorType OUTPUT
		 , @ErrorMessage OUTPUT
		 , @ErrorCode OUTPUT
		 , @SiteUserKey
		 , @Parameters
		 , @Action
		 , @Procedure;
				
	--RAISERROR(@ErrorMessage, 16, 1);
	RETURN;
END CATCH
GO

IF EXISTS (SELECT TOP 1 100 FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'usp_deactivate_SiteUser')
BEGIN
	DROP PROC dbo.usp_deactivate_SiteUser;
END
GO
CREATE PROC dbo.usp_deactivate_SiteUser
@ErrorType nchar(2) = 'ST' OUTPUT
, @ErrorMessage nvarchar(max) = NULL OUTPUT
, @ErrorCode nvarchar(10) = NULL OUTPUT 
, @Action nvarchar(200)
, @SiteUserKey int NULL OUTPUT

, @PK int
AS
DECLARE @Procedure nvarchar(200) = 'dbo.usp_deactivate_SiteUser'
, @Parameters varchar(max) = '@PK: ' + ISNULL(CONVERT(varchar, @PK), 'NULL');

BEGIN TRY

	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;

	IF (@ErrorType = 'ET')
	BEGIN
		RETURN;
	END
	
	IF (dbo.sp_user_exists(@SiteUserKey) = 0)
	BEGIN
		SET @ErrorCode = 'UNAUTH';
		SET @ErrorMessage = 'Unauthorized';
		GOTO LogError;
	END
	
	IF (dbo.sp_user_authorized(@SiteUserKey, 1) = 0)
	BEGIN
		SET @ErrorCode = 'UNAUTH';
		SET @ErrorMessage = 'Unauthorized';
		GOTO LogError;
	END
		
	IF (dbo.sp_user_exists(@PK) = 0)
	BEGIN
		SET @ErrorCode = 'USEREXIST';
		SET @ErrorMessage = 'User account cannot be found';
		GOTO LogError;
	END
	
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRAN

	DECLARE @UpdatedDate datetime2 = GETDATE();

	UPDATE T
	SET
	[InActive] = 1
	, [UpdatedDate] = @UpdatedDate
	, [UpdatedBy] = dbo.sp_get_username(@SiteUserKey)
	FROM [dbo].[SiteUser] T 
	WHERE [PK] = @PK

	COMMIT TRAN
	
	SET @ErrorType = 'CT';
	SET @ErrorMessage = 'Account deactivated.';

LogError: 
	SET @ErrorType = (CASE WHEN @ErrorType != 'ST' THEN @ErrorType ELSE 'ET' END);
	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;
RETURN
END TRY
BEGIN CATCH
	IF (@@TRANCOUNT > 0)
	BEGIN
		ROLLBACK TRAN
	END
	SET @ErrorCode = (SELECT ERROR_NUMBER());
	SET @ErrorType = 'ET'
	SET @ErrorMessage = (SELECT ERROR_MESSAGE());

	EXEC dbo.usp_insert_DraftLog 
		 @ErrorType OUTPUT
		 , @ErrorMessage OUTPUT
		 , @ErrorCode OUTPUT
		 , @SiteUserKey
		 , @Parameters
		 , @Action
		 , @Procedure;
				
	--RAISERROR(@ErrorMessage, 16, 1);
	RETURN;
END CATCH
GO

IF EXISTS (SELECT TOP 1 100 FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'usp_delete_SiteUser')
BEGIN
	DROP PROC dbo.usp_delete_SiteUser;
END
GO
CREATE PROC dbo.usp_delete_SiteUser
@ErrorType nchar(2) = 'ST' OUTPUT
, @ErrorMessage nvarchar(max) = NULL OUTPUT
, @ErrorCode nvarchar(10) = NULL OUTPUT 
, @Action nvarchar(200)
, @SiteUserKey int NULL OUTPUT

, @PK int
AS
DECLARE @Procedure nvarchar(200) = 'dbo.usp_delete_SiteUser'
, @Parameters varchar(max) = '@PK: ' + ISNULL(CONVERT(varchar, @PK), 'NULL');

BEGIN TRY

	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;

	IF (@ErrorType = 'ET')
	BEGIN
		RETURN;
	END
	
	IF (dbo.sp_user_exists(@SiteUserKey) = 0)
	BEGIN
		SET @ErrorCode = 'UNAUTH';
		SET @ErrorMessage = 'Unauthorized';
		GOTO LogError;
	END
	
	IF (dbo.sp_user_authorized(@SiteUserKey, 1) = 0)
	BEGIN
		SET @ErrorCode = 'UNAUTH';
		SET @ErrorMessage = 'Unauthorized';
		GOTO LogError;
	END
		
	IF (dbo.sp_user_exists(@PK) = 0)
	BEGIN
		SET @ErrorCode = 'USEREXIST';
		SET @ErrorMessage = 'User account cannot be found';
		GOTO LogError;
	END
	
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRAN

	DECLARE @UpdatedDate datetime2 = GETDATE();

	DELETE
	FROM [dbo].[SiteUser] 
	WHERE [PK] = @PK

	COMMIT TRAN
	
	SET @ErrorType = 'CT';
	SET @ErrorMessage = 'Account deleted.';

LogError: 
	SET @ErrorType = (CASE WHEN @ErrorType != 'ST' THEN @ErrorType ELSE 'ET' END);
	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;
RETURN
END TRY
BEGIN CATCH
	IF (@@TRANCOUNT > 0)
	BEGIN
		ROLLBACK TRAN
	END
	SET @ErrorCode = (SELECT ERROR_NUMBER());
	SET @ErrorType = 'ET'
	SET @ErrorMessage = (SELECT ERROR_MESSAGE());

	EXEC dbo.usp_insert_DraftLog 
		 @ErrorType OUTPUT
		 , @ErrorMessage OUTPUT
		 , @ErrorCode OUTPUT
		 , @SiteUserKey
		 , @Parameters
		 , @Action
		 , @Procedure;
				
	--RAISERROR(@ErrorMessage, 16, 1);
	RETURN;
END CATCH
GO