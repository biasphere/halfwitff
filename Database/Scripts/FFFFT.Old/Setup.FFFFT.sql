﻿USE FFFFT
GO

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'vew')
BEGIN
	EXEC('CREATE SCHEMA [vew] AUTHORIZATION [dbo]');
END
GO
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'rpt')
BEGIN
	EXEC('CREATE SCHEMA [rpt] AUTHORIZATION [dbo]');
END
GO
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'tabletype')
BEGIN
	EXEC('CREATE SCHEMA [tabletype] AUTHORIZATION [dbo]');
END
GO
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'lu')
BEGIN
	EXEC('CREATE SCHEMA [lu] AUTHORIZATION [dbo]');
END
GO

BEGIN --drop views
	DECLARE @name nvarchar(max);
	DECLARE @SQL nvarchar(max);

	SELECT @name = (SELECT TOP 1 name FROM sys.views where SCHEMA_NAME(schema_id) = N'vew' ORDER BY [name] ASC);
	WHILE @name IS NOT NULL
	BEGIN	
		SELECT @SQL = 'DROP VIEW [vew].[' + RTRIM(@name) +']';
		EXEC (@SQL)
		PRINT 'Dropped View: vew.' + @name;
		SELECT @name = (SELECT TOP 1 name FROM sys.views where SCHEMA_NAME(schema_id) = N'vew' and [name] > @name ORDER BY [name] ASC);
	END
END
GO

BEGIN --drop procs
	DECLARE @name nvarchar(max);
	DECLARE @SQL nvarchar(max);
	SELECT @name = (SELECT TOP 1 name FROM sys.procedures where SCHEMA_NAME(schema_id) = N'dbo' ORDER BY [name]);
	WHILE @name IS NOT NULL
	BEGIN	
		SELECT @SQL = 'DROP PROCEDURE [dbo].[' + RTRIM(@name) +']';
		EXEC (@SQL)
		PRINT 'Dropped Stored Procedure: dbo.' + @name;
		SELECT @name = (SELECT TOP 1 name FROM sys.procedures where SCHEMA_NAME(schema_id) = N'dbo' and [name] > @name ORDER BY [name]);	
	END
END
GO

BEGIN /* Drop all functions */
	DECLARE @name nvarchar(max);
	DECLARE @SQL nvarchar(max);
	SELECT @name = (SELECT TOP 1 o.[name] FROM sys.objects o WHERE [type] IN (N'FN', N'IF', N'TF', N'FS', N'FT') AND SCHEMA_NAME(schema_id) = 'dbo' ORDER BY [name]);
	WHILE @name IS NOT NULL
	BEGIN
		SELECT @SQL = 'DROP FUNCTION [dbo].[' + RTRIM(@name) +']'
		EXEC (@SQL)
		PRINT 'Dropped Function: ' + @name
		SELECT @name = (SELECT TOP 1 o.[name] FROM sys.objects o WHERE [type] IN (N'FN', N'IF', N'TF', N'FS', N'FT') AND SCHEMA_NAME(schema_id) = 'dbo' AND o.[name] > @name ORDER BY o.[name]);
	END
END
GO

--drop all the key constraints
BEGIN
--http://stackoverflow.com/questions/536350/drop-all-the-tables-stored-procedures-triggers-constraints-and-all-the-depend
	DECLARE @name nvarchar(max);
	DECLARE @SQL nvarchar(max);
	DECLARE @constraint VARCHAR(254);
	SELECT @name = (SELECT TOP 1 TABLE_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE constraint_catalog=DB_NAME() and TABLE_SCHEMA = 'dbo' AND CONSTRAINT_TYPE = 'FOREIGN KEY' ORDER BY TABLE_NAME);
	WHILE @name IS NOT NULL
	BEGIN
		SELECT @constraint = (SELECT TOP 1 CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE constraint_catalog=DB_NAME() and TABLE_SCHEMA = 'dbo' AND CONSTRAINT_TYPE = 'FOREIGN KEY' AND TABLE_NAME = @name ORDER BY CONSTRAINT_NAME)
		WHILE @constraint IS NOT NULL
		BEGIN
			SELECT @SQL = 'ALTER TABLE [dbo].[' + RTRIM(@name) +'] DROP CONSTRAINT [' + RTRIM(@constraint) +']';
			EXEC (@SQL);
			PRINT 'Dropped FK Constraint: ' + @constraint + ' on ' + @name;
			SELECT @constraint = (SELECT TOP 1 CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE constraint_catalog=DB_NAME() AND CONSTRAINT_TYPE = 'FOREIGN KEY' AND CONSTRAINT_NAME <> @constraint and TABLE_SCHEMA = 'dbo' AND TABLE_NAME = @name ORDER BY CONSTRAINT_NAME);
		END
		SELECT @name = (SELECT TOP 1 TABLE_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE constraint_catalog=DB_NAME() and TABLE_SCHEMA = 'dbo' AND CONSTRAINT_TYPE = 'FOREIGN KEY' ORDER BY TABLE_NAME);
	END
END
GO

BEGIN --drop tables
	DECLARE @name nvarchar(max);
	DECLARE @SQL nvarchar(max);
	SELECT @name = (SELECT TOP 1 name FROM sys.tables where SCHEMA_NAME(schema_id) = N'dbo' ORDER BY [name] ASC);
	WHILE @name IS NOT NULL
	BEGIN	
		SELECT @SQL = 'DROP TABLE [dbo].[' + RTRIM(@name) +']';
		EXEC (@SQL)
		PRINT 'Dropped Table: dbo.' + @name;
		SELECT @name = (SELECT TOP 1 name FROM sys.tables where SCHEMA_NAME(schema_id) = N'dbo' and [name] > @name ORDER BY [name] ASC);
	END
END
GO

BEGIN --drop tables [lu]
	DECLARE @name nvarchar(max);
	DECLARE @SQL nvarchar(max);
	SELECT @name = (SELECT TOP 1 name FROM sys.tables where SCHEMA_NAME(schema_id) = N'lu' ORDER BY [name] ASC);
	WHILE @name IS NOT NULL
	BEGIN	
		SELECT @SQL = 'DROP TABLE [lu].[' + RTRIM(@name) +']';
		EXEC (@SQL)
		PRINT 'Dropped Table: lu.' + @name;
		SELECT @name = (SELECT TOP 1 name FROM sys.tables where SCHEMA_NAME(schema_id) = N'lu' and [name] > @name ORDER BY [name] ASC);
	END
END
GO

--drop all types
BEGIN
	DECLARE @name nvarchar(max);
	DECLARE @SQL nvarchar(max);
	DECLARE @typename nvarchar(50) = '';
	SET @typename = (SELECT TOP 1 name FROM sys.types WHERE is_table_type = 1 AND SCHEMA_NAME([schema_id]) = 'tabletype' ORDER BY name ASC);
	WHILE @typename IS NOT NULL
	BEGIN
		SET @sql = 'DROP TYPE [tabletype].[' + @typename + '];';
		EXEC (@sql);
		SET @typename = (SELECT TOP 1 name FROM sys.types WHERE is_table_type = 1 AND name > @typename ORDER BY name ASC);
	END
END
GO
--dbos 
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'Team')
BEGIN
	DROP TABLE dbo.[Team]
END
GO
CREATE TABLE [dbo].[Team] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[City] [nvarchar](50) NOT NULL,
	[TeamName] [nvarchar](50) NOT NULL,
	[FullName] [nvarchar](100) NOT NULL,
	[Url] [nvarchar](400) NOT NULL,
	[SiteKey] nchar(1) NOT NULL,
	[ByeWeek] int NOT NULL,
	[Inactive] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(6) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(6) NULL
	CONSTRAINT [PK_Team] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'Player')
BEGIN
	DROP TABLE dbo.[Player]
END
GO
CREATE TABLE [dbo].[Player] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[SourceID] int NOT NULL,
	[TeamKey] int NOT NULL,
	[Height] int NOT NULL,
	[Weight] int NOT NULL,
	[Experience] int NOT NULL,
	[Age] int NOT NULL,
	[JerseyNumber] int NOT NULL,
	[PositionKey] [nvarchar](5) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[College] [nvarchar](100) NOT NULL,
	[Url] [nvarchar](400) NOT NULL,
	[SiteKey] nchar(1) NOT NULL,
	[Inactive] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(6) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(6) NULL
	CONSTRAINT [PK_Player] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
--DROP TABLE [dbo].[PlayerStats]
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'PlayerStatsNFL')
BEGIN
	DROP TABLE dbo.[PlayerStatsNFL]
END
GO
CREATE TABLE [dbo].[PlayerStatsNFL] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[PlayerKey] int NOT NULL,
	[Year] int NOT NULL,
	[GamesPlayed] int NOT NULL,

	[Rushing_Yards] [decimal](10,2) NOT NULL,
	[Rushing_Att] int NOT NULL,
	[Rushing_TD] int NOT NULL,
	[Rushing_Fumbles] int NOT NULL,
	
	[Receiving_Yards] [decimal](10,2) NOT NULL,
	[Receiving_Rec] int NOT NULL,
	[Receiving_Att] int NOT NULL,
	[Receiving_TD] int NOT NULL,
	[Receiving_Fumbles] int NOT NULL,
	
	[Passing_Yards] [decimal](10,2) NOT NULL,
	[Passing_Comp] int NOT NULL,
	[Passing_Att] int NOT NULL,
	[Passing_TD] int NOT NULL,
	[Passing_Int] int NOT NULL,
	[Passing_Rating] [decimal](10,2) NOT NULL,
	[Passing_Fumbles] int NOT NULL,
	
	[Kicking_FGM] int NOT NULL,
	[Kicking_FGA] int NOT NULL,
	[Kicking_119] nvarchar(8) NOT NULL,
	[Kicking_2029] nvarchar(8) NOT NULL,
	[Kicking_3039] nvarchar(8) NOT NULL,
	[Kicking_4049] nvarchar(8) NOT NULL,
	[Kicking_50Plus] nvarchar(8) NOT NULL,
	[Kicking_Long] [decimal](10,2) NOT NULL,
	[Kicking_XPM] int NOT NULL,
	[Kicking_XPA] int NOT NULL,
	[Kicking_Points] int NOT NULL,
	
	[Voided] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(6) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(6) NULL
	CONSTRAINT [PK_PlayerStatsNFL] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
--DROP TABLE [dbo].[TeamStats]
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'TeamStatsNFL')
BEGIN
	DROP TABLE dbo.[TeamStatsNFL]
END
GO
CREATE TABLE [dbo].[TeamStatsNFL] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[TeamKey] int NOT NULL,
	[Year] int NOT NULL,
	
	[Tackles] int NOT NULL,
	[Sacks] int NOT NULL,
	[TacklesForLoss] int NOT NULL,

	[PassesDefensed] int NOT NULL,
	[Interceptions] int NOT NULL,
	[Interceptions_TD] int NOT NULL,
	
	[Fumbles] int NOT NULL,
	[Fumbles_TD] int NOT NULL,
	[Fumbles_Recovered] int NOT NULL,

	[BlockedKicks] int NOT NULL,
	[Kickoff_Avg] [decimal](10,2) NOT NULL,
	[Kickoff_TD] int NOT NULL,
	[Punt_Avg] [decimal](10,2) NOT NULL,
	[Punt_TD] int NOT NULL,
	
	[Voided] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(6) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(6) NULL
	CONSTRAINT [PK_TeamStatsNFL] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'PlayerData')
BEGIN
	DROP TABLE dbo.[PlayerData]
END
GO
CREATE TABLE [dbo].[PlayerData] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[PlayerKey] int NOT NULL,
	[DataTypeKey] [nchar](1) NOT NULL,
	[Data] [varbinary](max) NOT NULL,
	[Voided] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(6) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(6) NULL
	CONSTRAINT [PK_PlayerData] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO

--creating the interactive tables
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'DraftLeague')
BEGIN
	DROP TABLE dbo.[DraftLeague]
END
GO
CREATE TABLE [dbo].[DraftLeague] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[LeagueName] nvarchar(50) NOT NULL,
	[LeagueMotto] nvarchar(400) NULL,
	[LeagueSportCode] nvarchar(5) NOT NULL,
	[LeagueBuyIn] money NULL,
	[LeagueTeamCount] int NULL,
	[LeagueMaxPlayersPerTeam] int NULL,
	[LeagueHash] [varbinary](128) NOT NULL,
	[LeagueImage] [varbinary](max) NULL,
	[Voided] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(50) NULL
	CONSTRAINT [PK_DraftLeague] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'SiteUser')
BEGIN
	DROP TABLE dbo.[SiteUser]
END
GO
CREATE TABLE [dbo].[SiteUser] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[UserName] nvarchar(50) NOT NULL,
	[Password] [varbinary](128) NULL,
	[Hash] [varbinary](128) NULL,
	[Salt] [varchar](32) NULL,
	[FirstName] nvarchar(50) NOT NULL,
	[LastName] nvarchar(50) NOT NULL,
	[MiddleInitial] nvarchar(1) NULL,
	[UserEmail] nvarchar(200) NOT NULL,
	[UserImage] varbinary(max) NULL,
	[IsAdmin] bit NOT NULL DEFAULT(0),
	[Inactive] bit NOT NULL DEFAULT(0),
	[Voided] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(50) NULL
	CONSTRAINT [PK_SiteUser] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'DraftLeaguePayout')
BEGIN
	DROP TABLE dbo.[DraftLeaguePayout]
END
GO
CREATE TABLE [dbo].[DraftLeaguePayout] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[DraftLeagueKey] int NOT NULL,
	[Ranking] int NOT NULL,
	[Payout] money NULL,
	[Voided] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(50) NULL
	CONSTRAINT [PK_DraftLeaguePayout] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'DraftUser')
BEGIN
	DROP TABLE dbo.[DraftUser]
END
GO
CREATE TABLE [dbo].[DraftUser] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[SiteUserKey] int NOT NULL,
	[DraftLeagueKey] int NOT NULL,
	[DraftImage] [varbinary](max) NOT NULL,
	[IsCommissioner] bit NOT NULL DEFAULT(0),
	[IsCoCommissioner] bit NOT NULL DEFAULT(0),
	[IsPieceOfShit] bit NOT NULL DEFAULT(1),
	[Voided] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(50) NULL
	CONSTRAINT [PK_DraftUser] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'DraftTeam')
BEGIN
	DROP TABLE dbo.[DraftTeam]
END
GO
CREATE TABLE [dbo].[DraftTeam] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[DraftUserKey] int NOT NULL,
	[TeamName] nvarchar(50) NOT NULL,
	[TeamImage] [varbinary](max) NULL,
	[TeamMotto] nvarchar(100) NULL,
	[Voided] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(50) NULL
	CONSTRAINT [PK_DraftTeam] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'PlayersDrafted')
BEGIN
	DROP TABLE dbo.[PlayersDrafted]
END
GO
CREATE TABLE [dbo].[PlayersDrafted] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[PlayerKey] int NOT NULL,
	[DraftTeamKey] int NOT NULL,
	[AmountPaid] money NOT NULL,
	[Voided] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(50) NULL
	CONSTRAINT [PK_PlayersDrafted] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'TeamsDrafted')
BEGIN
	DROP TABLE dbo.[TeamsDrafted]
END
GO
CREATE TABLE [dbo].[TeamsDrafted] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[TeamKey] int NOT NULL,
	[DraftTeamKey] int NOT NULL,
	[AmountPaid] money NOT NULL,
	[Voided] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(50) NULL
	CONSTRAINT [PK_TeamsDrafted] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'SiteUrl')
BEGIN
	DROP TABLE [dbo].[SiteUrl]
END
GO
CREATE TABLE [dbo].[SiteUrl] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[SiteKey] char(1) NOT NULL,
	[UrlTypeKey] char(1) NOT NULL,
	[Url] varchar(500) NOT NULL,
	[Inactive] bit NOT NULL DEFAULT(0),
	[Voided] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(50) NULL
	CONSTRAINT [PK_SiteURL] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO

IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'DraftLog')
BEGIN
	DROP TABLE dbo.[DraftLog]
END
GO
CREATE TABLE [dbo].[DraftLog] (
	[PK] [bigint] NOT NULL IDENTITY(1,1),
	[SiteUserKey] int NOT NULL,
	[Procedure] nvarchar(200) NOT NULL,
	[Action] nvarchar(200) NOT NULL,
	[Parameters] varchar(max) NULL,
	[Status] nvarchar(3) NOT NULL,
	[ErrorCode] nvarchar(10) NULL,
	[ErrorMessage] varchar(max) NULL,
	[DateTimeStamp] datetime2 NOT NULL
	CONSTRAINT [PK_DraftLog] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO

--lus
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'lu' AND [name] = 'LeagueSportType')
BEGIN
	DROP TABLE [lu].[LeagueSportType]
END
GO
CREATE TABLE [lu].[LeagueSportType] (
	[Code] nvarchar(5) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Voided] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(6) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(6) NULL
	CONSTRAINT [PK_LeagueSportType] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'lu' AND [name] = 'Position')
BEGIN
	DROP TABLE [lu].[Position]
END
GO
CREATE TABLE [lu].[Position] (
	[Code] [nvarchar](5) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Voided] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(6) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(6) NULL
	CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'lu' AND [name] = 'PlayerDataType')
BEGIN
	DROP TABLE [lu].[PlayerDataType]
END
GO
CREATE TABLE [lu].[PlayerDataType] (
	[Code] [nchar](1) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Voided] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(6) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(6) NULL
	CONSTRAINT [PK_PlayerDataType] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'lu' AND [name] = 'Site')
BEGIN
	DROP TABLE [lu].[Site]
END
GO
CREATE TABLE [lu].[Site] (
	[Code] [nchar](1) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Voided] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(6) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(6) NULL
	CONSTRAINT [PK_Site] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF EXISTS (SELECT * FROM sys.tables WHERE SCHEMA_NAME(schema_id) = 'lu' AND [name] = 'UrlTypes')
BEGIN
	DROP TABLE [lu].[UrlTypes]
END
GO
CREATE TABLE [lu].[UrlTypes] (
	[Code] [nchar](1) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Voided] bit NOT NULL DEFAULT(0),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] nvarchar(6) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] nvarchar(6) NULL
	CONSTRAINT [PK_UrlTypes] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO


--FOREIGN KEYS
ALTER TABLE [dbo].[DraftLeague] WITH NOCHECK ADD CONSTRAINT [FK_DraftLeague_LeagueSportType] 
FOREIGN KEY([LeagueSportCode])
REFERENCES [lu].[LeagueSportType] ([Code])
ALTER TABLE [dbo].[DraftLeague] CHECK CONSTRAINT [FK_DraftLeague_LeagueSportType];
GO
ALTER TABLE [dbo].[DraftLeaguePayout] WITH NOCHECK ADD CONSTRAINT [FK_DraftLeaguePayout_DraftLeague] 
FOREIGN KEY([DraftLeagueKey])
REFERENCES [dbo].[DraftLeague] ([PK])
ALTER TABLE [dbo].[DraftLeaguePayout] CHECK CONSTRAINT [FK_DraftLeaguePayout_DraftLeague];
GO
ALTER TABLE [dbo].[DraftUser] WITH NOCHECK ADD CONSTRAINT [FK_DraftUser_DraftLeague] 
FOREIGN KEY([DraftLeagueKey])
REFERENCES [dbo].[DraftLeague] ([PK])
ALTER TABLE [dbo].[DraftUser] CHECK CONSTRAINT [FK_DraftUser_DraftLeague];
GO
ALTER TABLE [dbo].[DraftUser] WITH NOCHECK ADD CONSTRAINT [FK_DraftUser_SiteUser] 
FOREIGN KEY([SiteUserKey])
REFERENCES [dbo].[SiteUser] ([PK])
ALTER TABLE [dbo].[DraftUser] CHECK CONSTRAINT [FK_DraftUser_SiteUser];
GO
ALTER TABLE [dbo].[DraftLog] WITH NOCHECK ADD CONSTRAINT [FK_DraftLog_SiteUser] 
FOREIGN KEY([SiteUserKey])
REFERENCES [dbo].[SiteUser] ([PK])
ALTER TABLE [dbo].[DraftLog] CHECK CONSTRAINT [FK_DraftLog_SiteUser];
GO
ALTER TABLE [dbo].[DraftTeam] WITH NOCHECK ADD CONSTRAINT [FK_DraftTeam_DraftUser] 
FOREIGN KEY([DraftUserKey])
REFERENCES [dbo].[DraftUser] ([PK])
ALTER TABLE [dbo].[DraftTeam] CHECK CONSTRAINT [FK_DraftTeam_DraftUser];
GO
ALTER TABLE [dbo].[PlayersDrafted] WITH NOCHECK ADD CONSTRAINT [FK_PlayersDrafted_DraftTeamKey] 
FOREIGN KEY([DraftTeamKey])
REFERENCES [dbo].[DraftTeam] ([PK])
ALTER TABLE [dbo].[PlayersDrafted] CHECK CONSTRAINT [FK_PlayersDrafted_DraftTeamKey];
GO
ALTER TABLE [dbo].[PlayersDrafted] WITH NOCHECK ADD CONSTRAINT [FK_PlayersDrafted_Player] 
FOREIGN KEY([PlayerKey])
REFERENCES [dbo].[Player] ([PK])
ALTER TABLE [dbo].[PlayersDrafted] CHECK CONSTRAINT [FK_PlayersDrafted_Player];
GO
ALTER TABLE [dbo].[TeamsDrafted] WITH NOCHECK ADD CONSTRAINT [FK_TeamsDrafted_DraftTeamKey] 
FOREIGN KEY([DraftTeamKey])
REFERENCES [dbo].[DraftTeam] ([PK])
ALTER TABLE [dbo].[TeamsDrafted] CHECK CONSTRAINT [FK_TeamsDrafted_DraftTeamKey];
GO
ALTER TABLE [dbo].[TeamsDrafted] WITH NOCHECK ADD CONSTRAINT [FK_TeamsDrafted_Team] 
FOREIGN KEY([TeamKey])
REFERENCES [dbo].[Player] ([PK])
ALTER TABLE [dbo].[TeamsDrafted] CHECK CONSTRAINT [FK_TeamsDrafted_Team];
GO

--FOREIGHN KEYS
IF EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_TeamSite')
BEGIN
	ALTER TABLE [dbo].[Team] DROP CONSTRAINT [FK_TeamSite];
END
GO
ALTER TABLE [dbo].[Team] WITH NOCHECK ADD CONSTRAINT [FK_TeamSite] 
FOREIGN KEY([SiteKey])
REFERENCES [lu].[Site] ([Code])
ALTER TABLE [dbo].[Team] CHECK CONSTRAINT [FK_TeamSite];
GO
IF EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_PlayerSite')
BEGIN
	ALTER TABLE [dbo].[Player] DROP CONSTRAINT [FK_PlayerSite];
END
GO
ALTER TABLE [dbo].[Player] WITH NOCHECK ADD CONSTRAINT [FK_PlayerSite] 
FOREIGN KEY([SiteKey])
REFERENCES [lu].[Site] ([Code])
ALTER TABLE [dbo].[Player] CHECK CONSTRAINT [FK_PlayerSite];
GO
IF EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_TeamStatsTeam')
BEGIN
	ALTER TABLE [dbo].[TeamStats] DROP CONSTRAINT [FK_TeamStatsTeam];
END
GO
ALTER TABLE [dbo].[TeamStatsNFL] WITH NOCHECK ADD CONSTRAINT [FK_TeamStatsNFLTeam] 
FOREIGN KEY([TeamKey])
REFERENCES [dbo].[Team] ([PK])
ALTER TABLE [dbo].[TeamStatsNFL] CHECK CONSTRAINT [FK_TeamStatsNFLTeam];
GO
IF EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_PlayerTeam')
BEGIN
	ALTER TABLE [dbo].[Player] DROP CONSTRAINT [FK_PlayerTeam];
END
GO
ALTER TABLE [dbo].[Player] WITH NOCHECK ADD CONSTRAINT [FK_PlayerTeam] 
FOREIGN KEY([TeamKey])
REFERENCES [dbo].[Team] ([PK])
ALTER TABLE [dbo].[Player] CHECK CONSTRAINT [FK_PlayerTeam];
GO
IF EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_PlayerDataPlayer')
BEGIN
	ALTER TABLE [dbo].[PlayerData] DROP CONSTRAINT [FK_PlayerDataPlayer];
END
GO
ALTER TABLE [dbo].[PlayerData] WITH NOCHECK ADD CONSTRAINT [FK_PlayerDataPlayer] 
FOREIGN KEY([PlayerKey])
REFERENCES [dbo].[Player] ([PK])
ALTER TABLE [dbo].[PlayerData] CHECK CONSTRAINT [FK_PlayerDataPlayer];
GO
IF EXISTS (SELECT * FROM sys.foreign_keys WHERE name = 'FK_PlayerStatsPlayer')
BEGIN
	ALTER TABLE [dbo].[PlayerStats] DROP CONSTRAINT [FK_PlayerStatsPlayer];
END
GO
ALTER TABLE [dbo].[PlayerStatsNFL] WITH NOCHECK ADD CONSTRAINT [FK_PlayerStatsNFLPlayer] 
FOREIGN KEY([PlayerKey])
REFERENCES [dbo].[Player] ([PK])
ALTER TABLE [dbo].[PlayerStatsNFL] CHECK CONSTRAINT [FK_PlayerStatsNFLPlayer];
GO

--uniques
IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'UIX_PlayerData_PlayerKeyDataTypeKey')
BEGIN
	 DROP INDEX [UIX_PlayerData_PlayerKeyDataTypeKey] ON [dbo].[PlayerData];
END
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_PlayerData_PlayerKeyDataTypeKey
ON dbo.PlayerData (PlayerKey, DataTypeKey)
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'UIX_LeagueSportType_Description')
BEGIN
	DROP INDEX [UIX_LeagueSportType_Description] ON [dbo].[LeagueSportType];
END
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_LeagueSportType_Description
ON [lu].[LeagueSportType] ([Description])
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'UIX_SiteUser_UserName')
BEGIN
	DROP INDEX [UIX_SiteUser_UserName] ON [dbo].[SiteUser];
END
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_SiteUser_UserName
ON [dbo].[SiteUser] ([UserName])
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'UIX_SiteUser_UserEmail')
BEGIN
	DROP INDEX [UIX_SiteUser_UserEmail] ON [dbo].[SiteUser];
END
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_SiteUser_UserEmail
ON [dbo].[SiteUser] ([UserEmail])
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'UIX_DraftLeague_LeagueSportCodeLeagueHash')
BEGIN
	DROP INDEX [UIX_DraftLeague_LeagueSportCodeLeagueHash] ON [dbo].[DraftLeague];
END
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_DraftLeague_LeagueSportCodeLeagueHash
ON [dbo].[DraftLeague] ([LeagueSportCode], [LeagueHash])
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'UIX_DraftUser_SiteUserKeyDraftLeagueKey')
BEGIN
	DROP INDEX [UIX_DraftUser_SiteUserKeyDraftLeagueKey] ON [dbo].[DraftUser];
END
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_DraftUser_SiteUserKeyDraftLeagueKey
ON [dbo].[DraftUser] ([SiteUserKey], [DraftLeagueKey])
WHERE [SiteUserKey] IS NOT NULL
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'UIX_PlayersDrafted_PlayerKeyDraftTeamKey')
BEGIN
	DROP INDEX [UIX_PlayersDrafted_PlayerKeyDraftTeamKey] ON [dbo].[PlayersDrafted];
END
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_PlayersDrafted_PlayerKeyDraftTeamKey
ON [dbo].[PlayersDrafted] ([PlayerKey], [DraftTeamKey])
GO
IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'UIX_TeamsDrafted_PlayerKeyTeamKey')
BEGIN
	DROP INDEX [UIX_TeamsDrafted_PlayerKeyTeamKey] ON [dbo].[TeamsDrafted];
END
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_TeamsDrafted_PlayerKeyTeamKey
ON [dbo].[TeamsDrafted] ([TeamKey], [DraftTeamKey])
GO

/*
	Data Imports
*/


BEGIN TRY
BEGIN TRAN

DECLARE @CreatedDate datetime2 = GETDATE(), @CreatedBy nvarchar(6) = 'SYSTEM';

INSERT INTO [lu].[LeagueSportType] (Code, [Description], CreatedDate, CreatedBy)
VALUES
('NBA','NBA League', @CreatedDate, @CreatedBy)
, ('MLB','MLB League', @CreatedDate, @CreatedBy)
, ('NHL','NHL League', @CreatedDate, @CreatedBy)
, ('NFL','NFL League', @CreatedDate, @CreatedBy);

INSERT INTO lu.PlayerDataType
(Code, [Description], CreatedDate, CreatedBy)
VALUES
('I', 'Image', @CreatedDate, @CreatedBy),
('S', 'Stats', @CreatedDate, @CreatedBy),
('O', 'Other', @CreatedDate, @CreatedBy);

INSERT INTO lu.[Site]
(Code, [Description], CreatedDate, CreatedBy)
VALUES
('E', 'ESPN', @CreatedDate, @CreatedBy),
('Y', 'Yahoo', @CreatedDate, @CreatedBy),
('C', 'CBS', @CreatedDate, @CreatedBy);

INSERT INTO lu.[Position]
(Code, [Description], CreatedDate, CreatedBy)
VALUES
('QB', 'Quarterback', @CreatedDate, @CreatedBy),
('DEF', 'Defense', @CreatedDate, @CreatedBy),
('RB', 'Running Back', @CreatedDate, @CreatedBy),
('TE', 'Tight End', @CreatedDate, @CreatedBy),
('K', 'Kicker', @CreatedDate, @CreatedBy),
('PK', 'Place Kicker', @CreatedDate, @CreatedBy),
('P', 'Punter', @CreatedDate, @CreatedBy),
('WR', 'Wide Receiver', @CreatedDate, @CreatedBy);

INSERT INTO lu.[UrlTypes]
(Code, [Description], CreatedDate, CreatedBy)
VALUES
('T', 'Url to get teams.', @CreatedDate, @CreatedBy),
('I', 'Url to get player images', @CreatedDate, @CreatedBy),
('P', 'Url to get player stats', @CreatedDate, @CreatedBy);

INSERT INTO dbo.[SiteURL]
(SiteKey, [UrlTypeKey], [Url], Inactive, CreatedDate, CreatedBy)
VALUES
('E', 'I', 'http://a.espncdn.com/combiner/i?img=/i/headshots/nfl/players/full/', 0, @CreatedDate, @CreatedBy),
('E', 'T', 'http://espn.go.com/nfl/teams', 0, @CreatedDate, @CreatedBy),
('E', 'P', 'http://espn.go.com/nfl/player/stats/_/id/', 0, @CreatedDate, @CreatedBy);

COMMIT TRAN
END TRY
BEGIN CATCH
	IF (@@TRANCOUNT > 0)
	BEGIN
		ROLLBACK TRAN
	END

	DECLARE @ErrorMessage nvarchar(max) = (SELECT ERROR_MESSAGE());
					
	RAISERROR(@ErrorMessage, 16, 1);
	RETURN;
END CATCH
GO

/*

	pLAYER/TEAM OBJECTS

*/


--TABLE TYPES TO MAKE


IF EXISTS (SELECT * FROM sys.table_types WHERE SCHEMA_NAME([schema_id]) = 'tabletype' AND [name] = 'PlayerTableType')
BEGIN
	DROP TYPE [tabletype].[PlayerTableType];
END
--GO
CREATE TYPE [tabletype].[PlayerTableType] AS TABLE (
	[PK] int NULL
	, [SourceID] int NULL
	, [TeamKey] int NULL
	, [Height] int NULL
	, [Weight] int NULL
	, [Experience] int NULL
	, [Age] int NULL
	, [JerseyNumber] int NULL
	, [PositionKey] nvarchar (5) NULL
	, [Name] nvarchar (100) NULL
	, [College] nvarchar (100) NULL
	, [Url] nvarchar (400) NULL
	, [SiteKey] nchar (1) NULL
	, [Inactive] bit NULL
);
--GO

IF EXISTS (SELECT * FROM sys.table_types WHERE SCHEMA_NAME([schema_id]) = 'tabletype' AND [name] = 'TeamTableType')
BEGIN
	DROP TYPE [tabletype].[TeamTableType];
END
--GO
CREATE TYPE [tabletype].[TeamTableType] AS TABLE (
	[PK] int NULL
	, [City] nvarchar (50) NULL
	, [TeamName] nvarchar (50) NULL
	, [FullName] nvarchar (100) NULL
	, [Url] nvarchar (400) NULL
	, [SiteKey] nchar (1) NULL
	, [ByeWeek] int NULL
	, [Inactive] bit NULL
);
--GO

IF EXISTS (SELECT * FROM sys.table_types WHERE SCHEMA_NAME([schema_id]) = 'tabletype' AND [name] = 'TeamStatsNFLTableType')
BEGIN
	DROP TYPE [tabletype].[TeamStatsNFLTableType];
END
--GO
CREATE TYPE [tabletype].[TeamStatsNFLTableType] AS TABLE (
	[PK] int NULL
	, [TeamKey] int NULL
	, [Year] int NULL
	, [Tackles] int NULL
	, [Sacks] int NULL
	, [TacklesForLoss] int NULL
	, [PassesDefensed] int NULL
	, [Interceptions] int NULL
	, [Interceptions_TD] int NULL
	, [Fumbles] int NULL
	, [Fumbles_TD] int NULL
	, [Fumbles_Recovered] int NULL
	, [BlockedKicks] int NULL
	, [Kickoff_Avg] decimal NULL
	, [Kickoff_TD] int NULL
	, [Punt_Avg] decimal NULL
	, [Punt_TD] int NULL
);
--GO

IF EXISTS (SELECT * FROM sys.table_types WHERE SCHEMA_NAME([schema_id]) = 'tabletype' AND [name] = 'PlayerStatsNFLTableType')
BEGIN
	DROP TYPE [tabletype].[PlayerStatsNFLTableType];
END
--GO
CREATE TYPE [tabletype].[PlayerStatsNFLTableType] AS TABLE (
	[PK] int NULL
	, [PlayerKey] int NULL
	, [Year] int NULL
	, [GamesPlayed] int NULL
	, [Rushing_Yards] decimal NULL
	, [Rushing_Att] int NULL
	, [Rushing_TD] int NULL
	, [Rushing_Fumbles] int NULL
	, [Receiving_Yards] decimal NULL
	, [Receiving_Rec] int NULL
	, [Receiving_Att] int NULL
	, [Receiving_TD] int NULL
	, [Receiving_Fumbles] int NULL
	, [Passing_Yards] decimal NULL
	, [Passing_Comp] int NULL
	, [Passing_Att] int NULL
	, [Passing_TD] int NULL
	, [Passing_Int] int NULL
	, [Passing_Rating] decimal NULL
	, [Passing_Fumbles] int NULL
	, [Kicking_FGM] int NULL
	, [Kicking_FGA] int NULL
	, [Kicking_119] nvarchar (8) NULL
	, [Kicking_2029] nvarchar (8) NULL
	, [Kicking_3039] nvarchar (8) NULL
	, [Kicking_4049] nvarchar (8) NULL
	, [Kicking_50Plus] nvarchar (8) NULL
	, [Kicking_Long] decimal NULL
	, [Kicking_XPM] int NULL
	, [Kicking_XPA] int NULL
	, [Kicking_Points] int NULL
);
--GO

IF EXISTS (SELECT * FROM sys.table_types WHERE SCHEMA_NAME([schema_id]) = 'tabletype' AND [name] = 'PlayerDataTableType')
BEGIN
	DROP TYPE [tabletype].[PlayerDataTableType];
END
--GO
CREATE TYPE [tabletype].[PlayerDataTableType] AS TABLE (
	[PK] int NULL
	, [PlayerKey] int NULL
	, [DataTypeKey] nchar (1) NULL
	, [Data] varbinary (max) NULL
);
--GO

IF EXISTS (SELECT * FROM sys.table_types WHERE SCHEMA_NAME([schema_id]) = 'tabletype' AND [name] = 'DraftLeagueTableType')
BEGIN
	DROP TYPE [tabletype].[DraftLeagueTableType];
END
--GO
CREATE TYPE [tabletype].[DraftLeagueTableType] AS TABLE (
	[PK] int NULL
	, [LeagueName] nvarchar (50) NULL
	, [LeagueMotto] nvarchar (400) NULL
	, [LeagueSportCode] nvarchar (5) NULL
	, [LeagueBuyIn] money NULL
	, [LeagueTeamCount] int NULL
	, [LeagueMaxPlayersPerTeam] int NULL
	, [LeagueHash] varbinary (128) NULL
	, [LeagueImage] varbinary (max) NULL
);
--GO

IF EXISTS (SELECT * FROM sys.table_types WHERE SCHEMA_NAME([schema_id]) = 'tabletype' AND [name] = 'DraftLeaguePayoutTableType')
BEGIN
	DROP TYPE [tabletype].[DraftLeaguePayoutTableType];
END
--GO
CREATE TYPE [tabletype].[DraftLeaguePayoutTableType] AS TABLE (
	[PK] int NULL
	, [DraftLeagueKey] int NULL
	, [Ranking] int NULL
	, [Payout] money NULL
);
--GO

IF EXISTS (SELECT * FROM sys.table_types WHERE SCHEMA_NAME([schema_id]) = 'tabletype' AND [name] = 'DraftTeamTableType')
BEGIN
	DROP TYPE [tabletype].[DraftTeamTableType];
END
--GO
CREATE TYPE [tabletype].[DraftTeamTableType] AS TABLE (
	[PK] int NULL
	, [DraftUserKey] int NULL
	, [TeamName] nvarchar (50) NULL
	, [TeamImage] varbinary (max) NULL
	, [TeamMotto] nvarchar (100) NULL
);
--GO

IF EXISTS (SELECT * FROM sys.table_types WHERE SCHEMA_NAME([schema_id]) = 'tabletype' AND [name] = 'DraftUserTableType')
BEGIN
	DROP TYPE [tabletype].[DraftUserTableType];
END
--GO
CREATE TYPE [tabletype].[DraftUserTableType] AS TABLE (
	[PK] int NULL
	, [SiteUserKey] int NULL
	, [DraftLeagueKey] int NULL
	, [DraftImage] varbinary (max) NULL
	, [IsCommissioner] bit NULL
	, [IsCoCommissioner] bit NULL
	, [IsPieceOfShit] bit NULL
);
--GO

IF EXISTS (SELECT * FROM sys.table_types WHERE SCHEMA_NAME([schema_id]) = 'tabletype' AND [name] = 'PlayersDraftedTableType')
BEGIN
	DROP TYPE [tabletype].[PlayersDraftedTableType];
END--
CREATE TYPE [tabletype].[PlayersDraftedTableType] AS TABLE (
	[PK] int NULL
	, [PlayerKey] int NULL
	, [DraftTeamKey] int NULL
	, [AmountPaid] money NULL
);

IF EXISTS (SELECT * FROM sys.table_types WHERE SCHEMA_NAME([schema_id]) = 'tabletype' AND [name] = 'SiteUserTableType')
BEGIN
	DROP TYPE [tabletype].[SiteUserTableType];
END
--GO
CREATE TYPE [tabletype].[SiteUserTableType] AS TABLE (
	[PK] int NULL
	, [UserName] nvarchar (50) NULL
	, [Password] varbinary (128) NULL
	, [Hash] varbinary (128) NULL
	, [Salt] varchar (32) NULL
	, [FirstName] nvarchar (50) NULL
	, [LastName] nvarchar (50) NULL
	, [MiddleInitial] nvarchar (1) NULL
	, [UserEmail] nvarchar (200) NULL
	, [UserImage] varbinary (max) NULL
	, [IsAdmin] bit NULL
	, [Inactive] bit NULL
);
--GO

IF EXISTS (SELECT * FROM sys.table_types WHERE SCHEMA_NAME([schema_id]) = 'tabletype' AND [name] = 'TeamsDraftedTableType')
BEGIN
	DROP TYPE [tabletype].[TeamsDraftedTableType];
END
--GO
CREATE TYPE [tabletype].[TeamsDraftedTableType] AS TABLE (
	[PK] int NULL
	, [TeamKey] int NULL
	, [DraftTeamKey] int NULL
	, [AmountPaid] money NULL
);
--GO




IF EXISTS (SELECT * FROM sys.objects T WHERE T.[type] IN (N'FN', N'IF', N'TF', N'FS', N'FT') AND SCHEMA_NAME(T.[schema_id]) = 'dbo' AND T.[name] = 'fn_GetModelDataType')
BEGIN
	DROP FUNCTION dbo.fn_GetModelDataType;
END
GO
CREATE FUNCTION dbo.fn_GetModelDataType
( @DataType nvarchar(50)
, @ColumnName nvarchar(100)
, @Nullable bit = 1)
RETURNS varchar(500)
WITH EXECUTE AS CALLER
AS
BEGIN
	DECLARE @retval varchar(500) = 'public '
			, @gettersettersection nvarchar(100) = @ColumnName + ' { get; set; }' + CHAR(13);
	SET @DataType = UPPER(@DataType);

	IF (@DataType IN ('DECIMAL','MONEY','NUMERIC'))
	BEGIN
		SET @retval += 'decimal';
	END
	ELSE IF (@DataType IN ('INT'))
	BEGIN
		SET @retval += 'int';
	END
	ELSE IF (@DataType IN ('SMALLINT'))
	BEGIN
		SET @retval += 'short';
	END
	ELSE IF (@DataType IN ('BIGINT'))
	BEGIN
		SET @retval += 'long';
	END
	ELSE IF (@DataType IN ('BIT'))
	BEGIN
		SET @retval += 'bool';
		SET @Nullable = 0;
	END
	ELSE IF (@DataType LIKE '%CHAR%')
	BEGIN
		SET @retval += 'string';
		SET @Nullable = 0;
	END
	ELSE IF (@DataType LIKE '%BINARY%')
	BEGIN
		SET @retval += 'byte[]';
		SET @Nullable = 0;
	END
	ELSE IF (@DataType LIKE '%DATE%')
	BEGIN
		SET @retval += 'DateTime';
	END
	ELSE IF (@DataType IN ('TIME'))
	BEGIN
		SET @retval += 'TimeSpan';
	END

	SET @retval += (CASE WHEN @Nullable = 1 THEN '? ' ELSE ' ' END) + @gettersettersection;
	RETURN @retval; 
END
GO

IF EXISTS (SELECT * FROM sys.objects T WHERE T.[type] IN (N'FN', N'IF', N'TF', N'FS', N'FT') AND SCHEMA_NAME(T.[schema_id]) = 'dbo' AND T.[name] = 'fn_GenerateHelperText')
BEGIN
	DROP FUNCTION dbo.fn_GenerateHelperText;
END
GO
CREATE FUNCTION dbo.fn_GenerateHelperText
( 
	@TableName nvarchar(50)
	, @SchemaName nvarchar(100)
	, @WhatDo varchar(10)
	, @ForceNullable bit = 1
	, @TableTypeSuffix varchar(20) = 'TableType'  --THIS IS WHAT i USE FOR TABLETYPES
	, @TableTypeSchema varchar(20) = 'tabletype'  --THIS IS WHAT i USE FOR TABLETYPES
)
RETURNS varchar(max)
WITH EXECUTE AS CALLER
AS
BEGIN
		
	IF NOT EXISTS (SELECT * FROM sys.tables T WHERE T.name = @TableName AND SCHEMA_NAME(T.[schema_id]) = @SchemaName)
	BEGIN
		RETURN 'Table / Schema Pair Not Found'
	END
	
	DECLARE @updatecolumnvalues varchar(max) = ''
			, @mergecolumns varchar(max) = ''
			, @inserttopcolumns varchar(max) = ''
			, @modelcreator varchar(max) = ''		
			, @insertbottomcolumns varchar(max) = ''
			, @sql varchar(max);

	/*
	SELECT 
		@updatecolumnvalues += ', TARGET.[' + J.name + '] = SOURCE.[' + J.name + ']' + CHAR(13)
		, @inserttopcolumns += ', [' + J.name + ']' + CHAR(13)
		, @insertbottomcolumns += ', SOURCE.[' + J.name + ']' + CHAR(13)
	FROM sys.table_types T
	INNER JOIN sys.columns J ON J.object_id = T.type_table_object_id
	WHERE T.name = 'TableParameter_PayPeriod'
	AND SCHEMA_NAME(T.schema_id) = 'tabletype';
	*/

	SELECT 
		@updatecolumnvalues += ', T.[' + J.name + '] = @' + J.name + '' + CHAR(13)
		, @mergecolumns += ', TARGET.[' + J.name + '] = SOURCE.[' + J.name + ']' + CHAR(13)
		, @inserttopcolumns += ', [' + J.name + ']' + CHAR(13)
		, @insertbottomcolumns += ', SOURCE.[' + J.name + ']' + CHAR(13)
		, @modelcreator += (CASE WHEN LJ1.name IS NOT NULL THEN '[AssignToTableType(true)]' ELSE '[AssignToTableType(false)]' END) + CHAR(13) 
							+ (CASE WHEN MAX(ISNULL(CONVERT(int, LJ4.is_primary_key),0)) = 1 THEN '[Key]' + CHAR(13) ELSE '' END) 
							+ (CASE WHEN MAX(ISNULL(CONVERT(int, LJ4.is_primary_key),0)) = 1 AND LJ1.name IS NOT NULL THEN '[Required]' + CHAR(13) ELSE '' END) 
							+ dbo.fn_GetModelDataType(J1.[DATA_TYPE], J.[name], (CASE WHEN @ForceNullable = 1 THEN 1 ELSE MAX(CONVERT(int, J.is_nullable)) END))
	FROM sys.tables T
	INNER JOIN sys.columns J ON J.[object_id] = T.[object_id]
	INNER JOIN INFORMATION_SCHEMA.COLUMNS J1 ON J1.[TABLE_NAME] = T.[name] AND J1.[TABLE_SCHEMA] = SCHEMA_NAME(T.[schema_id]) AND J1.[COLUMN_NAME] = J.[name]
	LEFT JOIN sys.table_types LJ ON LJ.[name] = T.[name] + @TableTypeSuffix AND SCHEMA_NAME(LJ.[schema_id]) = @TableTypeSchema
	LEFT JOIN sys.columns LJ1 ON LJ1.[object_id] = LJ.type_table_object_id AND LJ1.[name] = J.[name]
	LEFT JOIN INFORMATION_SCHEMA.COLUMNS LJ2 ON LJ2.[TABLE_NAME] = LJ.[name] AND LJ2.[TABLE_SCHEMA] = SCHEMA_NAME(LJ.[schema_id]) AND LJ2.[COLUMN_NAME] = LJ1.[name]
	LEFT JOIN sys.index_columns LJ3 ON LJ3.[object_id] = J.[object_id] AND LJ3.column_id = J.column_id
	LEFT JOIN sys.indexes LJ4 ON LJ3.[object_id] = LJ4.[object_id]
	WHERE T.name = @TableName
	AND SCHEMA_NAME(T.[schema_id]) = @SchemaName
	GROUP BY J.name, LJ1.name, J1.[DATA_TYPE],LJ2.ORDINAL_POSITION, J1.ORDINAL_POSITION
	ORDER BY LJ2.ORDINAL_POSITION, J1.ORDINAL_POSITION;
	
	SET @updatecolumnvalues = SUBSTRING(@updatecolumnvalues, 3, LEN(@updatecolumnvalues) - 2);
	SET @mergecolumns = SUBSTRING(@mergecolumns, 3, LEN(@mergecolumns) - 2);
	SET @inserttopcolumns = SUBSTRING(@inserttopcolumns, 3, LEN(@inserttopcolumns) - 2);
	SET @insertbottomcolumns = SUBSTRING(@insertbottomcolumns, 3, LEN(@insertbottomcolumns) - 2);
	SET @modelcreator = '[Table("'+ @TableName +'", Schema="'+ @SchemaName +'")]' + CHAR(13) + 'public class ' + @TableName + CHAR (13) + '{' + CHAR (13) + @modelcreator + '}' + CHAR(13); 

	/*
	PRINT(@updatecolumnvalues);
	PRINT(@mergecolumns);
	PRINT(@inserttopcolumns);
	PRINT(@insertbottomcolumns);
	PRINT(@modelcreator);
	*/

	SET @sql = (CASE WHEN @WhatDo = 'INSERT' THEN (@inserttopcolumns + @insertbottomcolumns)
					 WHEN @WhatDo = 'UPDATE' THEN @updatecolumnvalues
					 WHEN @WhatDo = 'MERGE' THEN @mergecolumns
					 WHEN @WhatDo = 'CLASS' THEN @modelcreator
					 END);
	RETURN @sql; 
END
GO 

IF EXISTS (SELECT * FROM sys.objects T WHERE T.[type] IN (N'FN', N'IF', N'TF', N'FS', N'FT') AND SCHEMA_NAME(T.[schema_id]) = 'dbo' AND T.[name] = 'fn_GenerateTableType')
BEGIN
	DROP FUNCTION dbo.fn_GenerateTableType;
END
GO
CREATE FUNCTION dbo.fn_GenerateTableType
( 
	@TableName nvarchar(50)
	, @SchemaName nvarchar(100)
	, @StructureMatch bit = 0 --want nullable everything, No createds/updateds
)
RETURNS varchar(max)
WITH EXECUTE AS CALLER
AS
BEGIN
		
	IF NOT EXISTS (SELECT * FROM sys.tables T WHERE T.name = @TableName AND SCHEMA_NAME(T.[schema_id]) = @SchemaName)
	BEGIN
		RETURN 'Table / Schema Pair Not Found'
	END
	
	DECLARE @columns varchar(max) = ''
			, @sql varchar(max) = 'CREATE TYPE [tabletype].['+ @TableName + 'TableType] AS TABLE (' + CHAR(13)
			, @ifsql varchar(max) = 'IF EXISTS (SELECT * FROM sys.table_types WHERE SCHEMA_NAME([schema_id]) = ''tabletype'' AND [name] = '''+ @TableName +'TableType'')'
									+ CHAR(13) + 'BEGIN' 
									+ CHAR(13) + '	DROP TYPE [tabletype].['+ @TableName + 'TableType];'
									+ CHAR(13) + 'END'
									+ CHAR(13) + 'GO'
									+ CHAR(13);
				
	SELECT 
		@columns += '	, ['+ c.[COLUMN_NAME] +'] '
					+ c.[DATA_TYPE] + ' '
					+ (CASE WHEN c.[CHARACTER_MAXIMUM_LENGTH] IS NULL THEN ''
							WHEN c.[CHARACTER_MAXIMUM_LENGTH] = -1 THEN '(max) '
							ELSE '('+ CONVERT(varchar, c.[CHARACTER_MAXIMUM_LENGTH]) +') ' END)
					+ (CASE WHEN @StructureMatch = 0 THEN 'NULL' 
							WHEN C.[IS_NULLABLE] = 'YES' THEN 'NOT NULL'
							ELSE 'NULL' END)
					+ CHAR(13)
	FROM INFORMATION_SCHEMA.COLUMNS c 
	WHERE c.[TABLE_NAME] = @TableName 
	AND c.[TABLE_SCHEMA] = @SchemaName
	AND (@StructureMatch = 1 OR c.[COLUMN_NAME] NOT IN ('CreatedDate','CreatedBy','UpdatedDate','UpdatedBy'))
	ORDER BY C.[ORDINAL_POSITION] ASC;
	
	SET @columns = SUBSTRING(@columns, 3, LEN(@columns) - 3);

	SET @sql = @ifsql + @sql + @columns + CHAR(13) + ');' + CHAR(13)  + 'GO' + CHAR(13);

	RETURN @sql; 
END
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'sp_createtableobjects')
BEGIN
	DROP PROC [dbo].[sp_createtableobjects];
END
GO
CREATE PROC [dbo].[sp_createtableobjects]
@TableName nvarchar(50)
, @Schema nvarchar(4)
, @WhatToGenerate nvarchar(2)
, @AutoExecute bit = 0
AS

	IF (@TableName IS NULL)
	BEGIN
		RAISERROR('Please input a @TableName parameter', 1, 16);
		RETURN;
	END
	IF (@Schema IS NULL)
	BEGIN
		RAISERROR('Please input a @Schema parameter', 1, 16);
		RETURN;
	END
	IF (@WhatToGenerate IS NULL)
	BEGIN
		RAISERROR('Please input a @WhatToGenerate parameter', 1, 16);
		RETURN;
	END
	DECLARE @columns varchar(max) = ''	
	, @ordinalposition int
	, @columninsert varchar(max) = ''
	, @columnname nvarchar(100)
	, @datatype nvarchar(100)
	, @datalength nvarchar(10)
	, @scale nvarchar(2)
	, @precision nvarchar(2)
	, @NULLable nvarchar(10)
	, @DEFAULT nvarchar(50)
	, @paramcreate varchar(max) = ''
	, @paramset varchar(max) = ''
	, @columnrow nvarchar(1000);

	SET @ordinalposition = (SELECT DISTINCT TOP 1 C.ORDINAL_POSITION
						   FROM INFORMATION_SCHEMA.COLUMNS C
						   WHERE C.TABLE_NAME = @TableName
						   AND C.TABLE_SCHEMA = @Schema
						   ORDER BY C.ORDINAL_POSITION ASC);

	IF (@ordinalposition IS NULL)
	BEGIN
		RAISERROR('This schema/table does not exist in the database', 1, 16);
		RETURN;
	END
	WHILE @ordinalposition IS NOT NULL
	BEGIN
		SELECT DISTINCT
			@columnname = (CASE WHEN @ordinalposition > 1 THEN ', [' ELSE '[' END) + (CASE WHEN @WhatToGenerate = 'Y' THEN 'his' ELSE '' END) + C.COLUMN_NAME + ']'
			, @datatype = ' [' + C.DATA_TYPE + ']'
			, @precision = CONVERT(nvarchar, C.NUMERIC_PRECISION)
			, @scale = CONVERT(nvarchar, ISNULL(C.NUMERIC_SCALE, 2))
			, @datalength = ISNULL('(' + CONVERT(nvarchar, C.CHARACTER_MAXIMUM_LENGTH) + ')', '')
			, @NULLable = (CASE WHEN C.IS_NULLABLE = 'NO' THEN ' NOT NULL' ELSE ' NULL' END)
			, @DEFAULT = ' ' + ISNULL('DEFAULT' + UPPER(C.COLUMN_DEFAULT), '')
		FROM INFORMATION_SCHEMA.COLUMNS C
		WHERE C.TABLE_NAME = @TableName
		AND C.TABLE_SCHEMA = @Schema
		AND C.COLUMN_NAME != @TableName + 'ID'
		AND C.ORDINAL_POSITION = @ordinalposition;

		--SET @columnrow = @columnname + @datatype + @datalength + @NULLable + @DEFAULT + CHAR(13);
		--SET @columns += @columnrow;

		IF (@WhatToGenerate = 'P') --CREATE PARAMETERS FOR INSERT
		BEGIN
			SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));
			IF (@columnname != 'CreatedBy' AND @columnname != 'CreatedDate' AND @columnname != 'UpdatedBy' AND @columnname != 'UpdatedDate')
			BEGIN
				SET @columns += (CASE WHEN @ordinalposition = 1 THEN '@' ELSE ', @' END)
								+ @columnname + @datatype + (CASE WHEN UPPER(LTRIM(RTRIM(@datatype))) = '[DECIMAL]' THEN '(' + @precision + ',' + @scale + ')'  ELSE @datalength END) + CHAR(13);
			END
		END
		ELSE IF (@WhatToGenerate = 'PS') --CREATE PARAMETER STRING FOR LOGGING
		BEGIN
			SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));
			IF (@columnname != 'CreatedBy' AND @columnname != 'CreatedDate' AND @columnname != 'UpdatedBy' AND @columnname != 'UpdatedDate')
			BEGIN
				SET @columns += (CASE WHEN @ordinalposition = 1 THEN '''@' ELSE '+ ' + '''| @' END)
								+ @columnname + ': '' + ISNULL(' + (CASE WHEN @datatype LIKE '%char%' THEN  '@' + @columnname ELSE 'CONVERT(varchar, @' + @columnname + ')' END) + ', ''NULL'')' + CHAR(13);
			END
		END
		ELSE IF (@WhatToGenerate = 'C') --CREATE CLASS members for c# object
		BEGIN			
			SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));
			IF (@columnname != 'CreatedBy' AND @columnname != 'CreatedDate' AND @columnname != 'UpdatedBy' AND @columnname != 'UpdatedDate')
			BEGIN
				SET @datatype = UPPER(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@datatype, '[' , ''), ']', ''), ',', ''))));
				SET @NULLable = LTRIM(RTRIM(@NULLable));
				--PRINT(@NULLable);
				SET @columns += '[DataMember(IsRequired = true)]' + CHAR(13) + 'public ' + 
								(CASE WHEN @datatype IN ('DATE', 'DATETIME', 'DATETIME2') THEN (CASE WHEN @NULLable = 'NOT NULL' THEN 'DateTime ' ELSE 'DateTime? ' END)
								WHEN @datatype = 'INT' THEN (CASE WHEN @NULLable = 'NOT NULL' THEN 'int ' ELSE 'int? ' END)
								WHEN @datatype = 'SMALLINT' THEN (CASE WHEN @NULLable = 'NOT NULL' THEN 'short ' ELSE 'short? ' END)
								WHEN @datatype = 'BIGINT' THEN (CASE WHEN @NULLable = 'NOT NULL' THEN 'long ' ELSE 'long? ' END)
								WHEN @datatype IN ('DECIMAL', 'MONEY') THEN (CASE WHEN @NULLable = 'NOT NULL' THEN 'decimal ' ELSE 'decimal? ' END)
								WHEN @datatype IN ('FLOAT') THEN (CASE WHEN @NULLable = 'NOT NULL' THEN 'float ' ELSE 'float? ' END)
								WHEN @datatype = 'BIT' THEN (CASE WHEN @NULLable = 'NOT NULL' THEN 'bool ' ELSE 'bool? ' END)							 
								WHEN @datatype IN ('NVARCHAR', 'VARCHAR', 'CHAR', 'NCHAR') THEN 'string ' 
								ELSE 'string '
								END) + @columnname + ' { get; set; }' + CHAR(13);
			END
		END
		ELSE IF (@WhatToGenerate = 'H') --create html objects
		BEGIN			
			SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));
			IF (@columnname != 'CreatedBy' AND @columnname != 'CreatedDate' AND @columnname != 'UpdatedBy' AND @columnname != 'UpdatedDate')
			BEGIN
				SET @datatype = UPPER(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@datatype, '[' , ''), ']', ''), ',', ''))));
				SET @NULLable = LTRIM(RTRIM(@NULLable));
				--PRINT(@NULLable);
				SET @columns += (CASE WHEN UPPER(RIGHT(@columnname, 3)) = 'KEY' THEN
									'<div class="divHorizontal">' + CHAR(13) 
									+ CHAR(9) + '<label for="' + @columnname + '">' + @columnname + '</label>' + CHAR(13)
									+ CHAR(9) + '<div class="ComboBoxDivStyle BlockRelativeOverride">' + CHAR(13)
									+ CHAR(9) + CHAR(9) + '<select runat="server" data-runatname="' + @columnname + '" class="stuff BlockRelativeOverride" clientidmode="Static" id="' + @columnname + '"></select>' + CHAR(13)
									+ CHAR(9) + '</div>' + CHAR(13)
									+ '</div>'								
								WHEN @datatype IN ('DATE', 'DATETIME', 'DATETIME2') THEN
									'<div class="divHorizontal">' + CHAR(13) 
									+ CHAR(9) + '<label for="' + @columnname + '">' + @columnname + '</label>' + CHAR(13)
									+ CHAR(9) + '<div class="DateTextBoxDivStyle BlockRelativeOverride">' + CHAR(13)
									+ CHAR(9) + CHAR(9) + '<input runat="server" data-runatname="' + @columnname + '" class="WrappedDateTextBoxStyle" clientidmode="Static" id="' + @columnname + '" type="text" />' + CHAR(13)
									+ CHAR(9) + '</div>' + CHAR(13)
									+ '</div>'
								WHEN @datatype IN ('INT','SMALLINT','BIGINT') THEN
									'<div class="divHorizontal">' + CHAR(13) 
									+ CHAR(9) + '<label for="' + @columnname + '">' + @columnname + '</label>' + CHAR(13)
									+ CHAR(9) + '<input name="' + @columnname + '" style="width:100px;" maxlength="' + @precision + '" value="' + (CASE WHEN @columnname = 'PK' THEN '-1' ELSE '' END) + '" class="NumbersOnlyClass" type="text" />' + CHAR(13)
									+ '</div>'
								WHEN @datatype IN ('MONEY') THEN
									'<div class="divHorizontal">' + CHAR(13) 
									+ CHAR(9) + '<label for="' + @columnname + '">' + @columnname + '</label>' + CHAR(13)
									+ CHAR(9) + '<input name="' + @columnname + '" style="width:100px;" value="$0.00" class="CurrencyClass" maxlength="10" type="text" />' + CHAR(13)
									+ '</div>'
								WHEN @datatype = 'BIT' THEN			
									'<div class="divRadioButtonLabel">' + CHAR(13) 
									+ CHAR(9) + '<label>' + @columnname + '</label>' + CHAR(13)
									+ CHAR(9) + '<div>' + CHAR(13)
									+ CHAR(9) + CHAR(9) + '<label for="' + @columnname + '_1">' + @columnname + '_true</label>' + CHAR(13)
									+ CHAR(9) + CHAR(9) + '<input name="' + @columnname + '" id="' + @columnname + '_1" type="radio" checked="checked" value="true" />' + CHAR(13)
									+ CHAR(9) + CHAR(9) + '<label for="' + @columnname + '_2">' + @columnname + '_false</label>' + CHAR(13)
									+ CHAR(9) + CHAR(9) + '<input name="' + @columnname + '" id="' + @columnname + '_2" type="radio" value="false" />' + CHAR(13)
									+ CHAR(9) + '</div>' + CHAR(13)
									+ '</div>'				 
								WHEN @datatype IN ('FLOAT', 'DECIMAL') THEN
									'<div class="divHorizontal">' + CHAR(13) 
									+ CHAR(9) + '<label for="' + @columnname + '">' + @columnname + '</label>' + CHAR(13)
									+ CHAR(9) + '<input name="' + @columnname + '" style="width:100px;" value="' + (CASE WHEN @columnname = 'PK' THEN '-1' ELSE '0' END) + '" class="DecimalsOnlyClass" data-scale="' + @scale + '" data-precision="' + @precision + '" maxlength="' + CONVERT(nvarchar,(1 + (CONVERT(int, @precision) + CONVERT(int, @scale)))) + '" type="text" />' + CHAR(13)
									+ '</div>'
								ELSE 
									'<div class="divHorizontal">' + CHAR(13) 
									+ CHAR(9) + '<label for="' + @columnname + '">' + @columnname + '</label>' + CHAR(13)
									+ CHAR(9) + '<input name="' + @columnname + '" ' + (CASE WHEN UPPER(@columnname) LIKE '%MOBILE%' OR UPPER(@columnname) LIKE '%PHONE%' OR UPPER(@columnname) LIKE '%FAX%' THEN 'class="PhoneNumberClass"' ELSE '' END) + ' style="width:100px;" value="' + (CASE WHEN @columnname = 'PK' THEN '-1' ELSE '' END) + '"  type="text" maxlength="' + REPLACE(REPLACE(@datalength, '(', '') ,')', '') + '" />' + CHAR(13)
									+ '</div>'
								END) + CHAR(13);
			END
		END
		ELSE IF (@WhatToGenerate = 'A') --ado.net sql param setup
		BEGIN			
			SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));
			IF (@columnname != 'CreatedBy' AND @columnname != 'CreatedDate' AND @columnname != 'UpdatedBy' AND @columnname != 'UpdatedDate')
			BEGIN
				SET @datatype = UPPER(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@datatype, '[' , ''), ']', ''), ',', ''))));
				SET @NULLable = LTRIM(RTRIM(@NULLable));
				--PRINT(@NULLable);
				SET @paramcreate += CHAR(13) + 'var ' + @columnname + ' = new SqlParameter("@' + @columnname + '", obj.' + @columnname + 
											(CASE WHEN 
											@datatype NOT IN ('DATE', 'DATETIME', 'DATETIME2', 'INT', 'SMALLINT', 'BIGINT', 'DECIMAL', 'MONEY', 'FLOAT', 'BIT') 
											THEN '.ToSQLNull()' ELSE '' END) + ');' +
									(CASE WHEN @datatype = 'DATE' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.Date;'
									WHEN @datatype = 'DATETIME' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.DateTime;'
									WHEN @datatype = 'DATETIME2' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.DateTime2;'
									WHEN @datatype = 'INT' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.Int;'
									WHEN @datatype = 'SMALLINT' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.SmallInt;'
									WHEN @datatype = 'BIGINT' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.BigInt;'
									WHEN @datatype = 'DECIMAL' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.Decimal;'
									WHEN @datatype = 'MONEY' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.Money;'
									WHEN @datatype = 'FLOAT' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.Float;'
									WHEN @datatype = 'BIT' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.Bit;'							 
									ELSE ''	END);
				SET @paramset += 'cmd.Parameters.Add(' + @columnname + ');' + CHAR(13);
			END
		END
		ELSE IF (@WhatToGenerate = 'E') --entity framework sql param setup
		BEGIN			
			SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));
			IF (@columnname != 'CreatedBy' AND @columnname != 'CreatedDate' AND @columnname != 'UpdatedBy' AND @columnname != 'UpdatedDate')
			BEGIN
				SET @datatype = UPPER(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@datatype, '[' , ''), ']', ''), ',', ''))));
				SET @NULLable = LTRIM(RTRIM(@NULLable));
				--PRINT(@NULLable);
				SET @paramset += CHAR(13) + (CASE WHEN @ordinalposition > 1 THEN ', obj.' ELSE 'obj.' END) + @columnname;								
			END
		END
		ELSE IF (@WhatToGenerate = 'N') --NULL checks
		BEGIN			
			SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));
			IF (@columnname != 'CreatedBy' AND @columnname != 'CreatedDate' AND @columnname != 'UpdatedBy' AND @columnname != 'UpdatedDate')
			BEGIN
				SET @datatype = UPPER(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@datatype, '[' , ''), ']', ''), ',', ''))));
				SET @NULLable = LTRIM(RTRIM(@NULLable));
				--PRINT(@NULLable);
				SET @paramset += CHAR(13) + (CASE WHEN @ordinalposition > 1 THEN '|| obj.' ELSE 'obj.' END) + @columnname;								
			END
		END
		ELSE IF (@WhatToGenerate = 'V') --validation (NULL catching)
		BEGIN			
			SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));
			IF (@columnname != 'CreatedBy' AND @columnname != 'CreatedDate' AND @columnname != 'UpdatedBy' AND @columnname != 'UpdatedDate')
			BEGIN
				SET @datatype = UPPER(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@datatype, '[' , ''), ']', ''), ',', ''))));
				SET @NULLable = LTRIM(RTRIM(@NULLable));
				--PRINT(@NULLable);
				IF (@NULLable = 'NOT NULL')
				BEGIN
					SET @columns += (CASE WHEN @ordinalposition > 1 THEN '|| ' ELSE '' END) +
									(CASE WHEN @datatype IN ('DATE', 'DATETIME', 'DATETIME2') THEN '(obj.' + @columnname + ' == NULL || obj.' + @columnname + ' == new DateTime(1900,1,1))' 
									WHEN @datatype = 'INT' THEN '(obj.' + @columnname + ' == NULL || obj.' + @columnname + ' < 0)'
									WHEN @datatype = 'SMALLINT' THEN '(obj.' + @columnname + ' == NULL || obj.' + @columnname + ' < 0)'
									WHEN @datatype = 'BIGINT' THEN '(obj.' + @columnname + ' == NULL || obj.' + @columnname + ' < 0)'
									WHEN @datatype IN ('DECIMAL', 'MONEY') THEN '(obj.' + @columnname + ' == NULL || obj.' + @columnname + ' < 0)' 
									WHEN @datatype IN ('FLOAT') THEN '(obj.' + @columnname + ' == NULL || obj.' + @columnname + ' < 0)'
									WHEN @datatype = 'BIT' THEN '(obj.' + @columnname + ' == NULL || obj.' + @columnname + ' < 0)'							 
									WHEN @datatype IN ('NVARCHAR', 'VARCHAR', 'CHAR', 'NCHAR') THEN 'string.IsNullOrEmpty(obj.' + @columnname + ')'
									ELSE 'string.IsNullOrEmpty(obj.' + @columnname + ')'
									END) + CHAR(13);
				END
				ELSE IF (@NULLable = 'NULL')
				BEGIN
					SET @columns += (CASE WHEN @ordinalposition > 1 THEN '|| ' ELSE '' END) +
									(CASE WHEN @datatype IN ('DATE', 'DATETIME', 'DATETIME2') THEN '(!obj.' + @columnname + '.HasValue || obj.' + @columnname + '.Value == new DateTime(1900,1,1))' 
									WHEN @datatype = 'INT' THEN '(!obj.' + @columnname + '.HasValue || obj.' + @columnname + ' < 0)'
									WHEN @datatype = 'SMALLINT' THEN ' (!obj.' + @columnname + '.HasValue || obj.' + @columnname + ' < 0)'
									WHEN @datatype = 'BIGINT' THEN '(!obj.' + @columnname + '.HasValue || obj.' + @columnname + ' < 0)'
									WHEN @datatype IN ('DECIMAL', 'MONEY') THEN '(!obj.' + @columnname + '.HasValue || obj.' + @columnname + ' < 0)' 
									WHEN @datatype IN ('FLOAT') THEN '(!obj.' + @columnname + '.HasValue || obj.' + @columnname + ' < 0)'
									WHEN @datatype = 'BIT' THEN '(!obj.' + @columnname + '.HasValue)'							 
									WHEN @datatype IN ('NVARCHAR', 'VARCHAR', 'CHAR', 'NCHAR') THEN 'string.IsNullOrEmpty(obj.' + @columnname + ')'
									ELSE 'string.IsNullOrEmpty(obj.' + @columnname + ')'
									END) + CHAR(13);
				END				
			END
		END
		ELSE IF (@WhatToGenerate IN ('T','Y')) --CREATING A TABLE type parameter WITH THE COLUMNS OF EXISTING table to replicate
		BEGIN
			SET @columns += @columnname + @datatype + @datalength + @NULLable + @DEFAULT + CHAR(13);
		END
		ELSE IF (@WhatToGenerate = 'S') --CREATING column list for select
		BEGIN			
			IF (CHARINDEX('UpdatedBy', @columnname) < 1 AND CHARINDEX('UpdatedDate', @columnname) < 1 AND CHARINDEX('CreatedBy', @columnname) < 1 AND CHARINDEX('CreatedDate', @columnname) < 1)
			BEGIN
				SET @columns += @columnname + CHAR(13);
			END
		END
		ELSE IF (@WhatToGenerate = 'I') --CREATING column list for insert
		BEGIN			
			IF (CHARINDEX('UpdatedBy', @columnname) < 1 AND CHARINDEX('UpdatedDate', @columnname) < 1)
			BEGIN
				SET @columns += @columnname + CHAR(13);
				SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE((CASE WHEN CHARINDEX('CreatedBy', @columnname) > 0 THEN ', @UserID' ELSE @columnname END), '[' , '@'), ']', '')));
				SET @columninsert += @columnname + CHAR(13);
			END
		END
		ELSE IF (@WhatToGenerate = 'U') --CREATING update set list
		BEGIN
			DECLARE @columnnameparameter nvarchar(100) = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE((CASE WHEN CHARINDEX('UpdatedBy', @columnname) > 0 THEN 'UserID' ELSE @columnname END), '[' , ''), ']', ''), ',', '')));	
			IF (CHARINDEX('CreatedBy', @columnname) < 1 AND CHARINDEX('CreatedDate', @columnname) < 1)
			BEGIN
				SET @columns += @columnname + ' = @' + @columnnameparameter + CHAR(13);
			END
		END

		SET @ordinalposition = (SELECT DISTINCT TOP 1 C.ORDINAL_POSITION
								FROM INFORMATION_SCHEMA.COLUMNS C
								WHERE C.TABLE_NAME = @TableName
								AND C.TABLE_SCHEMA = @Schema
								AND C.ORDINAL_POSITION > @ordinalposition
								ORDER BY C.ORDINAL_POSITION ASC);
	END

	IF (@WhatToGenerate = 'P') --CREATE PARAMETERS FOR INSERT
	BEGIN	
		SET @columns = @columns + ', @UserID nvarchar(6)' + CHAR(13) + ', @SuccessValue bit OUTPUT' + CHAR(13) + ', @Message nvarchar(max) OUTPUT';
	END
	ELSE IF (@WhatToGenerate = 'A') --CREATING A TABLE type parameter WITH THE COLUMNS OF EXISTING table to replicate
	BEGIN
		SET @columns = 'var UserID = new SqlParameter("@UserID", userid);
						var SuccessValue = new SqlParameter("@SuccessValue", DBNull.Value);
						SuccessValue.Direction = ParameterDirection.Output;
						SuccessValue.SqlDbType = SqlDbType.Bit;
						var Message = new SqlParameter("@Message", DBNull.Value);
						Message.Direction = ParameterDirection.Output;
						Message.Size = 4000;' + CHAR(13) + @paramcreate + CHAR(13) + CHAR(13) +
						'using (con)
						{
                        // Create a SQL command to execute the sproc
                        var cmd = con.CreateCommand();
                        cmd.CommandTimeout = 180;
                        cmd.CommandText = "[dbo].[]";
                        cmd.CommandType = CommandType.StoredProcedure;' + @paramset + CHAR(13) +
						'cmd.Parameters.Add(UserID);
                        cmd.Parameters.Add(SuccessValue);
                        cmd.Parameters.Add(Message);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Dispose();

                        data.StatusOk = (bool)SuccessValue.Value;
                        data.ReturnMessage = (string)(Message.Value == DBNull.Value ? string.Empty : Message.Value);
						}';
	END
	ELSE IF (@WhatToGenerate = 'E') --CREATING A TABLE type parameter WITH THE COLUMNS OF EXISTING table to replicate
	BEGIN
		SET @columns = '(' + @paramset + '
						, userid
                        , SuccessValue
                        , Message
						);';		 
	END
	ELSE IF (@WhatToGenerate IN ('T','Y')) --CREATING A TABLE type parameter WITH THE COLUMNS OF EXISTING table to replicate
	BEGIN
		IF (@WhatToGenerate = 'Y')
		BEGIN
			SET @columns += ', [his' + @TableName + 'ID] int' +
							CHAR(13) + ', [his' + @TableName + 'TraceID] int' +
							CHAR(13) + ', [his' + @TableName + 'DateAdded] datetime2' +
							CHAR(13) + ', [his' + @TableName + 'LastUpdateDate] datetime2' +
							CHAR(13) + ', [his' + @TableName + 'UserID] nvarchar(6)' +
							CHAR(13) + ', [ActionTaken] nvarchar(10)';
		END
		SET @columns = 'CREATE TYPE [tabletype].[' + (CASE WHEN @WhatToGenerate = 'T' THEN '' ELSE 'History' END) + @TableName + 'TableType] AS TABLE (' + CHAR(13) + @columns + ');';
	END
	ELSE IF (@WhatToGenerate = 'S') --CREATING column list for select
	BEGIN		
		SET @columns = 'SELECT' + CHAR(13) + @columns + 'FROM [' + @Schema + '].[' + @TableName + '] T;';		
	END
	ELSE IF (@WhatToGenerate = 'I') --CREATING column list for insert
	BEGIN		
		SET @columns = 'DECLARE @CreatedDate datetime2 = GETDATE();' + CHAR(13) + CHAR(13) + 'INSERT INTO [' + @Schema + '].[' + @TableName + '] (' + CHAR(13) + @columns + ')' + CHAR(13) + 'VALUES' + CHAR(13) + '(' + CHAR(13) + @columninsert + ')';
	END
	ELSE IF (@WhatToGenerate = 'U') --CREATING update set list
	BEGIN	
			SET @columns = 'DECLARE @UpdatedDate datetime2 = GETDATE();' + CHAR(13) + CHAR(13) + 'UPDATE T' + CHAR(13) + 'SET' + CHAR(13) + @columns + 'FROM [' + @Schema + '].[' + @TableName + '] T WHERE ';
	END

	DECLARE @SQL varchar(max) = @columns;
	IF (@AutoExecute = 1)
	BEGIN
		 EXEC(@SQL);
	END
	ELSE
	BEGIN
		PRINT(@SQL);
		SELECT [SQL] = @SQL;
	END
GO

/*

	Interactive user objects.

*/


IF EXISTS (SELECT * FROM sys.objects WHERE [type] IN (N'FN', N'IF', N'TF', N'FS', N'FT') AND SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'sp_user_exists')
BEGIN
	DROP FUNCTION dbo.sp_user_exists;
END
GO
CREATE FUNCTION dbo.sp_user_exists (@SiteUserKey int)
RETURNS bit
WITH EXECUTE AS CALLER
AS
	BEGIN
		IF EXISTS (SELECT * FROM dbo.SiteUser WHERE PK = @SiteUserKey)
		BEGIN
			RETURN 1;
		END
		RETURN 0;		
	END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE [type] IN (N'FN', N'IF', N'TF', N'FS', N'FT') AND SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'sp_get_username')
BEGIN
	DROP FUNCTION dbo.sp_get_username;
END
GO
CREATE FUNCTION dbo.sp_get_username (@SiteUserKey int)
RETURNS nvarchar(50)
WITH EXECUTE AS CALLER
AS
	BEGIN		
		RETURN (SELECT TOP 1 UserName FROM dbo.SiteUser WHERE PK = @SiteUserKey);		
	END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE [type] IN (N'FN', N'IF', N'TF', N'FS', N'FT') AND SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'sp_user_authorized')
BEGIN
	DROP FUNCTION dbo.sp_user_authorized;
END
GO
CREATE FUNCTION dbo.sp_user_authorized (@SiteUserKey int, @AdminOnly bit = 0)
RETURNS bit
WITH EXECUTE AS CALLER
AS
	BEGIN
		IF EXISTS (SELECT * FROM dbo.SiteUser WHERE PK = @SiteUserKey AND Inactive = 0 AND (@AdminOnly = 0 OR IsAdmin = 1))
		BEGIN
			RETURN 1;
		END
		RETURN 0;		 
	END
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'usp_insert_DraftLog')
BEGIN
	DROP PROC dbo.usp_insert_DraftLog;
END
GO
CREATE PROC dbo.usp_insert_DraftLog
@ErrorType nchar(2) OUTPUT
, @ErrorMessage nvarchar(max) = NULL OUTPUT
, @ErrorCode nvarchar(10) = NULL OUTPUT 
, @SiteUserKey int 
, @Parameters varchar(max) 
, @Action nvarchar(200) 
, @Procedure nvarchar(200) 
AS
	BEGIN TRY

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRAN
	
	DECLARE @DateTimeStamp datetime2 = GETDATE();

	INSERT INTO [dbo].[DraftLog] (
		[SiteUserKey]
		, [Parameters]
		, [Action]
		, [Procedure]
		, [Status]
		, [ErrorCode]
		, [ErrorMessage]
		, [DateTimeStamp]
	)
	VALUES
	(@SiteUserKey
	, @Parameters
	, @Action
	, @Procedure
	, @ErrorType
	, @ErrorCode
	, @ErrorMessage
	, @DateTimeStamp)

	COMMIT TRAN

	END TRY
	BEGIN CATCH
		IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRAN
		END
		SET @ErrorCode = (SELECT ERROR_NUMBER());
		SET @ErrorType = 'ET'
		SET @ErrorMessage = (SELECT ERROR_MESSAGE());
				
		RAISERROR(@ErrorMessage, 16, 1);
		RETURN;
	END CATCH
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'usp_insert_SiteUser')
BEGIN
	DROP PROC dbo.usp_insert_SiteUser;
END
GO
CREATE PROC dbo.usp_insert_SiteUser
@ErrorType nchar(2) = 'ST' OUTPUT
, @ErrorMessage nvarchar(max) = NULL OUTPUT
, @ErrorCode nvarchar(10) = NULL OUTPUT 
, @Action nvarchar(200)
, @SiteUserKey int NULL OUTPUT

, @UserName [nvarchar](50) 
, @Password [varbinary](128) 
, @Hash [varbinary](128) 
, @Salt [varchar](32) 
, @FirstName [nvarchar](50) 
, @LastName [nvarchar](50) 
, @MiddleInitial [nvarchar](1)
, @UserEmail [nvarchar](200) 
, @UserImage [varbinary](max) 
, @IsAdmin [bit] = 0
--, @SiteUserKey int OUTPUT
AS
DECLARE @Procedure nvarchar(200) = 'dbo.usp_insert_SiteUser'
, @Parameters varchar(max) = '@UserName: ' + ISNULL(@UserName, 'NULL')
							+ '| @Password: ' + ISNULL(CONVERT(varchar, @Password), 'NULL')
							+ '| @Hash: ' + ISNULL(CONVERT(varchar, @Hash), 'NULL')
							+ '| @Salt: ' + ISNULL(@Salt, 'NULL')
							+ '| @FirstName: ' + ISNULL(@FirstName, 'NULL')
							+ '| @LastName: ' + ISNULL(@LastName, 'NULL')
							+ '| @MiddleInitial: ' + ISNULL(@MiddleInitial, 'NULL')
							+ '| @UserEmail: ' + ISNULL(@UserEmail, 'NULL')
							+ '| @UserImage: ' + ISNULL(CONVERT(varchar, @UserImage), 'NULL')
							+ '| @IsAdmin: ' + ISNULL(CONVERT(varchar, @IsAdmin), 'NULL');

BEGIN TRY

	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;

	IF (@ErrorType = 'ET')
	BEGIN
		RETURN;
	END

	IF EXISTS (SELECT * FROM dbo.SiteUser WHERE UserName = @UserName)
	BEGIN
		SET @ErrorCode = 'USER';
		SET @ErrorMessage = 'Username already exists';
		GOTO LogError;
	END
	
	IF EXISTS (SELECT * FROM dbo.SiteUser WHERE UserEmail = @UserEmail)
	BEGIN
		SET @ErrorCode = 'USEREMAIL';
		SET @ErrorMessage = 'Email address already exists for another username';
		GOTO LogError;
	END
	
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRAN

	DECLARE @CreatedDate datetime2 = GETDATE();

	INSERT INTO [dbo].[SiteUser] (
		[UserName]
		, [Password]
		, [Hash]
		, [Salt]
		, [FirstName]
		, [LastName]
		, [MiddleInitial]
		, [UserEmail]
		, [UserImage]
		, [IsAdmin]
		, [CreatedDate]
		, [CreatedBy]
	)
	VALUES
	(
		@UserName
		, @Password
		, @Hash
		, @Salt
		, @FirstName
		, @LastName
		, @MiddleInitial
		, @UserEmail
		, @UserImage
		, @IsAdmin
		, @CreatedDate
		, @UserName
	)

	COMMIT TRAN
	
	SET @ErrorType = 'CT';
	SET @ErrorMessage = NULL;
	SET @ErrorCode = NULL;

	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;

RETURN
LogError: 
	SET @ErrorType = (CASE WHEN @ErrorType != 'ST' THEN @ErrorType ELSE 'ET' END);
	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;
RETURN
END TRY
BEGIN CATCH
	IF (@@TRANCOUNT > 0)
	BEGIN
		ROLLBACK TRAN
	END
	SET @ErrorCode = (SELECT ERROR_NUMBER());
	SET @ErrorType = 'ET'
	SET @ErrorMessage = (SELECT ERROR_MESSAGE());

	EXEC dbo.usp_insert_DraftLog 
		 @ErrorType OUTPUT
		 , @ErrorMessage OUTPUT
		 , @ErrorCode OUTPUT
		 , @SiteUserKey
		 , @Parameters
		 , @Action
		 , @Procedure;
				
	--RAISERROR(@ErrorMessage, 16, 1);
	RETURN;
END CATCH
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'usp_update_SiteUser')
BEGIN
	DROP PROC dbo.usp_update_SiteUser;
END
GO
CREATE PROC dbo.usp_update_SiteUser
@ErrorType nchar(2) = 'ST' OUTPUT
, @ErrorMessage nvarchar(max) = NULL OUTPUT
, @ErrorCode nvarchar(10) = NULL OUTPUT 
, @Action nvarchar(200)
, @SiteUserKey int

, @FirstName [nvarchar](50) 
, @LastName [nvarchar](50) 
, @MiddleInitial [nvarchar](1)
, @UserEmail [nvarchar](200) 
, @UserImage [varbinary](max) 
, @PK int
AS
DECLARE @Procedure nvarchar(200) = 'dbo.usp_update_SiteUser'
, @Parameters varchar(max) = '@PK: ' + ISNULL(CONVERT(varchar, @PK), 'NULL')
							+ '| @FirstName: ' + ISNULL(@FirstName, 'NULL')
							+ '| @LastName: ' + ISNULL(@LastName, 'NULL')
							+ '| @MiddleInitial: ' + ISNULL(@MiddleInitial, 'NULL')
							+ '| @UserEmail: ' + ISNULL(@UserEmail, 'NULL')
							+ '| @UserImage: ' + ISNULL(CONVERT(varchar, @UserImage), 'NULL');

BEGIN TRY

	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;

	IF (@ErrorType = 'ET')
	BEGIN
		RETURN;
	END

	IF (dbo.sp_user_exists(@SiteUserKey) = 0)
	BEGIN
		SET @ErrorCode = 'UNAUTH';
		SET @ErrorMessage = 'Unauthorized';
		GOTO LogError;
	END
	
	IF (dbo.sp_user_exists(@PK) = 0)
	BEGIN
		SET @ErrorCode = 'USEREXIST';
		SET @ErrorMessage = 'User account cannot be found';
		GOTO LogError;
	END

	IF EXISTS (SELECT * FROM dbo.SiteUser WHERE UserEmail = @UserEmail)
	BEGIN
		SET @ErrorCode = 'USEREMAIL';
		SET @ErrorMessage = 'Email address already exists for another username';
		GOTO LogError;
	END
	
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRAN

	DECLARE @UpdatedDate datetime2 = GETDATE();

	UPDATE T
	SET
	[FirstName] = @FirstName
	, [LastName] = @LastName
	, [MiddleInitial] = @MiddleInitial
	, [UserEmail] = @UserEmail
	, [UserImage] = @UserImage
	, [UpdatedDate] = @UpdatedDate
	, [UpdatedBy] = dbo.sp_get_username(@SiteUserKey)
	FROM [dbo].[SiteUser] T 
	WHERE [PK] = @PK

	COMMIT TRAN
	
	SET @ErrorType = 'CT';

LogError: 
	SET @ErrorType = (CASE WHEN @ErrorType != 'ST' THEN @ErrorType ELSE 'ET' END);
	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;
RETURN
END TRY
BEGIN CATCH
	IF (@@TRANCOUNT > 0)
	BEGIN
		ROLLBACK TRAN
	END
	SET @ErrorCode = (SELECT ERROR_NUMBER());
	SET @ErrorType = 'ET'
	SET @ErrorMessage = (SELECT ERROR_MESSAGE());

	EXEC dbo.usp_insert_DraftLog 
		 @ErrorType OUTPUT
		 , @ErrorMessage OUTPUT
		 , @ErrorCode OUTPUT
		 , @SiteUserKey
		 , @Parameters
		 , @Action
		 , @Procedure;
				
	--RAISERROR(@ErrorMessage, 16, 1);
	RETURN;
END CATCH
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'usp_updatepassword_SiteUser')
BEGIN
	DROP PROC dbo.usp_updatepassword_SiteUser;
END
GO
CREATE PROC dbo.usp_updatepassword_SiteUser
@ErrorType nchar(2) = 'ST' OUTPUT
, @ErrorMessage nvarchar(max) = NULL OUTPUT
, @ErrorCode nvarchar(10) = NULL OUTPUT 
, @Action nvarchar(200)
, @SiteUserKey int NULL OUTPUT

, @Password [varbinary](128) 
, @Hash [varbinary](128) 
, @Salt [varchar](32) 
, @PK int
AS
DECLARE @Procedure nvarchar(200) = 'dbo.usp_updatepassword_SiteUser'
, @Parameters varchar(max) = '@PK: ' + ISNULL(CONVERT(varchar, @PK), 'NULL');

BEGIN TRY

	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;

	IF (@ErrorType = 'ET')
	BEGIN
		RETURN;
	END
	
	IF (dbo.sp_user_exists(@SiteUserKey) = 0)
	BEGIN
		SET @ErrorCode = 'UNAUTH';
		SET @ErrorMessage = 'Unauthorized';
		GOTO LogError;
	END
	
	IF (dbo.sp_user_exists(@PK) = 0)
	BEGIN
		SET @ErrorCode = 'USEREXIST';
		SET @ErrorMessage = 'User account cannot be found';
		GOTO LogError;
	END
	
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRAN

	DECLARE @UpdatedDate datetime2 = GETDATE();

	UPDATE T
	SET
	[Password] = @Password
	, [Hash] = @Hash
	, [Salt] = @Salt
	, [UpdatedDate] = @UpdatedDate
	, [UpdatedBy] = dbo.sp_get_username(@SiteUserKey)
	FROM [dbo].[SiteUser] T 
	WHERE [PK] = @PK

	COMMIT TRAN
	
	SET @ErrorType = 'CT';

	LogError: 
		SET @ErrorType = (CASE WHEN @ErrorType != 'ST' THEN @ErrorType ELSE 'ET' END);
		EXEC dbo.usp_insert_DraftLog 
		@ErrorType OUTPUT
		, @ErrorMessage OUTPUT
		, @ErrorCode OUTPUT
		, @SiteUserKey
		, @Parameters
		, @Action
		, @Procedure;
	RETURN
END TRY
BEGIN CATCH
	IF (@@TRANCOUNT > 0)
	BEGIN
		ROLLBACK TRAN
	END
	SET @ErrorCode = (SELECT ERROR_NUMBER());
	SET @ErrorType = 'ET'
	SET @ErrorMessage = (SELECT ERROR_MESSAGE());

	EXEC dbo.usp_insert_DraftLog 
		 @ErrorType OUTPUT
		 , @ErrorMessage OUTPUT
		 , @ErrorCode OUTPUT
		 , @SiteUserKey
		 , @Parameters
		 , @Action
		 , @Procedure;
				
	--RAISERROR(@ErrorMessage, 16, 1);
	RETURN;
END CATCH
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'usp_activate_SiteUser')
BEGIN
	DROP PROC dbo.usp_activate_SiteUser;
END
GO
CREATE PROC dbo.usp_activate_SiteUser
@ErrorType nchar(2) = 'ST' OUTPUT
, @ErrorMessage nvarchar(max) = NULL OUTPUT
, @ErrorCode nvarchar(10) = NULL OUTPUT 
, @Action nvarchar(200)
, @SiteUserKey int NULL OUTPUT

, @PK int
AS
DECLARE @Procedure nvarchar(200) = 'dbo.usp_activate_SiteUser'
, @Parameters varchar(max) = '@PK: ' + ISNULL(CONVERT(varchar, @PK), 'NULL');

BEGIN TRY

	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;

	IF (@ErrorType = 'ET')
	BEGIN
		RETURN;
	END
	
	IF (dbo.sp_user_exists(@SiteUserKey) = 0)
	BEGIN
		SET @ErrorCode = 'UNAUTH';
		SET @ErrorMessage = 'Unauthorized';
		GOTO LogError;
	END
	
	IF (dbo.sp_user_exists(@PK) = 0)
	BEGIN
		SET @ErrorCode = 'USEREXIST';
		SET @ErrorMessage = 'User account cannot be found';
		GOTO LogError;
	END
	
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRAN

	DECLARE @UpdatedDate datetime2 = GETDATE();

	UPDATE T
	SET
	[Inactive] = 0
	, [UpdatedDate] = @UpdatedDate
	, [UpdatedBy] = dbo.sp_get_username(@SiteUserKey)
	FROM [dbo].[SiteUser] T 
	WHERE [PK] = @PK

	COMMIT TRAN
	
	SET @ErrorType = 'CT';
	SET @ErrorMessage = 'Account Reactivated.';

LogError: 
	SET @ErrorType = (CASE WHEN @ErrorType != 'ST' THEN @ErrorType ELSE 'ET' END);
	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;
RETURN
END TRY
BEGIN CATCH
	IF (@@TRANCOUNT > 0)
	BEGIN
		ROLLBACK TRAN
	END
	SET @ErrorCode = (SELECT ERROR_NUMBER());
	SET @ErrorType = 'ET'
	SET @ErrorMessage = (SELECT ERROR_MESSAGE());

	EXEC dbo.usp_insert_DraftLog 
		 @ErrorType OUTPUT
		 , @ErrorMessage OUTPUT
		 , @ErrorCode OUTPUT
		 , @SiteUserKey
		 , @Parameters
		 , @Action
		 , @Procedure;
				
	--RAISERROR(@ErrorMessage, 16, 1);
	RETURN;
END CATCH
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'usp_deactivate_SiteUser')
BEGIN
	DROP PROC dbo.usp_deactivate_SiteUser;
END
GO
CREATE PROC dbo.usp_deactivate_SiteUser
@ErrorType nchar(2) = 'ST' OUTPUT
, @ErrorMessage nvarchar(max) = NULL OUTPUT
, @ErrorCode nvarchar(10) = NULL OUTPUT 
, @Action nvarchar(200)
, @SiteUserKey int NULL OUTPUT

, @PK int
AS
DECLARE @Procedure nvarchar(200) = 'dbo.usp_deactivate_SiteUser'
, @Parameters varchar(max) = '@PK: ' + ISNULL(CONVERT(varchar, @PK), 'NULL');

BEGIN TRY

	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;

	IF (@ErrorType = 'ET')
	BEGIN
		RETURN;
	END
	
	IF (dbo.sp_user_exists(@SiteUserKey) = 0)
	BEGIN
		SET @ErrorCode = 'UNAUTH';
		SET @ErrorMessage = 'Unauthorized';
		GOTO LogError;
	END
	
	IF (dbo.sp_user_authorized(@SiteUserKey, 1) = 0)
	BEGIN
		SET @ErrorCode = 'UNAUTH';
		SET @ErrorMessage = 'Unauthorized';
		GOTO LogError;
	END
		
	IF (dbo.sp_user_exists(@PK) = 0)
	BEGIN
		SET @ErrorCode = 'USEREXIST';
		SET @ErrorMessage = 'User account cannot be found';
		GOTO LogError;
	END
	
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRAN

	DECLARE @UpdatedDate datetime2 = GETDATE();

	UPDATE T
	SET
	[Inactive] = 1
	, [UpdatedDate] = @UpdatedDate
	, [UpdatedBy] = dbo.sp_get_username(@SiteUserKey)
	FROM [dbo].[SiteUser] T 
	WHERE [PK] = @PK

	COMMIT TRAN
	
	SET @ErrorType = 'CT';
	SET @ErrorMessage = 'Account deactivated.';

LogError: 
	SET @ErrorType = (CASE WHEN @ErrorType != 'ST' THEN @ErrorType ELSE 'ET' END);
	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;
RETURN
END TRY
BEGIN CATCH
	IF (@@TRANCOUNT > 0)
	BEGIN
		ROLLBACK TRAN
	END
	SET @ErrorCode = (SELECT ERROR_NUMBER());
	SET @ErrorType = 'ET'
	SET @ErrorMessage = (SELECT ERROR_MESSAGE());

	EXEC dbo.usp_insert_DraftLog 
		 @ErrorType OUTPUT
		 , @ErrorMessage OUTPUT
		 , @ErrorCode OUTPUT
		 , @SiteUserKey
		 , @Parameters
		 , @Action
		 , @Procedure;
				
	--RAISERROR(@ErrorMessage, 16, 1);
	RETURN;
END CATCH
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = 'dbo' AND [name] = 'usp_delete_SiteUser')
BEGIN
	DROP PROC dbo.usp_delete_SiteUser;
END
GO
CREATE PROC dbo.usp_delete_SiteUser
@ErrorType nchar(2) = 'ST' OUTPUT
, @ErrorMessage nvarchar(max) = NULL OUTPUT
, @ErrorCode nvarchar(10) = NULL OUTPUT 
, @Action nvarchar(200)
, @SiteUserKey int NULL OUTPUT

, @PK int
AS
DECLARE @Procedure nvarchar(200) = 'dbo.usp_delete_SiteUser'
, @Parameters varchar(max) = '@PK: ' + ISNULL(CONVERT(varchar, @PK), 'NULL');

BEGIN TRY

	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;

	IF (@ErrorType = 'ET')
	BEGIN
		RETURN;
	END
	
	IF (dbo.sp_user_exists(@SiteUserKey) = 0)
	BEGIN
		SET @ErrorCode = 'UNAUTH';
		SET @ErrorMessage = 'Unauthorized';
		GOTO LogError;
	END
	
	IF (dbo.sp_user_authorized(@SiteUserKey, 1) = 0)
	BEGIN
		SET @ErrorCode = 'UNAUTH';
		SET @ErrorMessage = 'Unauthorized';
		GOTO LogError;
	END
		
	IF (dbo.sp_user_exists(@PK) = 0)
	BEGIN
		SET @ErrorCode = 'USEREXIST';
		SET @ErrorMessage = 'User account cannot be found';
		GOTO LogError;
	END
	
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
	BEGIN TRAN

	DECLARE @UpdatedDate datetime2 = GETDATE();

	DELETE
	FROM [dbo].[SiteUser] 
	WHERE [PK] = @PK

	COMMIT TRAN
	
	SET @ErrorType = 'CT';
	SET @ErrorMessage = 'Account deleted.';

LogError: 
	SET @ErrorType = (CASE WHEN @ErrorType != 'ST' THEN @ErrorType ELSE 'ET' END);
	EXEC dbo.usp_insert_DraftLog 
	@ErrorType OUTPUT
	, @ErrorMessage OUTPUT
	, @ErrorCode OUTPUT
	, @SiteUserKey
	, @Parameters
	, @Action
	, @Procedure;
RETURN
END TRY
BEGIN CATCH
	IF (@@TRANCOUNT > 0)
	BEGIN
		ROLLBACK TRAN
	END
	SET @ErrorCode = (SELECT ERROR_NUMBER());
	SET @ErrorType = 'ET'
	SET @ErrorMessage = (SELECT ERROR_MESSAGE());

	EXEC dbo.usp_insert_DraftLog 
		 @ErrorType OUTPUT
		 , @ErrorMessage OUTPUT
		 , @ErrorCode OUTPUT
		 , @SiteUserKey
		 , @Parameters
		 , @Action
		 , @Procedure;
				
	--RAISERROR(@ErrorMessage, 16, 1);
	RETURN;
END CATCH
GO

