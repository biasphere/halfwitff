USE FFFFT
GO

IF NOT EXISTS (SELECT TOP 1 * FROM sys.schemas WHERE name = 'vew')
BEGIN
	EXEC('CREATE SCHEMA [vew] AUTHORIZATION [dbo]');
END
GO
IF NOT EXISTS (SELECT TOP 1 * FROM sys.schemas WHERE name = 'his')
BEGIN
	EXEC('CREATE SCHEMA [his] AUTHORIZATION [dbo]');
END
GO
IF NOT EXISTS (SELECT TOP 1 * FROM sys.schemas WHERE name = 'admin')
BEGIN
	EXEC('CREATE SCHEMA [admin] AUTHORIZATION [dbo]');
END
GO
IF NOT EXISTS (SELECT TOP 1 * FROM sys.schemas WHERE name = 'rpt')
BEGIN
	EXEC('CREATE SCHEMA [rpt] AUTHORIZATION [dbo]');
END
GO
IF NOT EXISTS (SELECT TOP 1 * FROM sys.schemas WHERE name = 'typ')
BEGIN
	EXEC('CREATE SCHEMA [typ] AUTHORIZATION [dbo]');
END
GO
IF NOT EXISTS (SELECT TOP 1 * FROM sys.schemas WHERE name = 'lu')
BEGIN
	EXEC('CREATE SCHEMA [lu] AUTHORIZATION [dbo]');
END
GO
IF NOT EXISTS (SELECT TOP 1 * FROM sys.schemas WHERE name = 'log')
BEGIN
	EXEC('CREATE SCHEMA [log] AUTHORIZATION [dbo]');
END
GO

BEGIN --drop views
	DECLARE @name varchar(max);
	DECLARE @SQL varchar(max);

	SELECT @name = (SELECT TOP 1 name FROM sys.views where SCHEMA_NAME(schema_id) = N'vew' ORDER BY [name] ASC);
	WHILE @name IS NOT NULL
	BEGIN	
		SELECT @SQL = 'DROP VIEW [vew].[' + RTRIM(@name) +']';
		EXEC (@SQL)
		PRINT 'Dropped View: vew.' + @name;
		SELECT @name = (SELECT TOP 1 name FROM sys.views where SCHEMA_NAME(schema_id) = N'vew' and [name] > @name ORDER BY [name] ASC);
	END
END
GO

BEGIN --drop procs
	DECLARE @name varchar(max);
	DECLARE @SQL varchar(max);
	SELECT @name = (SELECT TOP 1 [name] FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = N'dbo' ORDER BY [name]);
	WHILE @name IS NOT NULL
	BEGIN	
		SELECT @SQL = 'DROP PROCEDURE [dbo].[' + RTRIM(@name) +']';
		EXEC (@SQL)
		PRINT 'Dropped Stored Procedure: dbo.' + @name;
		SELECT @name = (SELECT TOP 1 [name] FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = N'dbo' and [name] > @name ORDER BY [name]);	
	END
END
GO

BEGIN --drop admin procs
	DECLARE @name nvarchar(max);
	DECLARE @SQL nvarchar(max);
	SELECT @name = (SELECT TOP 1 [name] FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = N'admin' ORDER BY [name]);
	WHILE @name IS NOT NULL
	BEGIN	
		SELECT @SQL = 'DROP PROCEDURE [admin].[' + RTRIM(@name) +']';
		EXEC (@SQL)
		PRINT 'Dropped Stored Procedure: admin.' + @name;
		SELECT @name = (SELECT TOP 1 [name] FROM sys.procedures WHERE SCHEMA_NAME(schema_id) = N'admin' and [name] > @name ORDER BY [name]);	
	END
END
GO

BEGIN /* Drop all functions */
	DECLARE @name varchar(max);
	DECLARE @SQL varchar(max);
	SELECT @name = (SELECT TOP 1 o.[name] FROM sys.objects o WHERE [type] IN (N'FN', N'IF', N'TF', N'FS', N'FT') AND SCHEMA_NAME(schema_id) = 'dbo' ORDER BY [name]);
	WHILE @name IS NOT NULL
	BEGIN
		SELECT @SQL = 'DROP FUNCTION [dbo].[' + RTRIM(@name) +']'
		EXEC (@SQL)
		PRINT 'Dropped Function: ' + @name
		SELECT @name = (SELECT TOP 1 o.[name] FROM sys.objects o WHERE [type] IN (N'FN', N'IF', N'TF', N'FS', N'FT') AND SCHEMA_NAME(schema_id) = 'dbo' AND o.[name] > @name ORDER BY o.[name]);
	END
END
GO

BEGIN --drop tables
	DECLARE @name varchar(max);
	DECLARE @SQL varchar(max);
	SELECT @name = (SELECT TOP 1 name FROM sys.tables where SCHEMA_NAME(schema_id) = N'dbo' ORDER BY [name] ASC);
	WHILE @name IS NOT NULL
	BEGIN	
		SELECT @SQL = 'DROP TABLE [dbo].[' + RTRIM(@name) +']';
		EXEC (@SQL)
		PRINT 'Dropped Table: dbo.' + @name;
		SELECT @name = (SELECT TOP 1 name FROM sys.tables where SCHEMA_NAME(schema_id) = N'dbo' and [name] > @name ORDER BY [name] ASC);
	END
END
GO

BEGIN --drop tables [lu]
	DECLARE @name varchar(max);
	DECLARE @SQL varchar(max);
	SELECT @name = (SELECT TOP 1 name FROM sys.tables where SCHEMA_NAME(schema_id) = N'lu' ORDER BY [name] ASC);
	WHILE @name IS NOT NULL
	BEGIN	
		SELECT @SQL = 'DROP TABLE [lu].[' + RTRIM(@name) +']';
		EXEC (@SQL)
		PRINT 'Dropped Table: lu.' + @name;
		SELECT @name = (SELECT TOP 1 name FROM sys.tables where SCHEMA_NAME(schema_id) = N'lu' and [name] > @name ORDER BY [name] ASC);
	END
END
GO

BEGIN --drop tables [admin]
	DECLARE @name varchar(max);
	DECLARE @SQL varchar(max);
	SELECT @name = (SELECT TOP 1 name FROM sys.tables where SCHEMA_NAME(schema_id) = N'admin' ORDER BY [name] ASC);
	WHILE @name IS NOT NULL
	BEGIN	
		SELECT @SQL = 'DROP TABLE [admin].[' + RTRIM(@name) +']';
		EXEC (@SQL)
		PRINT 'Dropped Table: admin.' + @name;
		SELECT @name = (SELECT TOP 1 name FROM sys.tables where SCHEMA_NAME(schema_id) = N'admin' and [name] > @name ORDER BY [name] ASC);
	END
END
GO

--drop all types
BEGIN
	DECLARE @name varchar(max);
	DECLARE @SQL varchar(max);
	DECLARE @typename varchar(50) = '';
	SET @typename = (SELECT TOP 1 name FROM sys.types WHERE is_table_type = 1 ORDER BY name ASC);
	WHILE @typename IS NOT NULL
	BEGIN
		SET @sql = 'DROP TYPE [typ].[' + @typename + '];';
		EXEC (@sql);
		SET @typename = (SELECT TOP 1 name FROM sys.types WHERE is_table_type = 1 AND name > @typename ORDER BY name ASC);
	END
END
GO

--admins 
IF OBJECT_ID('[admin].[Team]') IS NOT NULL
BEGIN
	DROP TABLE [admin].[Team]
END
GO
CREATE TABLE [admin].[Team] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[City] [varchar](50) NOT NULL,
	[TeamName] [varchar](50) NOT NULL,
	[FullName] [varchar](100) NOT NULL,
	[TeamUrl] [varchar](400) NOT NULL,
	[RosterUrl] [varchar](400) NOT NULL,
	[ScheduleUrl] [varchar](400) NOT NULL,
	[StatsUrl] [varchar](400) NOT NULL,
	[SiteKey] [char](1) NOT NULL,
	[ByeWeek] int NOT NULL,
	[IsActive] bit NOT NULL DEFAULT(1),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] varchar(6) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] varchar(6) NULL
	CONSTRAINT [PK_Team] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO

IF OBJECT_ID('[admin].[Player]') IS NOT NULL
BEGIN
	DROP TABLE [admin].[Player]
END
GO
CREATE TABLE [admin].[Player] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[SourceID] int NOT NULL,
	[SiteKey] char(1) NOT NULL,
	[TeamKey] int NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Height] int NOT NULL,
	[Weight] int NOT NULL,
	[Experience] int NOT NULL,
	[Age] int NOT NULL,
	[JerseyNumber] int NOT NULL,
	[PositionKey] [varchar](5) NOT NULL,
	[College] [varchar](100) NOT NULL,
	[Url] [varchar](400) NOT NULL,
	[IsActive] bit NOT NULL DEFAULT(1),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] varchar(6) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] varchar(6) NULL
	CONSTRAINT [PK_Player] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF OBJECT_ID('[admin].[PlayerStats]') IS NOT NULL
BEGIN
	DROP TABLE [admin].[PlayerStats]
END
GO
CREATE TABLE [admin].[PlayerStats] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[PlayerKey] int NOT NULL,
	[Year] int NOT NULL,
	[GamesPlayed] int NOT NULL,

	[Rushing_Yards] [decimal](10,2) NOT NULL,
	[Rushing_Att] [decimal](10,2) NOT NULL,
	[Rushing_TD] [decimal](10,2) NOT NULL,
	[Rushing_Fumbles] [decimal](10,2) NOT NULL,
	
	[Receiving_Yards] [decimal](10,2) NOT NULL,
	[Receiving_Rec] [decimal](10,2) NOT NULL,
	[Receiving_Att] [decimal](10,2) NOT NULL,
	[Receiving_TD] [decimal](10,2) NOT NULL,
	[Receiving_Fumbles] [decimal](10,2) NOT NULL,
	
	[Passing_Yards] [decimal](10,2) NOT NULL,
	[Passing_Comp] [decimal](10,2) NOT NULL,
	[Passing_Att] [decimal](10,2) NOT NULL,
	[Passing_TD] [decimal](10,2) NOT NULL,
	[Passing_Int] [decimal](10,2) NOT NULL,
	[Passing_Rating] [decimal](10,2) NOT NULL,
	[Passing_Fumbles] [decimal](10,2) NOT NULL,
	
	[Kicking_FGM] [decimal](10,2) NOT NULL,
	[Kicking_FGA] [decimal](10,2) NOT NULL,
	[Kicking_119] varchar(8) NOT NULL,
	[Kicking_2029] varchar(8) NOT NULL,
	[Kicking_3039] varchar(8) NOT NULL,
	[Kicking_4049] varchar(8) NOT NULL,
	[Kicking_50Plus] varchar(8) NOT NULL,
	[Kicking_Long] [decimal](10,2) NOT NULL,
	[Kicking_XPM] [decimal](10,2) NOT NULL,
	[Kicking_XPA] [decimal](10,2) NOT NULL,
	[Kicking_Points] [decimal](10,2) NOT NULL,
	
	[IsActive] bit NOT NULL DEFAULT(1),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] varchar(6) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] varchar(6) NULL
	CONSTRAINT [PK_PlayerStats] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF OBJECT_ID('[admin].[TeamStats]') IS NOT NULL
BEGIN
	DROP TABLE [admin].[TeamStats]
END
GO
CREATE TABLE [admin].[TeamStats] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[TeamKey] int NOT NULL,
	[Year] int NOT NULL,
	
	[Tackles] [decimal](10,2) NOT NULL,
	[Sacks] [decimal](10,2) NOT NULL,
	[TacklesForLoss] [decimal](10,2) NOT NULL,

	[PassesDefensed] [decimal](10,2) NOT NULL,
	[Interceptions] [decimal](10,2) NOT NULL,
	[Interceptions_TD] [decimal](10,2) NOT NULL,
	
	[Fumbles] [decimal](10,2) NOT NULL,
	[Fumbles_TD] [decimal](10,2) NOT NULL,
	[Fumbles_Recovered] [decimal](10,2) NOT NULL,

	[Kickoff_Avg] [decimal](10,2) NOT NULL,
	[Kickoff_TD] [decimal](10,2) NOT NULL,
	[Punt_Avg] [decimal](10,2) NOT NULL,
	[Punt_TD] [decimal](10,2) NOT NULL,
	
	[Penalties] [decimal](10,2) NOT NULL,
	[PenaltyYards] [decimal](10,2) NOT NULL,
	[ThirdDownPercentage] [decimal](10,2) NOT NULL,

	[IsActive] bit NOT NULL DEFAULT(1),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] varchar(6) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] varchar(6) NULL
	CONSTRAINT [PK_TeamStats] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF OBJECT_ID('[admin].[PlayerData]') IS NOT NULL
BEGIN
	DROP TABLE [admin].[PlayerData]
END
GO
CREATE TABLE [admin].[PlayerData] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[PlayerKey] int NOT NULL,
	[DataTypeKey] [char](1) NOT NULL,
	[Data] [varbinary](max) NOT NULL,
	[IsActive] bit NOT NULL DEFAULT(1),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] varchar(6) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] varchar(6) NULL
	CONSTRAINT [PK_PlayerData] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO

--creating the interactive tables
IF OBJECT_ID('[dbo].[DraftLeague]') IS NOT NULL
BEGIN
	DROP TABLE [dbo].[DraftLeague]
END
GO
CREATE TABLE [dbo].[DraftLeague] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[LeagueName] varchar(50) NOT NULL,
	[LeagueMotto] varchar(400) NULL,
	[LeagueSportCode] varchar(5) NOT NULL,
	[LeagueBuyIn] money NULL,
	[LeagueTeamCount] int NULL,
	[LeagueMaxPlayersPerTeam] int NULL,
	[LeagueHash] [varbinary](128) NOT NULL,
	[LeagueImage] [varbinary](max) NULL,
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] varchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] varchar(50) NULL
	CONSTRAINT [PK_DraftLeague] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF OBJECT_ID('[dbo].[SiteUser]') IS NOT NULL
BEGIN
	DROP TABLE [dbo].[SiteUser]
END
GO
CREATE TABLE [dbo].[SiteUser] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[UserName] varchar(50) NOT NULL,
	[Password] [varbinary](128) NULL,
	[Hash] [varbinary](128) NULL,
	[Salt] [varchar](32) NULL,
	[FirstName] varchar(50) NOT NULL,
	[LastName] varchar(50) NOT NULL,
	[MiddleInitial] varchar(1) NULL,
	[UserEmail] varchar(200) NOT NULL,
	[UserImage] varbinary(max) NULL,
	[IsAdmin] bit NOT NULL DEFAULT(0),
	[IsActive] bit NOT NULL DEFAULT(1),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] varchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] varchar(50) NULL
	CONSTRAINT [PK_SiteUser] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF OBJECT_ID('[dbo].[DraftLeaguePayout]') IS NOT NULL
BEGIN
	DROP TABLE [dbo].[DraftLeaguePayout]
END
GO
CREATE TABLE [dbo].[DraftLeaguePayout] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[DraftLeagueKey] int NOT NULL,
	[Ranking] int NOT NULL,
	[Payout] money NULL,
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] varchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] varchar(50) NULL
	CONSTRAINT [PK_DraftLeaguePayout] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF OBJECT_ID('[dbo].[DraftUser]') IS NOT NULL
BEGIN
	DROP TABLE [dbo].[DraftUser]
END
GO
CREATE TABLE [dbo].[DraftUser] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[SiteUserKey] int NOT NULL,
	[DraftLeagueKey] int NOT NULL,
	[DraftImage] [varbinary](max) NOT NULL,
	[IsCommissioner] bit NOT NULL DEFAULT(0),
	[IsCoCommissioner] bit NOT NULL DEFAULT(0),
	[IsPieceOfShit] bit NOT NULL DEFAULT(1),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] varchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] varchar(50) NULL
	CONSTRAINT [PK_DraftUser] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF OBJECT_ID('[dbo].[DraftTeam]') IS NOT NULL
BEGIN
	DROP TABLE [dbo].[DraftTeam]
END
GO
CREATE TABLE [dbo].[DraftTeam] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[DraftUserKey] int NOT NULL,
	[TeamName] varchar(50) NOT NULL,
	[TeamImage] [varbinary](max) NULL,
	[TeamMotto] varchar(100) NULL,
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] varchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] varchar(50) NULL
	CONSTRAINT [PK_DraftTeam] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF OBJECT_ID('[dbo].[PlayersDrafted]') IS NOT NULL
BEGIN
	DROP TABLE [dbo].[PlayersDrafted]
END
GO
CREATE TABLE [dbo].[PlayersDrafted] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[PlayerKey] int NOT NULL,
	[DraftTeamKey] int NOT NULL,
	[AmountPaid] money NOT NULL,
	[IsActive] bit NOT NULL DEFAULT(1),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] varchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] varchar(50) NULL
	CONSTRAINT [PK_PlayersDrafted] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO

IF OBJECT_ID('[dbo].[TeamsDrafted]') IS NOT NULL
BEGIN
	DROP TABLE [dbo].[TeamsDrafted];
END
GO
CREATE TABLE [dbo].[TeamsDrafted] (
	[PK] [int] NOT NULL IDENTITY(1,1),
	[TeamKey] int NOT NULL,
	[DraftTeamKey] int NOT NULL,
	[AmountPaid] money NOT NULL,
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] varchar(50) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] varchar(50) NULL
	CONSTRAINT [PK_TeamsDrafted] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO

IF OBJECT_ID('[log].[ErrorLog]') IS NOT NULL
BEGIN
	DROP TABLE [log].[ErrorLog]
END
GO
CREATE TABLE [log].[ErrorLog] (
	[PK] [bigint] NOT NULL IDENTITY(1,1),
	[SiteUserKey] int NOT NULL,
	[Procedure] varchar(200) NOT NULL,
	[Action] varchar(200) NOT NULL,
	[Parameters] varchar(max) NULL,
	[Status] varchar(3) NOT NULL,
	[ErrorCode] varchar(10) NULL,
	[ErrorMessage] varchar(max) NULL,
	[DateTimeStamp] datetime2 NOT NULL
	CONSTRAINT [PK_ErrorLog] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO

IF OBJECT_ID('[log].[AppLog]') IS NOT NULL
BEGIN
	DROP TABLE [log].[AppLog]
END
GO
CREATE TABLE [log].[AppLog] (
	[PK] [bigint] NOT NULL IDENTITY(1,1),
	[SiteUserKey] int NOT NULL,
	[Location] varchar(200) NOT NULL,
	[Action] varchar(200) NOT NULL,
	[Parameters] varchar(max) NULL,
	[Details] varchar(max) NULL,
	[DateTimeStamp] datetime2 NOT NULL
	CONSTRAINT [PK_AppLog] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO


--lus
IF OBJECT_ID('[lu].[Enums]') IS NOT NULL
BEGIN
	DROP TABLE [lu].[Enums]
END
GO
CREATE TABLE [lu].[Enums] (
	[GroupCode] [int] NOT NULL,
	[Description] [varchar](100) NOT NULL
	CONSTRAINT [PK_Enums] PRIMARY KEY CLUSTERED 
(
	[GroupCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO
IF OBJECT_ID('[lu].[Lookups]') IS NOT NULL
BEGIN
	DROP TABLE [lu].[Lookups]
END
GO
CREATE TABLE [lu].[Lookups] (
	[PK] int NOT NULL IDENTITY(1,1),
	[GroupCode] [int] NOT NULL,
	[AlphaCode] [varchar](5) NULL,
	[Code] [varchar](20) NOT NULL,
	[Description] [varchar](1000) NOT NULL,
	[IsActive] bit NOT NULL DEFAULT(1),
	[CreatedDate] datetime2 NOT NULL DEFAULT(GETDATE()),
	[CreatedBy] varchar(10) NOT NULL DEFAULT('SYSTEM'),
	[UpdatedDate] datetime2 NULL,
	[UpdatedBy] varchar(10) NULL
	CONSTRAINT [PK_Lookups] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY];	
GO

CREATE UNIQUE NONCLUSTERED INDEX UIX_PlayerData_PlayerKeyDataTypeKey
ON [admin].PlayerData (PlayerKey, DataTypeKey)
GO

--uniques
CREATE UNIQUE NONCLUSTERED INDEX UIX_SiteUser_UserName
ON [dbo].[SiteUser] ([UserName])
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_SiteUser_UserEmail
ON [dbo].[SiteUser] ([UserEmail])
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_DraftLeague_LeagueSportCodeLeagueHash
ON [dbo].[DraftLeague] ([LeagueSportCode], [LeagueHash])
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_DraftUser_SiteUserKeyDraftLeagueKey
ON [dbo].[DraftUser] ([SiteUserKey], [DraftLeagueKey])
WHERE [SiteUserKey] IS NOT NULL
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_PlayersDrafted_PlayerKeyDraftTeamKey
ON [dbo].[PlayersDrafted] ([PlayerKey], [DraftTeamKey])
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_TeamsDrafted_PlayerKeyTeamKey
ON [dbo].[TeamsDrafted] ([TeamKey], [DraftTeamKey])
GO

CREATE UNIQUE NONCLUSTERED INDEX UIX_Lookups_GroupCodeAlphaCodeCode
ON [lu].[Lookups] ([GroupCode], [AlphaCode], [Code])
GO

BEGIN TRY
BEGIN TRAN
DECLARE @CreatedDate datetime2 = GETDATE(), @CreatedBy char(6) = 'SYSTEM';

INSERT INTO [lu].[Enums] ([GroupCode], [Description])
VALUES
(10, 'Player Data Type')
, (20, 'Site') --sitekeys, know where the data comes from
, (30, 'Position')
, (40, 'League Type')
, (50, 'URLs');

INSERT INTO [lu].[Lookups] ([GroupCode], [AlphaCode], [Code], [Description], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy])
VALUES
(10, NULL, 'I', 'Image', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)
, (10, NULL, 'S', 'Stats', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)
, (10, NULL, 'O', 'Other', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)

, (20, NULL, 'E', 'ESPN', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)
, (20, NULL, 'Y', 'Yahoo', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)
, (20, NULL, 'C', 'CBS', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)

, (30, 'NFL', 'QB', 'Quarterback', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)
, (30, 'NFL', 'DEF', 'Defense', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)
, (30, 'NFL', 'RB', 'Running Back', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)
, (30, 'NFL', 'TE', 'Tight End', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)
, (30, 'NFL', 'K', 'Kicker', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)
, (30, 'NFL', 'PK', 'Place Kicker', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)
, (30, 'NFL', 'P', 'Punter', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)
, (30, 'NFL', 'WR', 'Wide Receiver', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)

, (40, NULL, 'NBA','NBA League', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)
, (40, NULL, 'MLB','MLB League', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)
, (40, NULL, 'NHL','NHL League', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)
, (40, NULL, 'NFL','NFL League', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)

, (50, 'E.T', 'NFL','https://www.espn.com/nfl/teams', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy) --espn url for teams
, (50, 'E.ST', 'NFL','http://www.espn.com/nfl/team/_/name/', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy) --espn starter url for specific team
, (50, 'E.TS', 'NFL','http://www.espn.com/nfl/team/schedule/_/name/{0}', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy) --espn starter url for specific team schedule
, (50, 'E.TR', 'NFL','http://www.espn.com/nfl/team/roster/_/name/{0}/{1}', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)--espn starter url for specific team roster
, (50, 'E.PS', 'NFL','http://www.espn.com/nfl/team/stats/_/name/{0}', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy)--espn starter url for specific team player stats

, (50, 'ESTAT', 'NFL','http://www.espn.com/nfl/statistics/team/_/stat/{0}/position/defense/{1}', @CreatedDate, @CreatedBy, @CreatedDate, @CreatedBy); --esp helper url for team stats

COMMIT TRAN
END TRY
BEGIN CATCH
	IF (@@TRANCOUNT > 0)
	BEGIN
		ROLLBACK TRAN
	END

	DECLARE @ErrorMessage varchar(max) = (SELECT ERROR_MESSAGE());
					
	RAISERROR(@ErrorMessage, 16, 1);
	RETURN;
END CATCH
GO

IF OBJECT_ID('[dbo].[usp_createtableobjects]') IS NOT NULL
BEGIN
	DROP PROC [dbo].[usp_createtableobjects];
END
GO
CREATE PROC [dbo].[usp_createtableobjects]
@TableName varchar(50)
, @Schema varchar(10)
, @WhatToGenerate varchar(2)
, @AutoExecute bit = 0
AS

	IF (@TableName IS NULL)
	BEGIN
		RAISERROR('Please input a @TableName parameter', 1, 16);
		RETURN;
	END
	IF (@Schema IS NULL)
	BEGIN
		RAISERROR('Please input a @Schema parameter', 1, 16);
		RETURN;
	END
	IF (@WhatToGenerate IS NULL)
	BEGIN
		RAISERROR('Please input a @WhatToGenerate parameter', 1, 16);
		RETURN;
	END
	DECLARE @columns varchar(max) = ''	
	, @ordinalposition int
	, @columninsert varchar(max) = ''
	, @columnname varchar(100)
	, @datatype varchar(100)
	, @datalength varchar(10)
	, @scale varchar(2)
	, @precision varchar(2)
	, @NULLable varchar(10)
	, @DEFAULT varchar(50)
	, @paramcreate varchar(max) = ''
	, @paramset varchar(max) = ''
	, @columnrow varchar(1000)
	, @columnnameparameter varchar(100);

	SET @ordinalposition = (SELECT DISTINCT TOP 1 C.ORDINAL_POSITION
						   FROM INFORMATION_SCHEMA.COLUMNS C
						   WHERE C.TABLE_NAME = @TableName
						   AND C.TABLE_SCHEMA = @Schema
						   ORDER BY C.ORDINAL_POSITION ASC);

	IF (@ordinalposition IS NULL)
	BEGIN
		RAISERROR('This schema/table does not exist in the database', 1, 16);
		RETURN;
	END
	WHILE @ordinalposition IS NOT NULL
	BEGIN
		SELECT DISTINCT
			@columnname = (CASE WHEN @ordinalposition > 1 AND @WhatToGenerate NOT IN ('MU','MUP') THEN ', [' ELSE '[' END) + (CASE WHEN @WhatToGenerate = 'Y' THEN 'his' ELSE '' END) + C.COLUMN_NAME + ']'
			, @datatype = ' [' + C.DATA_TYPE + ']'
			, @precision = CONVERT(varchar, C.NUMERIC_PRECISION)
			, @scale = CONVERT(varchar, ISNULL(C.NUMERIC_SCALE, 2))
			, @datalength = ISNULL('(' + CONVERT(varchar, C.CHARACTER_MAXIMUM_LENGTH) + ')', '')
			, @NULLable = (CASE WHEN C.IS_NULLABLE = 'NO' THEN ' NOT NULL' ELSE ' NULL' END)
			, @DEFAULT = ' ' + ISNULL('DEFAULT' + UPPER(C.COLUMN_DEFAULT), '')
		FROM INFORMATION_SCHEMA.COLUMNS C
		WHERE C.TABLE_NAME = @TableName
		AND C.TABLE_SCHEMA = @Schema
		AND C.COLUMN_NAME != @TableName + 'ID'
		AND C.ORDINAL_POSITION = @ordinalposition;

		--SET @columnrow = @columnname + @datatype + @datalength + @NULLable + @DEFAULT + CHAR(13);
		--SET @columns += @columnrow;

		IF (@WhatToGenerate = 'P') --CREATE PARAMETERS FOR INSERT
		BEGIN
			SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));
			IF (@columnname != 'CreatedBy' AND @columnname != 'CreatedDate' AND @columnname != 'UpdatedBy' AND @columnname != 'UpdatedDate')
			BEGIN
				SET @columns += (CASE WHEN @ordinalposition = 1 THEN '@' ELSE ', @' END)
								+ @columnname + @datatype + (CASE WHEN UPPER(LTRIM(RTRIM(@datatype))) = '[DECIMAL]' THEN '(' + @precision + ',' + @scale + ')'  ELSE @datalength END) + CHAR(13);
			END
		END
		ELSE IF (@WhatToGenerate = 'PS') --CREATE PARAMETER STRING FOR LOGGING
		BEGIN
			SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));
			IF (@columnname != 'CreatedBy' AND @columnname != 'CreatedDate' AND @columnname != 'UpdatedBy' AND @columnname != 'UpdatedDate')
			BEGIN
				SET @columns += (CASE WHEN @ordinalposition = 1 THEN '''@' ELSE '+ ' + '''| @' END)
								+ @columnname + ': '' + ISNULL(' + (CASE WHEN @datatype LIKE '%char%' THEN  '@' + @columnname ELSE 'CONVERT(varchar, @' + @columnname + ')' END) + ', ''NULL'')' + CHAR(13);
			END
		END
		ELSE IF (@WhatToGenerate = 'C') --CREATE CLASS members for c# object
		BEGIN			
			SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));
			IF (@columnname != 'CreatedBy' AND @columnname != 'CreatedDate' AND @columnname != 'UpdatedBy' AND @columnname != 'UpdatedDate')
			BEGIN
				SET @datatype = UPPER(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@datatype, '[' , ''), ']', ''), ',', ''))));
				SET @NULLable = LTRIM(RTRIM(@NULLable));
				--PRINT(@NULLable);
				SET @columns += '[DataMember(IsRequired = true)]' + CHAR(13) + 'public ' + 
								(CASE WHEN @datatype IN ('DATE', 'DATETIME', 'DATETIME2') THEN (CASE WHEN @NULLable = 'NOT NULL' THEN 'DateTime ' ELSE 'DateTime? ' END)
								WHEN @datatype = 'INT' THEN (CASE WHEN @NULLable = 'NOT NULL' THEN 'int ' ELSE 'int? ' END)
								WHEN @datatype = 'SMALLINT' THEN (CASE WHEN @NULLable = 'NOT NULL' THEN 'short ' ELSE 'short? ' END)
								WHEN @datatype = 'BIGINT' THEN (CASE WHEN @NULLable = 'NOT NULL' THEN 'long ' ELSE 'long? ' END)
								WHEN @datatype IN ('DECIMAL', 'MONEY') THEN (CASE WHEN @NULLable = 'NOT NULL' THEN 'decimal ' ELSE 'decimal? ' END)
								WHEN @datatype IN ('FLOAT') THEN (CASE WHEN @NULLable = 'NOT NULL' THEN 'float ' ELSE 'float? ' END)
								WHEN @datatype = 'BIT' THEN (CASE WHEN @NULLable = 'NOT NULL' THEN 'bool ' ELSE 'bool? ' END)							 
								WHEN @datatype IN ('NVARCHAR', 'VARCHAR', 'CHAR', 'NCHAR') THEN 'string ' 
								ELSE 'string '
								END) + @columnname + ' { get; set; }' + CHAR(13);
			END
		END
		ELSE IF (@WhatToGenerate = 'H') --create html objects
		BEGIN			
			SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));
			IF (@columnname != 'CreatedBy' AND @columnname != 'CreatedDate' AND @columnname != 'UpdatedBy' AND @columnname != 'UpdatedDate')
			BEGIN
				SET @datatype = UPPER(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@datatype, '[' , ''), ']', ''), ',', ''))));
				SET @NULLable = LTRIM(RTRIM(@NULLable));
				--PRINT(@NULLable);
				SET @columns += (CASE WHEN UPPER(RIGHT(@columnname, 3)) = 'KEY' THEN
									'<div class="divHorizontal">' + CHAR(13) 
									+ CHAR(9) + '<label for="' + @columnname + '">' + @columnname + '</label>' + CHAR(13)
									+ CHAR(9) + '<div class="ComboBoxDivStyle BlockRelativeOverride">' + CHAR(13)
									+ CHAR(9) + CHAR(9) + '<select runat="server" data-runatname="' + @columnname + '" class="stuff BlockRelativeOverride" clientidmode="Static" id="' + @columnname + '"></select>' + CHAR(13)
									+ CHAR(9) + '</div>' + CHAR(13)
									+ '</div>'								
								WHEN @datatype IN ('DATE', 'DATETIME', 'DATETIME2') THEN
									'<div class="divHorizontal">' + CHAR(13) 
									+ CHAR(9) + '<label for="' + @columnname + '">' + @columnname + '</label>' + CHAR(13)
									+ CHAR(9) + '<div class="DateTextBoxDivStyle BlockRelativeOverride">' + CHAR(13)
									+ CHAR(9) + CHAR(9) + '<input runat="server" data-runatname="' + @columnname + '" class="WrappedDateTextBoxStyle" clientidmode="Static" id="' + @columnname + '" type="text" />' + CHAR(13)
									+ CHAR(9) + '</div>' + CHAR(13)
									+ '</div>'
								WHEN @datatype IN ('INT','SMALLINT','BIGINT') THEN
									'<div class="divHorizontal">' + CHAR(13) 
									+ CHAR(9) + '<label for="' + @columnname + '">' + @columnname + '</label>' + CHAR(13)
									+ CHAR(9) + '<input name="' + @columnname + '" style="width:100px;" maxlength="' + @precision + '" value="' + (CASE WHEN @columnname = 'PK' THEN '-1' ELSE '' END) + '" class="NumbersOnlyClass" type="text" />' + CHAR(13)
									+ '</div>'
								WHEN @datatype IN ('MONEY') THEN
									'<div class="divHorizontal">' + CHAR(13) 
									+ CHAR(9) + '<label for="' + @columnname + '">' + @columnname + '</label>' + CHAR(13)
									+ CHAR(9) + '<input name="' + @columnname + '" style="width:100px;" value="$0.00" class="CurrencyClass" maxlength="10" type="text" />' + CHAR(13)
									+ '</div>'
								WHEN @datatype = 'BIT' THEN			
									'<div class="divRadioButtonLabel">' + CHAR(13) 
									+ CHAR(9) + '<label>' + @columnname + '</label>' + CHAR(13)
									+ CHAR(9) + '<div>' + CHAR(13)
									+ CHAR(9) + CHAR(9) + '<label for="' + @columnname + '_1">' + @columnname + '_true</label>' + CHAR(13)
									+ CHAR(9) + CHAR(9) + '<input name="' + @columnname + '" id="' + @columnname + '_1" type="radio" checked="checked" value="true" />' + CHAR(13)
									+ CHAR(9) + CHAR(9) + '<label for="' + @columnname + '_2">' + @columnname + '_false</label>' + CHAR(13)
									+ CHAR(9) + CHAR(9) + '<input name="' + @columnname + '" id="' + @columnname + '_2" type="radio" value="false" />' + CHAR(13)
									+ CHAR(9) + '</div>' + CHAR(13)
									+ '</div>'				 
								WHEN @datatype IN ('FLOAT', 'DECIMAL') THEN
									'<div class="divHorizontal">' + CHAR(13) 
									+ CHAR(9) + '<label for="' + @columnname + '">' + @columnname + '</label>' + CHAR(13)
									+ CHAR(9) + '<input name="' + @columnname + '" style="width:100px;" value="' + (CASE WHEN @columnname = 'PK' THEN '-1' ELSE '0' END) + '" class="DecimalsOnlyClass" data-scale="' + @scale + '" data-precision="' + @precision + '" maxlength="' + CONVERT(varchar,(1 + (CONVERT(int, @precision) + CONVERT(int, @scale)))) + '" type="text" />' + CHAR(13)
									+ '</div>'
								ELSE 
									'<div class="divHorizontal">' + CHAR(13) 
									+ CHAR(9) + '<label for="' + @columnname + '">' + @columnname + '</label>' + CHAR(13)
									+ CHAR(9) + '<input name="' + @columnname + '" ' + (CASE WHEN UPPER(@columnname) LIKE '%MOBILE%' OR UPPER(@columnname) LIKE '%PHONE%' OR UPPER(@columnname) LIKE '%FAX%' THEN 'class="PhoneNumberClass"' ELSE '' END) + ' style="width:100px;" value="' + (CASE WHEN @columnname = 'PK' THEN '-1' ELSE '' END) + '"  type="text" maxlength="' + REPLACE(REPLACE(@datalength, '(', '') ,')', '') + '" />' + CHAR(13)
									+ '</div>'
								END) + CHAR(13);
			END
		END
		ELSE IF (@WhatToGenerate = 'A') --ado.net sql param setup
		BEGIN			
			SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));
			IF (@columnname != 'CreatedBy' AND @columnname != 'CreatedDate' AND @columnname != 'UpdatedBy' AND @columnname != 'UpdatedDate')
			BEGIN
				SET @datatype = UPPER(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@datatype, '[' , ''), ']', ''), ',', ''))));
				SET @NULLable = LTRIM(RTRIM(@NULLable));
				--PRINT(@NULLable);
				SET @paramcreate += CHAR(13) + 'var ' + @columnname + ' = new SqlParameter("@' + @columnname + '", obj.' + @columnname + 
											(CASE WHEN 
											@datatype NOT IN ('DATE', 'DATETIME', 'DATETIME2', 'INT', 'SMALLINT', 'BIGINT', 'DECIMAL', 'MONEY', 'FLOAT', 'BIT') 
											THEN '.ToSQLNull()' ELSE '' END) + ');' +
									(CASE WHEN @datatype = 'DATE' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.Date;'
									WHEN @datatype = 'DATETIME' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.DateTime;'
									WHEN @datatype = 'DATETIME2' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.DateTime2;'
									WHEN @datatype = 'INT' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.Int;'
									WHEN @datatype = 'SMALLINT' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.SmallInt;'
									WHEN @datatype = 'BIGINT' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.BigInt;'
									WHEN @datatype = 'DECIMAL' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.Decimal;'
									WHEN @datatype = 'MONEY' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.Money;'
									WHEN @datatype = 'FLOAT' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.Float;'
									WHEN @datatype = 'BIT' THEN CHAR(13) + @columnname + '.SqlDbType = SqlDbType.Bit;'							 
									ELSE ''	END);
				SET @paramset += 'cmd.Parameters.Add(' + @columnname + ');' + CHAR(13);
			END
		END
		ELSE IF (@WhatToGenerate = 'E') --entity framework sql param setup
		BEGIN			
			SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));
			IF (@columnname != 'CreatedBy' AND @columnname != 'CreatedDate' AND @columnname != 'UpdatedBy' AND @columnname != 'UpdatedDate')
			BEGIN
				SET @datatype = UPPER(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@datatype, '[' , ''), ']', ''), ',', ''))));
				SET @NULLable = LTRIM(RTRIM(@NULLable));
				--PRINT(@NULLable);
				SET @paramset += CHAR(13) + (CASE WHEN @ordinalposition > 1 THEN ', obj.' ELSE 'obj.' END) + @columnname;								
			END
		END
		ELSE IF (@WhatToGenerate = 'N') --NULL checks
		BEGIN			
			SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));
			IF (@columnname != 'CreatedBy' AND @columnname != 'CreatedDate' AND @columnname != 'UpdatedBy' AND @columnname != 'UpdatedDate')
			BEGIN
				SET @datatype = UPPER(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@datatype, '[' , ''), ']', ''), ',', ''))));
				SET @NULLable = LTRIM(RTRIM(@NULLable));
				--PRINT(@NULLable);
				SET @paramset += CHAR(13) + (CASE WHEN @ordinalposition > 1 THEN '|| obj.' ELSE 'obj.' END) + @columnname;								
			END
		END
		ELSE IF (@WhatToGenerate = 'V') --validation (NULL catching)
		BEGIN			
			SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));
			IF (@columnname != 'CreatedBy' AND @columnname != 'CreatedDate' AND @columnname != 'UpdatedBy' AND @columnname != 'UpdatedDate')
			BEGIN
				SET @datatype = UPPER(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@datatype, '[' , ''), ']', ''), ',', ''))));
				SET @NULLable = LTRIM(RTRIM(@NULLable));
				--PRINT(@NULLable);
				IF (@NULLable = 'NOT NULL')
				BEGIN
					SET @columns += (CASE WHEN @ordinalposition > 1 THEN '|| ' ELSE '' END) +
									(CASE WHEN @datatype IN ('DATE', 'DATETIME', 'DATETIME2') THEN '(obj.' + @columnname + ' == NULL || obj.' + @columnname + ' == new DateTime(1900,1,1))' 
									WHEN @datatype = 'INT' THEN '(obj.' + @columnname + ' == NULL || obj.' + @columnname + ' < 0)'
									WHEN @datatype = 'SMALLINT' THEN '(obj.' + @columnname + ' == NULL || obj.' + @columnname + ' < 0)'
									WHEN @datatype = 'BIGINT' THEN '(obj.' + @columnname + ' == NULL || obj.' + @columnname + ' < 0)'
									WHEN @datatype IN ('DECIMAL', 'MONEY') THEN '(obj.' + @columnname + ' == NULL || obj.' + @columnname + ' < 0)' 
									WHEN @datatype IN ('FLOAT') THEN '(obj.' + @columnname + ' == NULL || obj.' + @columnname + ' < 0)'
									WHEN @datatype = 'BIT' THEN '(obj.' + @columnname + ' == NULL || obj.' + @columnname + ' < 0)'							 
									WHEN @datatype IN ('NVARCHAR', 'VARCHAR', 'CHAR', 'NCHAR') THEN 'string.IsNullOrEmpty(obj.' + @columnname + ')'
									ELSE 'string.IsNullOrEmpty(obj.' + @columnname + ')'
									END) + CHAR(13);
				END
				ELSE IF (@NULLable = 'NULL')
				BEGIN
					SET @columns += (CASE WHEN @ordinalposition > 1 THEN '|| ' ELSE '' END) +
									(CASE WHEN @datatype IN ('DATE', 'DATETIME', 'DATETIME2') THEN '(!obj.' + @columnname + '.HasValue || obj.' + @columnname + '.Value == new DateTime(1900,1,1))' 
									WHEN @datatype = 'INT' THEN '(!obj.' + @columnname + '.HasValue || obj.' + @columnname + ' < 0)'
									WHEN @datatype = 'SMALLINT' THEN ' (!obj.' + @columnname + '.HasValue || obj.' + @columnname + ' < 0)'
									WHEN @datatype = 'BIGINT' THEN '(!obj.' + @columnname + '.HasValue || obj.' + @columnname + ' < 0)'
									WHEN @datatype IN ('DECIMAL', 'MONEY') THEN '(!obj.' + @columnname + '.HasValue || obj.' + @columnname + ' < 0)' 
									WHEN @datatype IN ('FLOAT') THEN '(!obj.' + @columnname + '.HasValue || obj.' + @columnname + ' < 0)'
									WHEN @datatype = 'BIT' THEN '(!obj.' + @columnname + '.HasValue)'							 
									WHEN @datatype IN ('NVARCHAR', 'VARCHAR', 'CHAR', 'NCHAR') THEN 'string.IsNullOrEmpty(obj.' + @columnname + ')'
									ELSE 'string.IsNullOrEmpty(obj.' + @columnname + ')'
									END) + CHAR(13);
				END				
			END
		END
		ELSE IF (@WhatToGenerate IN ('T','Y')) --CREATING A TABLE type parameter WITH THE COLUMNS OF EXISTING table to replicate
		BEGIN
			SET @columns += @columnname + @datatype + @datalength + @NULLable + @DEFAULT + CHAR(13);
		END
		ELSE IF (@WhatToGenerate = 'S') --CREATING column list for select
		BEGIN			
			--IF (CHARINDEX('UpdatedBy', @columnname) < 1 AND CHARINDEX('UpdatedDate', @columnname) < 1 AND CHARINDEX('CreatedBy', @columnname) < 1 AND CHARINDEX('CreatedDate', @columnname) < 1)
			--BEGIN
			--END
			SET @columns += @columnname + CHAR(13);
		END
		ELSE IF (@WhatToGenerate = 'I') --CREATING column list for insert
		BEGIN			
			IF (CHARINDEX('UpdatedBy', @columnname) < 1 AND CHARINDEX('UpdatedDate', @columnname) < 1)
			BEGIN
				SET @columns += @columnname + CHAR(13);
				SET @columnname = LTRIM(RTRIM(REPLACE(REPLACE(
										(CASE WHEN CHARINDEX('CreatedBy', @columnname) > 0 THEN ', @UserID' 
											  WHEN CHARINDEX('CreatedDate', @columnname) > 0 THEN ', @ActionDate'
											  WHEN CHARINDEX('UpdatedBy', @columnname) > 0 THEN ', @UserID'
											  WHEN CHARINDEX('UpdatedDate', @columnname) > 0 THEN ', @ActionDate'
											  ELSE @columnname END)
									, '[' , '@'), ']', '')));
				SET @columninsert += @columnname + CHAR(13);
			END
		END
		ELSE IF (@WhatToGenerate = 'U') --CREATING update set list
		BEGIN
			SET @columnnameparameter = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(
											(CASE WHEN CHARINDEX('UpdatedBy', @columnname) > 0 THEN 'UserID' 
												  WHEN CHARINDEX('UpdatedDate', @columnname) > 0 THEN 'ActionDate'
												  ELSE @columnname END)
										, '[' , ''), ']', ''), ',', '')));	
			IF (CHARINDEX('CreatedBy', @columnname) < 1 AND CHARINDEX('CreatedDate', @columnname) < 1)
			BEGIN
				SET @columns += CHAR(13) + CHAR(10) + @columnname + ' = @' + @columnnameparameter;
			END
		END
		ELSE IF (@WhatToGenerate = 'MUP') --CREATING MERGE, update set list with parameters
		BEGIN
			SET @columnnameparameter = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(@columnname, '[' , ''), ']', ''), ',', '')));	
			IF (CHARINDEX('CreatedBy', @columnname) < 1 AND CHARINDEX('CreatedDate', @columnname) < 1)
			BEGIN				
				IF (CHARINDEX('IsActive', @columnname) < 1 AND CHARINDEX('UpdatedBy', @columnname) < 1 AND CHARINDEX('UpdatedDate', @columnname) < 1)
				BEGIN
					SET @columns += CHAR(13) + CHAR(10) + ', TARGET.' + @columnname + ' = (CASE WHEN @IsActive = 0 THEN TARGET.' + @columnname + ' ELSE @' + @columnnameparameter + ' END)';
				END
				ELSE IF (CHARINDEX('UpdatedBy', @columnname) > 0)
				BEGIN
					SET @columns += CHAR(13) + CHAR(10) + ', TARGET.' + @columnname + ' = @UserID';
				END	
				ELSE IF (CHARINDEX('UpdatedDate', @columnname) > 0)
				BEGIN
					SET @columns += CHAR(13) + CHAR(10) + ', TARGET.' + @columnname + ' = @ActionDate';
				END	
				ELSE
				BEGIN
					SET @columns += CHAR(13) + CHAR(10) + ', TARGET.' + @columnname + ' = @' + @columnnameparameter;
				END				
			END
		END
		ELSE IF (@WhatToGenerate = 'MU') --CREATING MERGE, update set list with TABLE parameter
		BEGIN
			IF (CHARINDEX('CreatedBy', @columnname) < 1 AND CHARINDEX('CreatedDate', @columnname) < 1)
			BEGIN				
				IF (CHARINDEX('IsActive', @columnname) < 1 AND CHARINDEX('UpdatedBy', @columnname) < 1 AND CHARINDEX('UpdatedDate', @columnname) < 1)
				BEGIN
					SET @columns += CHAR(13) + CHAR(10) + ', TARGET.' + @columnname + ' = (CASE WHEN SOURCE.' + @columnname + ' = 0 THEN TARGET.' + @columnname + ' ELSE SOURCE.' + @columnname + ' END)';
				END
				ELSE IF (CHARINDEX('UpdatedBy', @columnname) > 0)
				BEGIN
					SET @columns += CHAR(13) + CHAR(10) + ', TARGET.' + @columnname + ' = @UserID';
				END	
				ELSE IF (CHARINDEX('UpdatedDate', @columnname) > 0)
				BEGIN
					SET @columns += CHAR(13) + CHAR(10) + ', TARGET.' + @columnname + ' = @ActionDate';
				END
				ELSE
				BEGIN
					SET @columns += CHAR(13) + CHAR(10) + ', TARGET.' + @columnname + ' = SOURCE.' + @columnname;
				END				
			END
		END

		SET @ordinalposition = (SELECT DISTINCT TOP 1 C.ORDINAL_POSITION
								FROM INFORMATION_SCHEMA.COLUMNS C
								WHERE C.TABLE_NAME = @TableName
								AND C.TABLE_SCHEMA = @Schema
								AND C.ORDINAL_POSITION > @ordinalposition
								ORDER BY C.ORDINAL_POSITION ASC);
	END

	IF (@WhatToGenerate = 'P') --CREATE PARAMETERS FOR INSERT
	BEGIN	
		SET @columns = @columns + ', @UserID varchar(6)' + CHAR(13) + ', @SuccessValue bit OUTPUT' + CHAR(13) + ', @Message varchar(max) OUTPUT';
	END
	ELSE IF (@WhatToGenerate = 'A') --CREATING A TABLE type parameter WITH THE COLUMNS OF EXISTING table to replicate
	BEGIN
		SET @columns = 'var UserID = new SqlParameter("@UserID", userid);
						var SuccessValue = new SqlParameter("@SuccessValue", DBNull.Value);
						SuccessValue.Direction = ParameterDirection.Output;
						SuccessValue.SqlDbType = SqlDbType.Bit;
						var Message = new SqlParameter("@Message", DBNull.Value);
						Message.Direction = ParameterDirection.Output;
						Message.Size = 4000;' + CHAR(13) + @paramcreate + CHAR(13) + CHAR(13) +
						'using (con)
						{
                        // Create a SQL command to execute the sproc
                        var cmd = con.CreateCommand();
                        cmd.CommandTimeout = 180;
                        cmd.CommandText = "[admin].[]";
                        cmd.CommandType = CommandType.StoredProcedure;' + @paramset + CHAR(13) +
						'cmd.Parameters.Add(UserID);
                        cmd.Parameters.Add(SuccessValue);
                        cmd.Parameters.Add(Message);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Dispose();

                        data.StatusOk = (bool)SuccessValue.Value;
                        data.ReturnMessage = (string)(Message.Value == DBNull.Value ? string.Empty : Message.Value);
						}';
	END
	ELSE IF (@WhatToGenerate = 'E') --CREATING A TABLE type parameter WITH THE COLUMNS OF EXISTING table to replicate
	BEGIN
		SET @columns = '(' + @paramset + '
						, userid
                        , SuccessValue
                        , Message
						);';		 
	END
	ELSE IF (@WhatToGenerate IN ('T','Y')) --CREATING A TABLE type parameter WITH THE COLUMNS OF EXISTING table to replicate
	BEGIN
		IF (@WhatToGenerate = 'Y')
		BEGIN
			SET @columns += ', [his' + @TableName + 'ID] int' +
							CHAR(13) + ', [his' + @TableName + 'TraceID] int' +
							CHAR(13) + ', [his' + @TableName + 'DateAdded] datetime2' +
							CHAR(13) + ', [his' + @TableName + 'LastUpdateDate] datetime2' +
							CHAR(13) + ', [his' + @TableName + 'UserID] varchar(6)' +
							CHAR(13) + ', [ActionTaken] varchar(10)';
		END
		SET @columns = 'CREATE TYPE [tabletype].[' + (CASE WHEN @WhatToGenerate = 'T' THEN '' ELSE 'History' END) + @TableName + 'TableType] AS TABLE (' + CHAR(13) + @columns + ');';
	END
	ELSE IF (@WhatToGenerate = 'S') --CREATING column list for select
	BEGIN		
		SET @columns = 'SELECT' + CHAR(13) + @columns + 'FROM [' + @Schema + '].[' + @TableName + '] T;';		
	END
	ELSE IF (@WhatToGenerate = 'I') --CREATING column list for insert
	BEGIN		
		SET @columns = 'DECLARE @ActionDate datetime2 = GETDATE();' + CHAR(13) + CHAR(13) + 'INSERT INTO [' + @Schema + '].[' + @TableName + '] (' + CHAR(13) + @columns + ')' + CHAR(13) + 'VALUES' + CHAR(13) + '(' + CHAR(13) + @columninsert + ')';
	END
	ELSE IF (@WhatToGenerate = 'U') --CREATING update set list
	BEGIN	
			SET @columns = 'DECLARE @ActionDate datetime2 = GETDATE();' + CHAR(13) + CHAR(13) + 'UPDATE T' + CHAR(13) + 'SET' + CHAR(13) + @columns + 'FROM [' + @Schema + '].[' + @TableName + '] T WHERE ';
	END

	DECLARE @SQL varchar(max) = @columns;
	IF (@AutoExecute = 1)
	BEGIN
		 EXEC(@SQL);
	END
	ELSE
	BEGIN
		PRINT(@SQL);
		SELECT [SQL] = @SQL;
	END
GO
/*
	EXEC [dbo].[usp_createtableobjects] 'Player', 'admin', 'S';
	GO
*/

IF OBJECT_ID('[dbo].[usp_save_teams]') IS NOT NULL
BEGIN
	DROP PROC [dbo].[usp_save_teams];
END
GO
IF TYPE_ID('[typ].[TeamTableType]') IS NOT NULL
BEGIN
	DROP PROC [admin].[usp_LoadTeams];
END
GO
IF EXISTS (SELECT * FROM sys.types WHERE SCHEMA_NAME(schema_id) = 'typ' AND [name] = 'TeamTableType')
BEGIN
	DROP TYPE [typ].[TeamTableType];
END
GO
CREATE TYPE [typ].[TeamTableType] AS TABLE (
	[City] [varchar](50) NOT NULL 
	, [TeamName] [varchar](50) NOT NULL 
	, [FullName] [varchar](100) NOT NULL 
	, [TeamUrl] [varchar](400) NOT NULL
	, [RosterUrl] [varchar](400) NOT NULL
	, [ScheduleUrl] [varchar](400) NOT NULL
	, [StatsUrl] [varchar](400) NOT NULL
	, [SiteKey] [char](1) NOT NULL 
	, [ByeWeek] int NOT NULL
);
GO
CREATE PROC [dbo].[usp_save_teams]
@TeamTableType [typ].[TeamTableType] READONLY
, @UserID varchar(10) = 'SYSTEM'
AS
DECLARE @ActionDate datetime2 = GETDATE();
BEGIN TRY

	;WITH CTE (
		City
		, FullName
		, [TeamName]
		, SiteKey
		, [LeagueTypeKey]
		, [ByeWeek]
		, [TeamUrl]
		, [RosterUrl]
		, [ScheduleUrl]
		, [StatsUrl]
		, [IsActive]
		, CreatedBy
		, CreatedDate
		, UpdatedBy
		, UpdatedDate
	) AS (
		SELECT
			City
			, FullName
			, [TeamName]
			, SiteKey
			, [LeagueTypeKey]
			, [ByeWeek]
			, [TeamUrl]
			, [RosterUrl]
			, [ScheduleUrl]
			, [StatsUrl]
			, [IsActive]
			, CreatedBy
			, CreatedDate
			, UpdatedBy
			, UpdatedDate
		FROM [admin].[Team] T
		WHERE T.IsActive = 1
		AND EXISTS (SELECT * FROM @TeamTableType sub WHERE T.[City] = sub.[City] AND T.[TeamName] = sub.[TeamName])
	)
	MERGE INTO CTE WITH (ROWLOCK, UPDLOCK) AS TARGET
	USING (SELECT T.* FROM @TeamTableType T) AS SOURCE
	ON TARGET.[City] = SOURCE.[City] 
		AND TARGET.[TeamName] = SOURCE.[TeamName]
	WHEN MATCHED
		THEN UPDATE
		SET TARGET.[FullName] = SOURCE.[FullName]
			, TARGET.[TeamUrl] = SOURCE.[TeamUrl]
			, TARGET.[RosterUrl] = SOURCE.[RosterUrl]
			, TARGET.[ScheduleUrl] = SOURCE.[ScheduleUrl]
			, TARGET.[StatsUrl] = SOURCE.[StatsUrl]
			, TARGET.[SiteKey] = SOURCE.[SiteKey]
			, TARGET.[ByeWeek] = SOURCE.[ByeWeek]
			, TARGET.UpdatedDate = @ActionDate
			, TARGET.UpdatedBy = @UserID
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (
			City
			, FullName
			, [TeamName]
			, SiteKey
			, [ByeWeek]
			, [TeamUrl]
			, [RosterUrl]
			, [ScheduleUrl]
			, [StatsUrl]
			, [IsActive]
			, CreatedBy
			, CreatedDate
			, UpdatedBy
			, UpdatedDate
		) VALUES (
			City
			, FullName
			, [TeamName]
			, SiteKey
			, [ByeWeek]
			, [TeamUrl]
			, [RosterUrl]
			, [ScheduleUrl]
			, [StatsUrl]
			, 1 --[IsActive]
			, @UserID
			, @ActionDate
			, @UserID
			, @ActionDate
		);
		
	END TRY
	BEGIN CATCH		
		DECLARE @ErrorMessage varchar(max) = (SELECT ERROR_MESSAGE());				
		RAISERROR(@ErrorMessage, 16, 1);
	END CATCH
GO

IF OBJECT_ID('[dbo].[usp_save_players]') IS NOT NULL
BEGIN
	DROP PROC [dbo].[usp_save_players];
END
GO
IF TYPE_ID('[typ].[PlayerTableType]') IS NOT NULL
BEGIN
	DROP PROC [admin].[usp_SavePlayer];
END
GO
IF EXISTS (SELECT * FROM sys.types WHERE SCHEMA_NAME(schema_id) = 'typ' AND [name] = 'PlayerTableType')
BEGIN
	DROP TYPE [typ].[PlayerTableType];
END
GO
CREATE TYPE [typ].[PlayerTableType] AS TABLE (	
	[SourceID] int NOT NULL
	, [SiteKey] char(1) NOT NULL
	, [TeamKey] int NOT NULL
	, [Name] [varchar](100) NOT NULL
	, [Height] int NOT NULL
	, [Weight] int NOT NULL
	, [Experience] int NOT NULL
	, [Age] int NOT NULL
	, [JerseyNumber] int NOT NULL
	, [PositionKey] [varchar](5) NOT NULL
	, [College] [varchar](100) NOT NULL
	, [Url] [varchar](400) NOT NULL
);
GO
CREATE PROC [dbo].[usp_save_players]
@PlayerTableType [typ].[PlayerTableType] READONLY
, @UserID varchar(10) = 'SYSTEM'
AS
	DECLARE @ActionDate datetime2 = GETDATE();
	BEGIN TRY

	;WITH CTE (
		[SourceID]
		, [SiteKey]
		, [TeamKey]
		, [Name]
		, [Height]
		, [Weight]
		, [Experience]
		, [Age]
		, [JerseyNumber]
		, [PositionKey]
		, [College]
		, [Url]
		, [IsActive]
		, [CreatedDate]
		, [CreatedBy]
		, [UpdatedDate]
		, [UpdatedBy]
	) AS (
		SELECT
			[SourceID]
			, [SiteKey]
			, [TeamKey]
			, [Name]
			, [Height]
			, [Weight]
			, [Experience]
			, [Age]
			, [JerseyNumber]
			, [PositionKey]
			, [College]
			, [Url]
			, [IsActive]
			, [CreatedDate]
			, [CreatedBy]
			, [UpdatedDate]
			, [UpdatedBy]	
		FROM [admin].[Player] T
		WHERE T.IsActive = 1
		AND EXISTS (
			SELECT * 
			FROM @PlayerTableType sub 
			WHERE T.[SiteKey] = sub.[SiteKey]
			AND T.[TeamKey] = sub.[TeamKey]
			AND T.[Name] = sub.[Name]
		)
	)
	MERGE INTO CTE WITH (ROWLOCK, UPDLOCK) AS TARGET
	USING (SELECT T.* FROM @PlayerTableType T) as SOURCE
	ON TARGET.[SiteKey] = SOURCE.[SiteKey]
		AND TARGET.[TeamKey] = SOURCE.[TeamKey]
		AND TARGET.[Name] = SOURCE.[Name]
	WHEN MATCHED
		THEN UPDATE
		SET TARGET.[SourceID] = SOURCE.[SourceID]
			, TARGET.[Height] = SOURCE.[Height]
			, TARGET.[Weight] = SOURCE.[Weight]
			, TARGET.[Experience] = SOURCE.[Experience]
			, TARGET.[Age] = SOURCE.[Age]
			, TARGET.[JerseyNumber] = SOURCE.[JerseyNumber]
			, TARGET.[PositionKey] = SOURCE.[PositionKey]
			, TARGET.[College] = SOURCE.[College]
			, TARGET.[Url] = SOURCE.[Url]
			, TARGET.UpdatedDate = @ActionDate
			, TARGET.UpdatedBy = @UserID
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (
			[SourceID]
			, [SiteKey]
			, [TeamKey]
			, [Name]
			, [Height]
			, [Weight]
			, [Experience]
			, [Age]
			, [JerseyNumber]
			, [PositionKey]
			, [College]
			, [Url]
			, [IsActive]
			, [CreatedDate]
			, [CreatedBy]
			, [UpdatedDate]
			, [UpdatedBy]
		) VALUES (
			[SourceID]
			, [SiteKey]
			, [TeamKey]
			, [Name]
			, [Height]
			, [Weight]
			, [Experience]
			, [Age]
			, [JerseyNumber]
			, [PositionKey]
			, [College]
			, [Url]
			, 1 --[IsActive]
			, @ActionDate
			, @UserID
			, @ActionDate
			, @UserID
		);

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage varchar(max) = (SELECT ERROR_MESSAGE());				
		RAISERROR(@ErrorMessage, 16, 1);
		RETURN;
	END CATCH
GO

IF OBJECT_ID('[dbo].[usp_save_playerdatas]') IS NOT NULL
BEGIN
	DROP PROC [dbo].[usp_save_playerdatas];
END
GO
IF TYPE_ID('[typ].[PlayerDataTableType]') IS NOT NULL
BEGIN
	DROP PROC [admin].[usp_SavePlayerData];
END
GO
IF EXISTS (SELECT * FROM sys.types WHERE SCHEMA_NAME(schema_id) = 'typ' AND [name] = 'PlayerDataTableType')
BEGIN
	DROP TYPE [typ].[PlayerDataTableType];
END
GO
CREATE TYPE [typ].[PlayerDataTableType] AS TABLE (
	[PlayerKey] [int] NOT NULL 
	, [DataTypeKey] [char](1) NOT NULL 
	, [Data] [varbinary](max) NOT NULL 
	, [IsActive] [bit] NOT NULL DEFAULT((1))
);
GO
CREATE PROC [dbo].[usp_save_playerdatas]
@PlayerDataTableType [typ].[PlayerDataTableType] READONLY
, @UserID varchar(10) = 'SYSTEM'
AS
	DECLARE @ActionDate datetime2 = GETDATE();
	BEGIN TRY
		
	;WITH CTE (
		[PlayerKey]
		, [DataTypeKey]
		, [Data]
		, [IsActive]
		, [CreatedDate]
		, [CreatedBy]
		, [UpdatedDate]
		, [UpdatedBy]
	) AS (
		SELECT
			[PlayerKey]
			, [DataTypeKey]
			, [Data]
			, [IsActive]
			, [CreatedDate]
			, [CreatedBy]
			, [UpdatedDate]
			, [UpdatedBy]
		FROM [admin].[PlayerData] T
		WHERE T.IsActive = 1
		AND EXISTS (
			SELECT * 
			FROM @PlayerDataTableType sub 
			WHERE T.[PlayerKey] = sub.[PlayerKey]
			AND T.[DataTypeKey] = sub.[DataTypeKey]
		)
	)
	MERGE INTO CTE WITH (ROWLOCK, UPDLOCK) AS TARGET
	USING (SELECT T.* FROM [admin].[PlayerData] T) AS SOURCE
	ON TARGET.[PlayerKey] = SOURCE.[PlayerKey]
		AND TARGET.[DataTypeKey] = SOURCE.[DataTypeKey]
	WHEN MATCHED
		THEN UPDATE
		SET TARGET.[Data] = SOURCE.[Data]
			, TARGET.[UpdatedDate] = @ActionDate
			, TARGET.[UpdatedBy] = @UserID
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (
			[PlayerKey]
			, [DataTypeKey]
			, [Data]
			, [IsActive]
			, CreatedBy
			, CreatedDate
			, UpdatedBy
			, UpdatedDate
		) VALUES (
			[PlayerKey]
			, [DataTypeKey]
			, [Data]
			, 1 --[IsActive]
			, @UserID
			, @ActionDate
			, @UserID
			, @ActionDate
		);
		
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage varchar(max) = (SELECT ERROR_MESSAGE());				
		RAISERROR(@ErrorMessage, 16, 1);
		RETURN;
	END CATCH
GO

IF OBJECT_ID('[dbo].[usp_save_playerstats]') IS NOT NULL
BEGIN
	DROP PROC [dbo].[usp_save_playerstats];
END
GO
IF TYPE_ID('[typ].[PlayerStatsTableType]') IS NOT NULL
BEGIN
	DROP PROC [admin].[usp_SavePlayerStats];
END
GO
IF EXISTS (SELECT * FROM sys.types WHERE SCHEMA_NAME(schema_id) = 'typ' AND [name] = 'PlayerStatsTableType')
BEGIN
	DROP TYPE [typ].[PlayerStatsTableType];
END
GO
CREATE TYPE [typ].[PlayerStatsTableType] AS TABLE (
	[PK] int NOT NULL
	, [PlayerKey] int NOT NULL 
	, [Year] int NOT NULL
	, [GamesPlayed] int NOT NULL 
	, [Rushing_Yards] [decimal](10,2) NOT NULL 
	, [Rushing_Att] [decimal](10,2) NOT NULL 
	, [Rushing_TD] [decimal](10,2) NOT NULL 
	, [Rushing_Fumbles] [decimal](10,2) NOT NULL 
	, [Receiving_Yards] [decimal](10,2) NOT NULL 
	, [Receiving_Rec] [decimal](10,2) NOT NULL 
	, [Receiving_Att] [decimal](10,2) NOT NULL 
	, [Receiving_TD] [decimal](10,2) NOT NULL 
	, [Receiving_Fumbles] [decimal](10,2) NOT NULL 
	, [Passing_Yards] [decimal](10,2) NOT NULL 
	, [Passing_Comp] [decimal](10,2) NOT NULL 
	, [Passing_Att] [decimal](10,2) NOT NULL 
	, [Passing_TD] [decimal](10,2) NOT NULL 
	, [Passing_Int] [decimal](10,2) NOT NULL 
	, [Passing_Rating] [decimal](10,2) NOT NULL 
	, [Passing_Fumbles] [decimal](10,2) NOT NULL 
	, [Kicking_FGM] [decimal](10,2) NOT NULL 
	, [Kicking_FGA] [decimal](10,2) NOT NULL 
	, [Kicking_119] varchar(8) NOT NULL 
	, [Kicking_2029] varchar(8) NOT NULL 
	, [Kicking_3039] varchar(8) NOT NULL 
	, [Kicking_4049] varchar(8) NOT NULL 
	, [Kicking_50Plus] varchar(8) NOT NULL 
	, [Kicking_Long] [decimal](10,2) NOT NULL 
	, [Kicking_XPM] [decimal](10,2) NOT NULL 
	, [Kicking_XPA] [decimal](10,2) NOT NULL 
	, [Kicking_Points] [decimal](10,2) NOT NULL 
	, [IsActive] [bit] NOT NULL DEFAULT((1))
);
GO
CREATE PROC [dbo].[usp_save_playerstats]
@PlayerStatsTableType [typ].[PlayerStatsTableType] READONLY
, @UserID varchar(10) = 'SYSTEM'
AS
	DECLARE @ActionDate datetime2 = GETDATE();
	BEGIN TRY
		
	;WITH CTE (
		[PlayerKey]
		, [Year]
		, [GamesPlayed]
		, [Rushing_Yards]
		, [Rushing_Att]
		, [Rushing_TD]
		, [Rushing_Fumbles]
		, [Receiving_Yards]
		, [Receiving_Rec]
		, [Receiving_Att]
		, [Receiving_TD]
		, [Receiving_Fumbles]
		, [Passing_Yards]
		, [Passing_Comp]
		, [Passing_Att]
		, [Passing_TD]
		, [Passing_Int]
		, [Passing_Rating]
		, [Passing_Fumbles]
		, [Kicking_FGM]
		, [Kicking_FGA]
		, [Kicking_119]
		, [Kicking_2029]
		, [Kicking_3039]
		, [Kicking_4049]
		, [Kicking_50Plus]
		, [Kicking_Long]
		, [Kicking_XPM]
		, [Kicking_XPA]
		, [Kicking_Points]
		, [IsActive]
		, [CreatedDate]
		, [CreatedBy]
		, [UpdatedDate]
		, [UpdatedBy]
	) AS (
		SELECT
			[PlayerKey]
			, [Year]
			, [GamesPlayed]
			, [Rushing_Yards]
			, [Rushing_Att]
			, [Rushing_TD]
			, [Rushing_Fumbles]
			, [Receiving_Yards]
			, [Receiving_Rec]
			, [Receiving_Att]
			, [Receiving_TD]
			, [Receiving_Fumbles]
			, [Passing_Yards]
			, [Passing_Comp]
			, [Passing_Att]
			, [Passing_TD]
			, [Passing_Int]
			, [Passing_Rating]
			, [Passing_Fumbles]
			, [Kicking_FGM]
			, [Kicking_FGA]
			, [Kicking_119]
			, [Kicking_2029]
			, [Kicking_3039]
			, [Kicking_4049]
			, [Kicking_50Plus]
			, [Kicking_Long]
			, [Kicking_XPM]
			, [Kicking_XPA]
			, [Kicking_Points]
			, [IsActive]
			, [CreatedDate]
			, [CreatedBy]
			, [UpdatedDate]
			, [UpdatedBy]
		FROM [admin].[PlayerStats] T
		WHERE T.IsActive = 1
		AND EXISTS (
			SELECT * 
			FROM @PlayerStatsTableType sub 
			WHERE T.[PlayerKey] = sub.[PlayerKey]
			AND T.[Year] = sub.[Year]
		)
	)
	MERGE INTO CTE WITH (ROWLOCK, UPDLOCK) AS TARGET
	USING (SELECT T.* FROM @PlayerStatsTableType T) AS SOURCE
	ON TARGET.[PlayerKey] = SOURCE.[PlayerKey]
		AND TARGET.[Year] = SOURCE.[Year]
	WHEN MATCHED
		THEN UPDATE
		SET TARGET.[GamesPlayed] = SOURCE.[GamesPlayed]
			, TARGET.[Rushing_Yards] = SOURCE.[Rushing_Yards]
			, TARGET.[Rushing_Att] = SOURCE.[Rushing_Att]
			, TARGET.[Rushing_TD] = SOURCE.[Rushing_TD]
			, TARGET.[Rushing_Fumbles] = SOURCE.[Rushing_Fumbles]
			, TARGET.[Receiving_Yards] = SOURCE.[Receiving_Yards]
			, TARGET.[Receiving_Rec] = SOURCE.[Receiving_Rec]
			, TARGET.[Receiving_Att] = SOURCE.[Receiving_Att]
			, TARGET.[Receiving_TD] = SOURCE.[Receiving_TD]
			, TARGET.[Receiving_Fumbles] = SOURCE.[Receiving_Fumbles]
			, TARGET.[Passing_Yards] = SOURCE.[Passing_Yards]
			, TARGET.[Passing_Comp] = SOURCE.[Passing_Comp]
			, TARGET.[Passing_Att] = SOURCE.[Passing_Att]
			, TARGET.[Passing_TD] = SOURCE.[Passing_TD]
			, TARGET.[Passing_Int] = SOURCE.[Passing_Int]
			, TARGET.[Passing_Rating] = SOURCE.[Passing_Rating]
			, TARGET.[Passing_Fumbles] = SOURCE.[Passing_Fumbles]
			, TARGET.[Kicking_FGM] = SOURCE.[Kicking_FGM]
			, TARGET.[Kicking_FGA] = SOURCE.[Kicking_FGA]
			, TARGET.[Kicking_119] = SOURCE.[Kicking_119]
			, TARGET.[Kicking_2029] = SOURCE.[Kicking_2029]
			, TARGET.[Kicking_3039] = SOURCE.[Kicking_3039]
			, TARGET.[Kicking_4049] = SOURCE.[Kicking_4049]
			, TARGET.[Kicking_50Plus] = SOURCE.[Kicking_50Plus]
			, TARGET.[Kicking_Long] = SOURCE.[Kicking_Long]
			, TARGET.[Kicking_XPM] = SOURCE.[Kicking_XPM]
			, TARGET.[Kicking_XPA] = SOURCE.[Kicking_XPA]
			, TARGET.[Kicking_Points] = SOURCE.[Kicking_Points]
			, TARGET.[UpdatedDate] = @ActionDate
			, TARGET.[UpdatedBy] = @UserID
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (
			[PlayerKey]
			, [Year]
			, [GamesPlayed]
			, [Rushing_Yards]
			, [Rushing_Att]
			, [Rushing_TD]
			, [Rushing_Fumbles]
			, [Receiving_Yards]
			, [Receiving_Rec]
			, [Receiving_Att]
			, [Receiving_TD]
			, [Receiving_Fumbles]
			, [Passing_Yards]
			, [Passing_Comp]
			, [Passing_Att]
			, [Passing_TD]
			, [Passing_Int]
			, [Passing_Rating]
			, [Passing_Fumbles]
			, [Kicking_FGM]
			, [Kicking_FGA]
			, [Kicking_119]
			, [Kicking_2029]
			, [Kicking_3039]
			, [Kicking_4049]
			, [Kicking_50Plus]
			, [Kicking_Long]
			, [Kicking_XPM]
			, [Kicking_XPA]
			, [Kicking_Points]
			, [IsActive]
			, CreatedBy
			, CreatedDate
			, UpdatedBy
			, UpdatedDate
		) VALUES (
			[PlayerKey]
			, [Year]
			, [GamesPlayed]
			, [Rushing_Yards]
			, [Rushing_Att]
			, [Rushing_TD]
			, [Rushing_Fumbles]
			, [Receiving_Yards]
			, [Receiving_Rec]
			, [Receiving_Att]
			, [Receiving_TD]
			, [Receiving_Fumbles]
			, [Passing_Yards]
			, [Passing_Comp]
			, [Passing_Att]
			, [Passing_TD]
			, [Passing_Int]
			, [Passing_Rating]
			, [Passing_Fumbles]
			, [Kicking_FGM]
			, [Kicking_FGA]
			, [Kicking_119]
			, [Kicking_2029]
			, [Kicking_3039]
			, [Kicking_4049]
			, [Kicking_50Plus]
			, [Kicking_Long]
			, [Kicking_XPM]
			, [Kicking_XPA]
			, [Kicking_Points]
			, 1 --[IsActive]
			, @UserID
			, @ActionDate
			, @UserID
			, @ActionDate
		);


	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage varchar(max) = (SELECT ERROR_MESSAGE());				
		RAISERROR(@ErrorMessage, 16, 1);
		RETURN;
	END CATCH
GO

IF OBJECT_ID('[dbo].[usp_save_teamstats]') IS NOT NULL
BEGIN
	DROP PROC [dbo].[usp_save_teamstats];
END
GO
IF TYPE_ID('[typ].[TeamStatsTableType]') IS NOT NULL
BEGIN
	DROP TYPE [typ].[TeamStatsTableType];
END
GO
CREATE TYPE [typ].[TeamStatsTableType] AS TABLE (
	[PK] int NOT NULL
	, [TeamKey] [int] NOT NULL 
	, [Year] [int] NOT NULL 
	, [Tackles] [decimal](10,2) NOT NULL 
	, [Sacks] [decimal](10,2) NOT NULL 
	, [TacklesForLoss] [decimal](10,2) NOT NULL 
	, [PassesDefensed] [decimal](10,2) NOT NULL 
	, [Interceptions] [decimal](10,2) NOT NULL 
	, [Interceptions_TD] [decimal](10,2) NOT NULL 
	, [Fumbles] [decimal](10,2) NOT NULL 
	, [Fumbles_TD] [decimal](10,2) NOT NULL 
	, [Fumbles_Recovered] [decimal](10,2) NOT NULL 
	, [Kickoff_Avg] [decimal](10,2) NOT NULL 
	, [Kickoff_TD] [decimal](10,2) NOT NULL 
	, [Punt_Avg] [decimal](10,2) NOT NULL 
	, [Punt_TD] [decimal](10,2) NOT NULL
	, [Penalties] [decimal](10,2) NOT NULL
	, [PenaltyYards] [decimal](10,2) NOT NULL
	, [ThirdDownPercentage] [decimal](10,2) NOT NULL
);
GO
CREATE PROC [dbo].[usp_save_teamstats]
@TeamStatsTableType [typ].[TeamStatsTableType] READONLY
, @UserID varchar(10) = 'SYSTEM'
AS
	DECLARE @ActionDate datetime2 = GETDATE();
	BEGIN TRY
		
	;WITH CTE (
		[TeamKey]
		, [Year]
		, [Tackles]
		, [Sacks]
		, [TacklesForLoss]
		, [PassesDefensed]
		, [Interceptions]
		, [Interceptions_TD]
		, [Fumbles]
		, [Fumbles_TD]
		, [Fumbles_Recovered]
		, [Kickoff_Avg]
		, [Kickoff_TD]
		, [Punt_Avg]
		, [Punt_TD]
		, [Penalties]
		, [PenaltyYards]
		, [ThirdDownPercentage]
		, [IsActive]
		, [CreatedDate]
		, [CreatedBy]
		, [UpdatedDate]
		, [UpdatedBy]
	) AS (
		SELECT
			[TeamKey]
			, [Year]
			, [Tackles]
			, [Sacks]
			, [TacklesForLoss]
			, [PassesDefensed]
			, [Interceptions]
			, [Interceptions_TD]
			, [Fumbles]
			, [Fumbles_TD]
			, [Fumbles_Recovered]
			, [Kickoff_Avg]
			, [Kickoff_TD]
			, [Punt_Avg]
			, [Punt_TD]
			, [Penalties]
			, [PenaltyYards]
			, [ThirdDownPercentage]
			, [IsActive]
			, [CreatedDate]
			, [CreatedBy]
			, [UpdatedDate]
			, [UpdatedBy]
		FROM [admin].TeamStats T
		WHERE T.IsActive = 1
		AND EXISTS (
			SELECT * 
			FROM @TeamStatsTableType sub 
			WHERE T.[TeamKey] = sub.[TeamKey]
			AND T.[Year] = sub.[Year] 
		)
	)
	MERGE INTO CTE WITH (ROWLOCK, UPDLOCK) AS TARGET
	USING (SELECT T.* FROM @TeamStatsTableType T) AS SOURCE
	ON TARGET.[TeamKey] = SOURCE.[TeamKey]
		AND TARGET.[Year] = SOURCE.[Year]
	WHEN MATCHED
		THEN UPDATE
		SET TARGET.[Tackles] = SOURCE.[Tackles]
			, TARGET.[Sacks] = SOURCE.[Sacks]
			, TARGET.[TacklesForLoss] = SOURCE.[TacklesForLoss]
			, TARGET.[PassesDefensed] = SOURCE.[PassesDefensed]
			, TARGET.[Interceptions] = SOURCE.[Interceptions]
			, TARGET.[Interceptions_TD] = SOURCE.[Interceptions_TD]
			, TARGET.[Fumbles] = SOURCE.[Fumbles]
			, TARGET.[Fumbles_TD] = SOURCE.[Fumbles_TD]
			, TARGET.[Fumbles_Recovered] = SOURCE.[Fumbles_Recovered]
			, TARGET.[Kickoff_Avg] = SOURCE.[Kickoff_Avg]
			, TARGET.[Kickoff_TD] = SOURCE.[Kickoff_TD]
			, TARGET.[Punt_Avg] = SOURCE.[Punt_Avg]
			, TARGET.[Punt_TD] = SOURCE.[Punt_TD]
			, TARGET.[Penalties] = SOURCE.[Penalties]
			, TARGET.[PenaltyYards] = SOURCE.[PenaltyYards]
			, TARGET.[ThirdDownPercentage] = SOURCE.[ThirdDownPercentage]
			, TARGET.UpdatedDate = @ActionDate
			, TARGET.UpdatedBy = @UserID
	WHEN NOT MATCHED BY TARGET
		THEN INSERT (
			[TeamKey]
			, [Year]
			, [Tackles]
			, [Sacks]
			, [TacklesForLoss]
			, [PassesDefensed]
			, [Interceptions]
			, [Interceptions_TD]
			, [Fumbles]
			, [Fumbles_TD]
			, [Fumbles_Recovered]
			, [Kickoff_Avg]
			, [Kickoff_TD]
			, [Punt_Avg]
			, [Punt_TD]
			, [Penalties]
			, [PenaltyYards]
			, [ThirdDownPercentage]
			, [IsActive]
			, CreatedBy
			, CreatedDate
			, UpdatedBy
			, UpdatedDate
		) VALUES (
			SOURCE.[TeamKey]
			, SOURCE.[Year]
			, SOURCE.[Tackles]
			, SOURCE.[Sacks]
			, SOURCE.[TacklesForLoss]
			, SOURCE.[PassesDefensed]
			, SOURCE.[Interceptions]
			, SOURCE.[Interceptions_TD]
			, SOURCE.[Fumbles]
			, SOURCE.[Fumbles_TD]
			, SOURCE.[Fumbles_Recovered]
			, SOURCE.[Kickoff_Avg]
			, SOURCE.[Kickoff_TD]
			, SOURCE.[Punt_Avg]
			, SOURCE.[Punt_TD]
			, SOURCE.[Penalties]
			, SOURCE.[PenaltyYards]
			, SOURCE.[ThirdDownPercentage]
			, 1 --[IsActive]
			, @UserID
			, @ActionDate
			, @UserID
			, @ActionDate
		);
		
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage varchar(max) = (SELECT ERROR_MESSAGE());				
		RAISERROR(@ErrorMessage, 16, 1);
	END CATCH
GO

--creates history tables and their triggers --TRUNCATE TABLE admin.TeamStats
DECLARE @debug int = 0;
DECLARE @schemas TABLE ([name] varchar(10))
INSERT INTO @schemas ([name])
VALUES ('admin')
, ('dbo');

DECLARE @schemaname varchar(10) = (SELECT MIN(S.[name]) FROM @schemas S)
		, @sql varchar(max);
WHILE @schemaname IS NOT NULL
BEGIN

	DECLARE @tablename varchar(50) = (SELECT MIN(T.[name]) FROM sys.tables T INNER JOIN sys.schemas S ON S.[name] = @schemaname AND T.schema_id = S.schema_id);
	WHILE @tablename IS NOT NULL
	BEGIN
		SET @sql = 'CREATE TABLE [his].['+ @tablename +'] (' + CHAR(13) + CHAR(10);
		SET @sql += '[HistoryID] bigint NOT NULL IDENTITY(1,1) PRIMARY KEY' + CHAR(13) + CHAR(10);
		SET @sql += ', [HistoryDateStamp] datetime2 NOT NULL DEFAULT(GETDATE())' + CHAR(13) + CHAR(10);
		SET @sql += ', [HistoryAction] char(1) NOT NULL' + CHAR(13) + CHAR(10);
		DECLARE @columnname varchar(100) = ''
				, @columnstring varchar(max) = ''
				, @datatype varchar(100)
				, @datalength varchar(10)
				, @scale varchar(2)
				, @ordinalposition int
				, @precision varchar(2)
				, @NULLable varchar(10)
				, @DEFAULT varchar(50);
		
		SET @ordinalposition = (SELECT MIN(C.ORDINAL_POSITION) FROM INFORMATION_SCHEMA.COLUMNS C  WHERE C.TABLE_NAME = @tablename AND C.TABLE_SCHEMA = @schemaname);
		
		WHILE (@ordinalposition IS NOT NULL)
		BEGIN			
			SELECT DISTINCT
				@columnname = ', [' + T.COLUMN_NAME + ']'
				, @datatype = ' [' + T.DATA_TYPE + ']'
				, @precision = CONVERT(varchar, T.NUMERIC_PRECISION)
				, @scale = CONVERT(varchar, ISNULL(T.NUMERIC_SCALE, 2))
				, @datalength = ISNULL('(' + (CASE WHEN T.CHARACTER_MAXIMUM_LENGTH = -1 THEN 'max' ELSE CONVERT(varchar, T.CHARACTER_MAXIMUM_LENGTH) END) + ')', '')
				, @NULLable = (CASE WHEN T.IS_NULLABLE = 'NO' THEN ' NOT NULL' ELSE ' NULL' END)
				, @DEFAULT = ' ' + ISNULL('DEFAULT' + UPPER(T.COLUMN_DEFAULT), '')
			FROM INFORMATION_SCHEMA.COLUMNS T
			WHERE T.TABLE_SCHEMA = @schemaname
			AND T.TABLE_NAME = @tablename
			AND T.ORDINAL_POSITION = @ordinalposition;
						
			IF (@datatype IN (' [decimal]',' [numeric]',' [float]'))
			BEGIN
				SET @columnstring += @columnname + @datatype + '('+ @precision +','+ @scale +')' + @NULLable + @DEFAULT + CHAR(13) + CHAR(10);
			END
			ELSE
			BEGIN
				SET @columnstring += @columnname + @datatype + @datalength + @NULLable + @DEFAULT + CHAR(13) + CHAR(10);
			END

			SET @ordinalposition = (SELECT MIN(C.ORDINAL_POSITION) FROM INFORMATION_SCHEMA.COLUMNS C  WHERE C.TABLE_NAME = @tablename AND C.TABLE_SCHEMA = @schemaname AND C.ORDINAL_POSITION > @ordinalposition);
		END

		SET @sql += @columnstring; 
		SET @sql += ');' + CHAR(13) + CHAR(10); 

		PRINT (@sql);
		IF(@debug = 0)
		BEGIN
			IF EXISTS (SELECT * FROM sys.tables T INNER JOIN sys.schemas S ON S.[name] = 'his' AND S.schema_id = T.schema_id WHERE T.[name] = @tablename)
			BEGIN
				DECLARE @dropsql varchar(500) = 'DROP TABLE [his].['+ @tablename +'];';
				EXEC (@dropsql);
			END
			
			EXEC (@sql);
			
			IF EXISTS (
				SELECT * 
				FROM sys.triggers TR 
				INNER JOIN sys.tables T ON T.object_id = TR.parent_id
				INNER JOIN sys.schemas S ON S.[name] = @schemaname AND S.schema_id = T.schema_id 
				WHERE T.[name] = @tablename
			)
			BEGIN
				SET @dropsql = 'DROP TRIGGER ['+ @schemaname +'].['+ @tablename +'_SaveToHistory];';
				EXEC (@dropsql);
			END
			
			SET @sql = '
				CREATE TRIGGER ['+ @schemaname +'].['+ @tablename +'_SaveToHistory] ON ['+ @schemaname +'].['+ @tablename +']
				AFTER INSERT,UPDATE,DELETE
				AS
					DECLARE @stamp datetime2 = GETDATE();
					INSERT INTO his.'+ @tablename +'	
					SELECT * FROM (
						SELECT [HistoryDateStamp] = @stamp, [HistoryAction] = ''D'', * FROM deleted
						UNION ALL
						SELECT [HistoryDateStamp] = @stamp, [HistoryAction] = ''I'', * FROM inserted
					) DER;
				ENABLE TRIGGER ['+ @tablename +'_SaveToHistory] ON ['+ @schemaname +'].['+ @tablename +'];
			';			

			EXEC (@sql);
		END
		
		SET @tablename = (SELECT MIN(T.[name]) FROM sys.tables T INNER JOIN sys.schemas S ON S.[name] = @schemaname AND T.schema_id = S.schema_id WHERE T.[name] > @tablename);
	END

	SET @schemaname = (SELECT MIN(S.[name]) FROM @schemas S WHERE S.[name] > @schemaname);
END
GO