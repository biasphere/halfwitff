USE FFFFT
GO

IF EXISTS (SELECT * FROM sys.objects T WHERE T.[type] IN (N'FN', N'IF', N'TF', N'FS', N'FT') AND SCHEMA_NAME(T.[schema_id]) = 'dbo' AND T.[name] = 'fn_GetModelDataType')
BEGIN
	DROP FUNCTION dbo.fn_GetModelDataType;
END
GO
CREATE FUNCTION dbo.fn_GetModelDataType
( @DataType nvarchar(50)
, @ColumnName nvarchar(100)
, @Nullable bit = 1)
RETURNS varchar(500)
WITH EXECUTE AS CALLER
AS
BEGIN
	DECLARE @retval varchar(500) = 'public '
			, @gettersettersection nvarchar(100) = @ColumnName + ' { get; set; }' + CHAR(13);
	SET @DataType = UPPER(@DataType);

	IF (@DataType IN ('DECIMAL','MONEY','NUMERIC'))
	BEGIN
		SET @retval += 'decimal';
	END
	ELSE IF (@DataType IN ('INT'))
	BEGIN
		SET @retval += 'int';
	END
	ELSE IF (@DataType IN ('SMALLINT'))
	BEGIN
		SET @retval += 'short';
	END
	ELSE IF (@DataType IN ('BIGINT'))
	BEGIN
		SET @retval += 'long';
	END
	ELSE IF (@DataType IN ('BIT'))
	BEGIN
		SET @retval += 'bool';
		SET @Nullable = 0;
	END
	ELSE IF (@DataType LIKE '%CHAR%')
	BEGIN
		SET @retval += 'string';
		SET @Nullable = 0;
	END
	ELSE IF (@DataType LIKE '%BINARY%')
	BEGIN
		SET @retval += 'byte[]';
		SET @Nullable = 0;
	END
	ELSE IF (@DataType LIKE '%DATE%')
	BEGIN
		SET @retval += 'DateTime';
	END
	ELSE IF (@DataType IN ('TIME'))
	BEGIN
		SET @retval += 'TimeSpan';
	END

	SET @retval += (CASE WHEN @Nullable = 1 THEN '? ' ELSE ' ' END) + @gettersettersection;
	RETURN @retval; 
END
GO

IF EXISTS (SELECT * FROM sys.objects T WHERE T.[type] IN (N'FN', N'IF', N'TF', N'FS', N'FT') AND SCHEMA_NAME(T.[schema_id]) = 'dbo' AND T.[name] = 'fn_GenerateHelperText')
BEGIN
	DROP FUNCTION dbo.fn_GenerateHelperText;
END
GO
CREATE FUNCTION dbo.fn_GenerateHelperText
( 
	@TableName nvarchar(50)
	, @SchemaName nvarchar(100)
	, @WhatDo varchar(10)
	, @ForceNullable bit = 1
	, @TableTypeSuffix varchar(20) = 'TableType'  --THIS IS WHAT i USE FOR TABLETYPES
	, @TableTypeSchema varchar(20) = 'tabletype'  --THIS IS WHAT i USE FOR TABLETYPES
)
RETURNS varchar(max)
WITH EXECUTE AS CALLER
AS
BEGIN
		
	IF NOT EXISTS (SELECT * FROM sys.tables T WHERE T.name = @TableName AND SCHEMA_NAME(T.[schema_id]) = @SchemaName)
	BEGIN
		RETURN 'Table / Schema Pair Not Found'
	END
	
	DECLARE @updatecolumnvalues varchar(max) = ''
			, @mergecolumns varchar(max) = ''
			, @inserttopcolumns varchar(max) = ''
			, @modelcreator varchar(max) = ''		
			, @insertbottomcolumns varchar(max) = ''
			, @sql varchar(max);

	/*
	SELECT 
		@updatecolumnvalues += ', TARGET.[' + J.name + '] = SOURCE.[' + J.name + ']' + CHAR(13)
		, @inserttopcolumns += ', [' + J.name + ']' + CHAR(13)
		, @insertbottomcolumns += ', SOURCE.[' + J.name + ']' + CHAR(13)
	FROM sys.table_types T
	INNER JOIN sys.columns J ON J.object_id = T.type_table_object_id
	WHERE T.name = 'TableParameter_PayPeriod'
	AND SCHEMA_NAME(T.schema_id) = 'tabletype';
	*/

	SELECT 
		@updatecolumnvalues += ', T.[' + J.name + '] = @' + J.name + '' + CHAR(13)
		, @mergecolumns += ', TARGET.[' + J.name + '] = SOURCE.[' + J.name + ']' + CHAR(13)
		, @inserttopcolumns += ', [' + J.name + ']' + CHAR(13)
		, @insertbottomcolumns += ', SOURCE.[' + J.name + ']' + CHAR(13)
		, @modelcreator += (CASE WHEN LJ1.name IS NOT NULL THEN '[AssignToTableType(true)]' ELSE '[AssignToTableType(false)]' END) + CHAR(13) 
							+ (CASE WHEN MAX(ISNULL(CONVERT(int, LJ4.is_primary_key),0)) = 1 THEN '[Key]' + CHAR(13) ELSE '' END) 
							+ (CASE WHEN MAX(ISNULL(CONVERT(int, LJ4.is_primary_key),0)) = 1 AND LJ1.name IS NOT NULL THEN '[Required]' + CHAR(13) ELSE '' END) 
							+ dbo.fn_GetModelDataType(J1.[DATA_TYPE], J.[name], (CASE WHEN @ForceNullable = 1 THEN 1 ELSE MAX(CONVERT(int, J.is_nullable)) END))
	FROM sys.tables T
	INNER JOIN sys.columns J ON J.[object_id] = T.[object_id]
	INNER JOIN INFORMATION_SCHEMA.COLUMNS J1 ON J1.[TABLE_NAME] = T.[name] AND J1.[TABLE_SCHEMA] = SCHEMA_NAME(T.[schema_id]) AND J1.[COLUMN_NAME] = J.[name]
	LEFT JOIN sys.table_types LJ ON LJ.[name] = T.[name] + @TableTypeSuffix AND SCHEMA_NAME(LJ.[schema_id]) = @TableTypeSchema
	LEFT JOIN sys.columns LJ1 ON LJ1.[object_id] = LJ.type_table_object_id AND LJ1.[name] = J.[name]
	LEFT JOIN INFORMATION_SCHEMA.COLUMNS LJ2 ON LJ2.[TABLE_NAME] = LJ.[name] AND LJ2.[TABLE_SCHEMA] = SCHEMA_NAME(LJ.[schema_id]) AND LJ2.[COLUMN_NAME] = LJ1.[name]
	LEFT JOIN sys.index_columns LJ3 ON LJ3.[object_id] = J.[object_id] AND LJ3.column_id = J.column_id
	LEFT JOIN sys.indexes LJ4 ON LJ3.[object_id] = LJ4.[object_id]
	WHERE T.name = @TableName
	AND SCHEMA_NAME(T.[schema_id]) = @SchemaName
	GROUP BY J.name, LJ1.name, J1.[DATA_TYPE],LJ2.ORDINAL_POSITION, J1.ORDINAL_POSITION
	ORDER BY LJ2.ORDINAL_POSITION, J1.ORDINAL_POSITION;
	
	SET @updatecolumnvalues = SUBSTRING(@updatecolumnvalues, 3, LEN(@updatecolumnvalues) - 2);
	SET @mergecolumns = SUBSTRING(@mergecolumns, 3, LEN(@mergecolumns) - 2);
	SET @inserttopcolumns = SUBSTRING(@inserttopcolumns, 3, LEN(@inserttopcolumns) - 2);
	SET @insertbottomcolumns = SUBSTRING(@insertbottomcolumns, 3, LEN(@insertbottomcolumns) - 2);
	SET @modelcreator = '[Table("'+ @TableName +'", Schema="'+ @SchemaName +'")]' + CHAR(13) + 'public class ' + @TableName + CHAR (13) + '{' + CHAR (13) + @modelcreator + '}' + CHAR(13); 

	/*
	PRINT(@updatecolumnvalues);
	PRINT(@mergecolumns);
	PRINT(@inserttopcolumns);
	PRINT(@insertbottomcolumns);
	PRINT(@modelcreator);
	*/

	SET @sql = (CASE WHEN @WhatDo = 'INSERT' THEN (@inserttopcolumns + @insertbottomcolumns)
					 WHEN @WhatDo = 'UPDATE' THEN @updatecolumnvalues
					 WHEN @WhatDo = 'MERGE' THEN @mergecolumns
					 WHEN @WhatDo = 'CLASS' THEN @modelcreator
					 END);
	RETURN @sql; 
END
GO

SELECT T.*
FROM sys.table_types T
INNER JOIN sys.columns C ON C.[object_id] = T.type_table_object_id AND SCHEMA_NAME(T.[schema_id]) = 'tabletype'

IF EXISTS (SELECT * FROM sys.objects T WHERE T.[type] IN (N'FN', N'IF', N'TF', N'FS', N'FT') AND SCHEMA_NAME(T.[schema_id]) = 'dbo' AND T.[name] = 'fn_GenerateTableType')
BEGIN
	DROP FUNCTION dbo.fn_GenerateTableType;
END
GO
CREATE FUNCTION dbo.fn_GenerateTableType
( 
	@TableName nvarchar(50)
	, @SchemaName nvarchar(100)
	, @StructureMatch bit = 0 --want nullable everything, No createds/updateds
)
RETURNS varchar(max)
WITH EXECUTE AS CALLER
AS
BEGIN
		
	IF NOT EXISTS (SELECT * FROM sys.tables T WHERE T.name = @TableName AND SCHEMA_NAME(T.[schema_id]) = @SchemaName)
	BEGIN
		RETURN 'Table / Schema Pair Not Found'
	END
	
	DECLARE @columns varchar(max) = ''
			, @sql varchar(max) = 'CREATE TYPE [tabletype].['+ @TableName + 'TableType] AS TABLE (' + CHAR(13)
			, @ifsql varchar(max) = 'IF EXISTS (SELECT * FROM sys.table_types WHERE SCHEMA_NAME([schema_id]) = ''tabletype'' AND [name] = '''+ @TableName +'TableType'') '
									+ CHAR(13) + 'BEGIN ' 
									+ CHAR(13) + '	DROP TYPE [tabletype].['+ @TableName + 'TableType]; '
									+ CHAR(13) + 'END '
									+ CHAR(13) + 'GO '
									+ CHAR(13);
				
	SELECT 
		@columns += '	, ['+ c.[COLUMN_NAME] +'] '
					+ c.[DATA_TYPE] + ' '
					+ (CASE WHEN c.[CHARACTER_MAXIMUM_LENGTH] IS NULL THEN ''
							WHEN c.[CHARACTER_MAXIMUM_LENGTH] = -1 THEN '(max) '
							ELSE '('+ CONVERT(varchar, c.[CHARACTER_MAXIMUM_LENGTH]) +') ' END)
					+ (CASE WHEN @StructureMatch = 0 THEN 'NULL' 
							WHEN C.[IS_NULLABLE] = 'YES' THEN 'NOT NULL'
							ELSE 'NULL' END)
					+ CHAR(13)
	FROM INFORMATION_SCHEMA.COLUMNS c 
	WHERE c.[TABLE_NAME] = @TableName 
	AND c.[TABLE_SCHEMA] = @SchemaName
	AND (@StructureMatch = 1 OR c.[COLUMN_NAME] NOT IN ('CreatedDate','CreatedBy','UpdatedDate','UpdatedBy'))
	ORDER BY C.[ORDINAL_POSITION] ASC;
	
	SET @columns = SUBSTRING(@columns, 3, LEN(@columns) - 3);

	SET @sql = @ifsql + @sql + @columns + CHAR(13) + ');' + CHAR(13)  + 'GO' + CHAR(13);

	RETURN @sql; 
END
GO

PRINT(dbo.fn_GenerateTableType('Player', 'dbo', DEFAULT));
GO
PRINT(dbo.fn_GenerateTableType('Team', 'dbo', DEFAULT));
GO
PRINT(dbo.fn_GenerateTableType('TeamStatsNFL', 'dbo', DEFAULT));
GO
PRINT(dbo.fn_GenerateTableType('PlayerStatsNFL', 'dbo', DEFAULT));
GO
PRINT(dbo.fn_GenerateTableType('PlayerData', 'dbo', DEFAULT));
GO
PRINT(dbo.fn_GenerateTableType('DraftLeague', 'dbo', DEFAULT));
GO
PRINT(dbo.fn_GenerateTableType('DraftLeaguePayout', 'dbo', DEFAULT));
GO
PRINT(dbo.fn_GenerateTableType('DraftTeam', 'dbo', DEFAULT));
GO
PRINT(dbo.fn_GenerateTableType('DraftUser', 'dbo', DEFAULT));
GO
PRINT(dbo.fn_GenerateTableType('PlayersDrafted', 'dbo', DEFAULT));
GO
PRINT(dbo.fn_GenerateTableType('SiteUser', 'dbo', DEFAULT));
GO
PRINT(dbo.fn_GenerateTableType('TeamsDrafted', 'dbo', DEFAULT));
GO

PRINT(dbo.fn_GenerateHelperText('DraftLeague', 'dbo', 'CLASS', DEFAULT, DEFAULT, DEFAULT));
GO